/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

/**
 * 
 * Contains method used to manipulate file (Java IO File).
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class FileTools {

    /** Default name for file. */
    public static final String DEFAULT_FILE_NAME = "Default_name"; //$NON-NLS-1$

    /** Buffer size for compression. */
    public static final int BUFFER_SIZE = 2048;

    /** Static for google code. */
    private static final int TEMP_DIR_ATTEMPTS = 10000;

    /** Pattern to for all non alphanumeric basic char. */
    private static final String ALL_NOTCHARNUMBER = "\\W+"; //$NON-NLS-1$

    /** Replace char for file name. */
    private static final String UNDERSCORE = "_"; //$NON-NLS-1$

    /**
     * Don't allow to instantiate this class.
     */
    private FileTools() {
    }

    /**
     * Check if file name is correct for platform OS, according to RCP standard.
     * 
     * No hypothesis are done on file rights.
     * 
     * {@link http://stackoverflow.com/questions/6730009/validate-a-file-name-on-windows}
     * 
     * @param pFileName file name for which validation is needed.
     * @return true if this file name is correct, false otherwise.
     */
    public static boolean isValidName(final String pFileName) {
        boolean vResult = false;

        if (pFileName != null) {
            vResult = Path.ROOT.isValidPath(pFileName);
        }

        return vResult;
    }

    /**
     * Filter a candidate file name and replace all non alphanumeric char.
     * If candidate file name is null or empty, return a default one without extension.
     * 
     * @param pCandidateName a base name to filter.
     * @return a filtered name.
     */
    public static String getCorrectName(final String pCandidateName) {
        String vFilteredName = DEFAULT_FILE_NAME;

        if (pCandidateName != null && !pCandidateName.isEmpty()) {
            vFilteredName = pCandidateName.replaceAll(ALL_NOTCHARNUMBER, UNDERSCORE);
        }

        return vFilteredName;
    }

    /**
     * Copy file from source URI to target URI.
     * Target will be overridden.
     * 
     * @param pSourceURI URI of source file.
     * @param pTargetURI URI of target file.
     * @throws IOException some IOException
     */
    public static void copyFile(final URI pSourceURI, final URI pTargetURI)
        throws IOException {

        ExtensibleURIConverterImpl vURIConverter = new ExtensibleURIConverterImpl();

        // If file exist delete it.
        deleteFile(pTargetURI);

        InputStream vSourceStream = vURIConverter.createInputStream(pSourceURI);
        OutputStream vTargetStream = vURIConverter.createOutputStream(pTargetURI);

        // Copy file.
        IOUtils.copy(vSourceStream, vTargetStream);

        // Close quietly will kill any exception
        // But don't kill quietly will let's open some stream.
        vTargetStream.flush();
        vTargetStream.close();
        vSourceStream.close();

    }

    /**
     * Compress a list of files in a target archive, as a flat hierarchy.
     * If archive already exists, it will be override.
     * 
     * @param pURIsList List of <b>files</b> URI
     * @param pTargetURI URI of target file
     * @throws IOException file I/O exceptions
     */
    public static void zipFiles(final List<URI> pURIsList, final URI pTargetURI) throws IOException {

        if (pURIsList != null && pTargetURI != null) {

            // Delete file
            deleteFile(pTargetURI);

            // Get a stream on target zip.
            ExtensibleURIConverterImpl vURIConverter = new ExtensibleURIConverterImpl();
            // Create an output stream.
            OutputStream vOutStream = vURIConverter.createOutputStream(pTargetURI);
            ZipOutputStream vZipOut = new ZipOutputStream(vOutStream);

            // Select the higher compression level.
            vZipOut.setLevel(Deflater.BEST_COMPRESSION);

            // Buffer used to read data from file stream and push it in zip output stream.
            byte[] vDataArray = new byte[BUFFER_SIZE];

            // Create a zip entry for each file.
            for (URI vURI : pURIsList) {
                InputStream vEntryStream = vURIConverter.createInputStream(vURI);

                ZipEntry vEntry = new ZipEntry(URI.decode(vURI.lastSegment()));
                vZipOut.putNextEntry(vEntry);

                int vCount = vEntryStream.read(vDataArray, 0, BUFFER_SIZE);
                while (vCount != -1) {
                    vZipOut.write(vDataArray, 0, vCount);
                    vCount = vEntryStream.read(vDataArray, 0, BUFFER_SIZE);
                }

                vZipOut.flush();
                vZipOut.closeEntry();

                // Close quietly will kill any exception
                // But don't kill quietly will let's open some stream.
                vEntryStream.close();
            }

            vZipOut.close();
            vOutStream.close();
        }
    }

    /**
     * Uncompress an archive file to target directory.
     * 
     * Works only with archive construct by{@link FileTools#zipFiles(List, URI)}.
     * 
     * @param pArchiveURI URI of source archive.
     * @param pTargetDirectoryURI URI of target directory.
     * @throws IOException if problems occurs with archive or target directory.
     */
    public static void unzip(final URI pArchiveURI, final URI pTargetDirectoryURI) throws IOException {

        if (pArchiveURI != null && pTargetDirectoryURI != null) {

            // Get a stream on target zip.
            ExtensibleURIConverterImpl vURIConverter = new ExtensibleURIConverterImpl();
            InputStream vInputStream = vURIConverter.createInputStream(pArchiveURI);
            ZipInputStream vZipInputStream = new ZipInputStream(vInputStream);

            // Iterate on each entry of this zip
            ZipEntry vZipEntry = vZipInputStream.getNextEntry();

            while (vZipEntry != null) {
                // Create a output stream for this entry
                URI vTargetEntryURI = pTargetDirectoryURI.appendSegment(vZipEntry.getName());

                // Delete file
                deleteFile(vTargetEntryURI);

                OutputStream vTargetStream = vURIConverter.createOutputStream(vTargetEntryURI);

                // Transfer data.
                // Buffer used to read data from zip stream and push it in file output stream.
                byte[] vDataArray = new byte[BUFFER_SIZE];

                // Read from zip file...
                int vCount = vZipInputStream.read(vDataArray, 0, BUFFER_SIZE);
                while (vCount != -1) {
                    // And write it to target file!
                    vTargetStream.write(vDataArray, 0, vCount);

                    vCount = vZipInputStream.read(vDataArray, 0, BUFFER_SIZE);
                }
                // Get next entry
                vZipEntry = vZipInputStream.getNextEntry();

                // Close quietly will kill any exception
                // But don't kill quietly will let's open some stream.
                vTargetStream.flush();
                vTargetStream.close();
            }

            vZipInputStream.close();
            vInputStream.close();
        }
    }

    /**
     * Delete file or directory.
     * 
     * @param pFileURI URI of file/directory to delete.
     * @throws IOException files exception. Can occurs for any reasons linked to I/O actions.
     */
    private static void deleteFile(final URI pFileURI) throws IOException {
        File vTargetFile = new File(CommonPlugin.resolve(pFileURI).toFileString());
        if (vTargetFile.exists()) {
            FileUtils.forceDelete(vTargetFile);
        }
    }

    /**
     * Method issue from guava Files (google API). Waiting JDK 7 for a proper solution.
     * 
     * @return a temp directory
     */
    public static File createTempDir() {
        File vBaseDir = new File(System.getProperty("java.io.tmpdir"));
        String vBaseName = System.currentTimeMillis() + "-";

        for (int vCounter = 0; vCounter < TEMP_DIR_ATTEMPTS; vCounter++) {
            File vTempDir = new File(vBaseDir, vBaseName + vCounter);
            if (vTempDir.mkdir()) {
                return vTempDir;
            }
        }
        throw new IllegalStateException("Failed to create directory within "
            + TEMP_DIR_ATTEMPTS + " attempts (tried "
            + vBaseName + "0 to " + vBaseName + (TEMP_DIR_ATTEMPTS - 1) + ')');
    }

    /**
     * Get file extension.
     * 
     * Can return empty string if no extension are found, or null if pFile is null.
     * 
     * @param pFile File to determine extension
     * @return file extension.
     */
    public static String getExtensionFile(final File pFile) {
        String vExt = null;

        if (pFile != null) {
            vExt = StringUtils.EMPTY;
            String vFileName = pFile.getName();
            int vIndex = vFileName.lastIndexOf('.');

            if (vIndex > 0 && vIndex < vFileName.length() - 1) {
                vExt = vFileName.substring(vIndex + 1).toLowerCase();
            }
        }

        return vExt;
    }

    /**
     * Load a file from the given String which must correspond to an EMF URI.
     * 
     * The URI can use the platform or file scheme. In case of a platform URI, the URI
     * is converted to a local file URI before loading the file.
     * 
     * @param pFileURIAsString The EMF URI of the file to load, as String
     * @return Java file loaded, or null if the given URI doesn't refer to an accessible file
     */
    public static File loadFile(final String pFileURIAsString) {
        File vResultFile = null;

        if (StringUtils.isNotEmpty(pFileURIAsString)) {
            // Build the URI from the given String
            URI vFileURI = URI.createURI(pFileURIAsString);

            // Load the file
            vResultFile = loadFile(vFileURI);
        }

        return vResultFile;
    }

    /**
     * Load a file from the given URI.
     * 
     * The URI can use the platform or file scheme. In case of a platform URI, the URI
     * is converted to a local file URI before loading the file.
     *
     * @param pFileURI The EMF URI of the file to load
     * @return Java file loaded, or null if the given URI doesn't refer to an accessible file
     */
    public static File loadFile(final URI pFileURI) {
        File vResultFile = null;

        if (pFileURI != null) {
            URI vLocalURI = pFileURI;

            // Resolve to local URI if the URI uses a platform scheme
            if (vLocalURI.isPlatform()) {
                vLocalURI = CommonPlugin.asLocalURI(vLocalURI);
            }

            // Get the file path and build the File object from it
            String vFilePath = vLocalURI.toFileString();
            if (vFilePath != null) {
                vResultFile = new File(vFilePath);
            }
        }

        return vResultFile;
    }

    /**
     * From an EMF <b>File</b> URI find the Eclipse project.
     * 
     * @param pFileURI URI of file for which project is search
     * @return The project found. <code>null</code> in case of no project found or incorrect parameter
     */
    public static IProject getProject(final URI pFileURI) {
        IProject vCurrentProject = null;

        if (pFileURI != null) {

            IWorkspaceRoot vWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
            IFile vFile = vWorkspaceRoot.getFileForLocation(new Path(pFileURI.toFileString()));

            vCurrentProject = vFile.getProject();
        }

        return vCurrentProject;
    }
}
