/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.state;

/**
 * 
 * Interface used to manipulate a state for filter.
 * DP State
 * 
 * @param <T> type of state
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IState<T> {

    /**
     * @return state of filter.
     */
    T getFilterState();

    /**
     * @param pValue state to set.
     */
    void setState(T pValue);

    /**
     * Automatically update this state.
     * 
     * @return current state.
     * */
    T updateState();

}
