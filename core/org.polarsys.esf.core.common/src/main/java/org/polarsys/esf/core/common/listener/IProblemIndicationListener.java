/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.listener;

import org.eclipse.emf.common.util.Diagnostic;



/**
 * Interface for all the objects which are waiting for a diagnostic
 * on resources. 
 * 
 * The diagnostics are performed by a ProblemIndicationAdapter,
 * which will also warn all the listeners.  
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IProblemIndicationListener {
    
    /**
     * Method launched to warn a listener that a new diagnostic has
     * been built.
     * 
     * @param pDiagnostic The new diagnostic, resulting of the last analysis
     */
    void updateProblemIndication(final Diagnostic pDiagnostic);

}
