/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;

/**
 * Class used to find a file in a project.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ProjectFinder {

    /** Current project for which research is done. */
    private IProject mProject = null;

    /**
     * Default constructor.
     *
     * @param pProject context for research.
     */
    public ProjectFinder(final IProject pProject) {
        mProject = pProject;
    }

    /**
     * Find first file in current project with given extension.
     *
     * @param pExtension search parameter.
     * @return {@link IFile} found, null otherwise.
     * @throws CoreException if problem occurs during visit of project.
     */
    public IFile findFirstFile(final String pExtension) throws CoreException {
        IFile vResult = null;

        if (mProject != null && mProject.isAccessible()) {
            FileVisitorFinder vFinder = new FileVisitorFinder(pExtension);
            mProject.accept(vFinder);

            vResult = vFinder.getFile();
        }

        return vResult;
    }

    /**
     * Find all file in current project with given extension.
     *
     * @param pExtensions Search parameter
     * @return {@link List} of {@link IFile} containing found files.
     * @throws CoreException f problem occurs during visit of project.
     */
    public List<IFile> findAllFiles(final String[] pExtensions) throws CoreException {
        List<IFile> vResultList = new ArrayList<IFile>();

        if (mProject != null && mProject.isAccessible()) {
            AllFileVisitor vVisitor = new AllFileVisitor(pExtensions);
            mProject.accept(vVisitor);
            vResultList.addAll(vVisitor.getFiles());
        }

        return vResultList;
    }

    /**
     *
     * Class to visit project.
     */
    private class FileVisitorFinder
        implements IResourceVisitor {

        /** Extension filter. */
        private String mExtensionFilter = null;

        /** URI of first file found with good extension. */
        private IFile mFirstFile = null;

        /**
         * Default constructor.
         *
         * @param pExtension filter for research.
         * */
        FileVisitorFinder(final String pExtension) {
            mExtensionFilter = pExtension;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {
            boolean vContinue = mFirstFile == null;

            if (pResource.getType() == IResource.FILE && vContinue) {
                if (pResource.getName().endsWith(mExtensionFilter)) {
                    mFirstFile = (IFile) pResource;
                    vContinue = false;
                }
            }

            return vContinue;
        }

        /**
         * @return the fileURI
         */
        public IFile getFile() {
            return mFirstFile;
        }

    }

    /**
     *
     * Visitor for project which find all files.
     *
     * @author $Author: jdumont $
     * @version $Revision: 83 $
     */
    private class AllFileVisitor
        implements IResourceVisitor {

        /** Found ESF files in project. */
        private List<IFile> mSAFilesList = null;

        /** Extension filter. */
        private List<String> mExtensionFilesList = null;

        /**
         * Default constructor.
         *
         * @param pFileExtensions filter for research.
         */
        AllFileVisitor(final String[] pFileExtensions) {
            mSAFilesList = new ArrayList<IFile>();
            mExtensionFilesList = Arrays.asList(pFileExtensions);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {

            // Check resource
            if (pResource instanceof IFile
                && (mExtensionFilesList.contains(pResource.getFileExtension()) || mExtensionFilesList.isEmpty())) {
                mSAFilesList.add((IFile) pResource);
            }

            // Explore all project
            return true;
        }

        /**
         * @return Found ESF files in project.
         */
        public List<IFile> getFiles() {
            return mSAFilesList;
        }

    }

}
