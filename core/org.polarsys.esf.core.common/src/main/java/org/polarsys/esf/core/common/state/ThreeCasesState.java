/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.state;

/**
 * 
 * State of filter with three cases : no filter, filter true, filter false.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ThreeCasesState
    implements IState<Integer> {

    /** Do not filter. */
    public static final int FILTER_NONE = 0;

    /** Filter only no feared events. */
    public static final int FILTER_FALSE = 1;

    /** Filter only feared events. */
    public static final int FILTER_TRUE = 2;

    /** 0 means show all, 1 means show only true, 2 means show only false. */
    private int mStateValue = FILTER_NONE;

    /**
     * Default constructor.
     */
    public ThreeCasesState() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Integer getFilterState() {
        return mStateValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void setState(final Integer pValue) {
        mStateValue = pValue;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Integer updateState() {
        if (mStateValue >= FILTER_TRUE) {
            mStateValue = FILTER_NONE;
        } else {
            mStateValue++;
        }
        return mStateValue;
    }

}
