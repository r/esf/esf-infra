/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.log;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.polarsys.esf.core.common.CommonActivator;

/**
 * Facilities to manipulate status.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 * 
 */
public final class StatusUtil {

    /**
     * Private constructor to avoid instantiation.
     */
    private StatusUtil() {
    }

    /**
     * Create an error status from an exception.
     * 
     * @param pException Source for status
     * @return An error status
     */
    public static IStatus createStatusFromException(final Exception pException) {

        IStatus vStatus = new Status(
            Status.ERROR,
            CommonActivator.getPlugin().getSymbolicName(),
            pException.getMessage(),
            pException);

        return vStatus;
    }
}
