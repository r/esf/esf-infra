/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.process;

import java.io.OutputStream;

import org.eclipse.core.runtime.IStatus;

/**
 * 
 * Interface implemented by class able to manipulate resulting error and standard stream from process.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IProcessOutputHandler {

    /**
     * Link a process launcher and this output handler.
     * 
     * @param pProcessLauncher launcher for process for which output results must be handled.
     */
    void linkToProcess(ProcessLauncher pProcessLauncher);

    /**
     * From output (error and standard), and exit code, construct a Status.
     * Must be redefine.
     * 
     * @param pProcessResult exit code of process.
     * @return an IStatus computed from data in output streams
     */
    IStatus computeStatus(int pProcessResult);

    /**
     * @return the standardStream
     */
    OutputStream getStandardStream();

    /**
     * @param pStandardStream the standardStream to set
     */
    void setStandardStream(OutputStream pStandardStream);

    /**
     * @return the errorStream
     */
    OutputStream getErrorStream();

    /**
     * @param pErrorStream the errorStream to set
     */
    void setErrorStream(OutputStream pErrorStream);

}
