/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.observer;

/**
 * Interface for observer (DP Observer).
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IObserver {

    /**
     * Method call by observable item when an event occurred.
     * 
     * @param pOb any argument given by originator.
     */
    void update(Object pOb);
}
