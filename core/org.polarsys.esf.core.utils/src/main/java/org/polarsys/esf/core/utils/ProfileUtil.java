/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.uml.tools.commands.ApplyProfileCommand;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;

/**
 * Utilities for the manipulation of profiles and stereotypes.
 *
 * @author $Author: jdumont $
 * @version $Revision: 168 $
 */
public final class ProfileUtil {

    /** Label of the command to apply stereotype. */
    private static final String COMMAND_LABEL_APPLY_STEREOTYPE = "ProfileUtil: Apply Stereotype"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private ProfileUtil() {
        // Nothing to do
    }

    /**
     * Apply profile on the model.
     *
     * @param pModel Model on which apply the profile
     * @param pProfile The profile to apply
     * @return <code>true</code> if the profile has been applied, <code>false</code> otherwise
     */
    public static boolean applyProfile(final Model pModel, final Profile pProfile) {
        boolean vProfileApplied = false;

        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pModel);

        // Create the command to apply the profile
        ApplyProfileCommand vApplySAProfileCmd =
            new ApplyProfileCommand(pModel.getNearestPackage(), Collections.singleton(pProfile), vEditingDomain);

        if (vApplySAProfileCmd.canExecute()) {
            try {
                vEditingDomain.getCommandStack().execute(vApplySAProfileCmd);
                vProfileApplied = true;

            } catch (final IllegalArgumentException pException) {
                CoreUtilsActivator.logError("Error during the profile application", pException); //$NON-NLS-1$
            }
        }
        return vProfileApplied;
    }

    /**
     * Apply stereotype on the element.
     *
     * @param pElement Element on which a stereotype must be applied
     * @param pStereotype Stereotype to apply
     */
    public static void applyStereotype(final Element pElement, final Stereotype pStereotype) {
        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pElement);
        RecordingCommand vApplyStereotypeCmd = new RecordingCommand(vEditingDomain, COMMAND_LABEL_APPLY_STEREOTYPE) {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void doExecute() {
                if (!pElement.isStereotypeApplied(pStereotype)) {
                    pElement.applyStereotype(pStereotype);
                }
            }
        };

        if (vApplyStereotypeCmd.canExecute()) {
            vEditingDomain.getCommandStack().execute(vApplyStereotypeCmd);
        }
    }

    /**
     * Apply stereotype on the element.
     *
     * @param pElement Element on which a stereotype must be applied
     * @param pEClass Stereotype to apply
     */
    public static void applyStereotype(final Element pElement, final EClass pEClass) {
        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pElement);
        RecordingCommand vApplyStereotypeCmd = new RecordingCommand(vEditingDomain, COMMAND_LABEL_APPLY_STEREOTYPE) {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void doExecute() {
                StereotypeApplicationHelper.getInstance(pElement.getModel()).applyStereotype(pElement, pEClass);
            }
        };

        if (vApplyStereotypeCmd.canExecute()) {
            vEditingDomain.getCommandStack().execute(vApplyStereotypeCmd);
        }
    }

    /**
     * Get all sub profiles from the profile.
     *
     * @param pProfile Profile from which sub profiles are searched
     * @return Map of sub profiles with their qualified name as key
     */
    public static Map<String, Profile> getSubProfiles(final Profile pProfile) {
        Map<String, Profile> vSubProfilesMap = new HashMap<String, Profile>();

        // Retrieve sub profiles
        for (Element vCurrentElement : pProfile.getOwnedElements()) {
            if (vCurrentElement instanceof Stereotype) {
                if (!vSubProfilesMap.containsValue((Profile) pProfile)) {
                    vSubProfilesMap.put(((Profile) pProfile).getQualifiedName(), (Profile) pProfile);
                }
            }
            if (vCurrentElement instanceof Profile) {
                vSubProfilesMap.putAll(getSubProfiles((Profile) vCurrentElement));
            }
        }

        return vSubProfilesMap;
    }

    /**
     * Apply list of profiles on the model.
     *
     * @param pModel Model on which the profiles must be applied
     * @param pProfilesMap Map of profiles to apply
     * @return <code>true</code> if the profiles have been applied, <code>false</code> otherwise
     */
    public static boolean applyProfilesList(final Model pModel, final Map<String, Profile> pProfilesMap) {
        boolean vProfilesApplied = false;

        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pModel);

        // Create the command to apply sub profiles
        ApplyProfileCommand vApplySubProfileCmd =
            new ApplyProfileCommand(pModel.getNearestPackage(), pProfilesMap.values(), vEditingDomain);

        if (vApplySubProfileCmd.canExecute()) {
            try {
                vEditingDomain.getCommandStack().execute(vApplySubProfileCmd);
                vProfilesApplied = true;

            } catch (final IllegalArgumentException pException) {
                CoreUtilsActivator.logError("Error during the profiles application", pException); //$NON-NLS-1$
            }
        }
        return vProfilesApplied;
    }

}
