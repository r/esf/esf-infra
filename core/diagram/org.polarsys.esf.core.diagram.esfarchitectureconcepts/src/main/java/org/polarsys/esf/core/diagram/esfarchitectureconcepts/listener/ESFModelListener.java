/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.listener;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.papyrus.infra.core.listenerservice.IPapyrusListener;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ProfileUtil;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage;;

/**
 * Listener for ESFModel.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFModelListener
    implements IPapyrusListener {

    /**
     * Get notification when a new SPart is added inside a SBlock. If it happens, apply SBlock stereotype on the type
     * of this new SPart if it is necessary.
     */
    @Override
    public void notifyChanged(Notification pNotification) {
        if ((Notification.SET == pNotification.getEventType()) && (pNotification.getNotifier() instanceof Property)) {
            Property vProp = (Property) pNotification.getNotifier();

            if (vProp.getType() instanceof Class) {
                Stereotype vSPartStereotype = null;

                // When added new SPart by palette
                if (UMLUtil.getStereotypeApplication(vProp, ISPart.class) != null) {
                    vSPartStereotype = UMLUtil.getStereotype(UMLUtil.getStereotypeApplication(vProp, ISPart.class));
                    if (vSPartStereotype != null) {
                        Class vType = (Class) vProp.getType();
                        // Verify if type of SPart is stereotyped by SBlock
                        // If type is not stereotyped by SBlock, it must be stereotyped
                        if (UMLUtil.getStereotypeApplication(vType, ISBlock.class) == null) {
                            ProfileUtil
                                .applyStereotype((Element) vType, ESFArchitectureConceptsPackage.eINSTANCE.getSBlock());
                        }
                    }
                }
            }
        }
    }
}
