/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.set;

/**
 * Sets of ESF Architecture Concepts Diagram.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFArchitectureConceptsDiagramSet {

    public static final String ESFARCHITECTURECONCEPTS_DIAGRAM = "ESF Architecture Diagram"; //$NON-NLS-1$

    public static final String ESFARCHITECTURECONCEPTS_DIAGRAM_SHORT_NAME = "ESFArchitectureDiagram"; //$NON-NLS-1$

    public static final String CREATE_ESFARCHITECTURECONCEPTS_DIAGRAM = "Create ESF Architecture Diagram"; //$NON-NLS-1$

    public static final String ID_SBLOCK = "org.polarsys.esf.ESFArchitectureConcepts.SBlock"; //$NON-NLS-1$

}
