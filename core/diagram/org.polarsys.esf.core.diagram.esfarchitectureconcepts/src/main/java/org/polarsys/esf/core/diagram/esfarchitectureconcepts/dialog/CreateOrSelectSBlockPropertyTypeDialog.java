/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.dialog;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.papyrus.uml.diagram.common.dialogs.CreateOrSelectTypeDialog;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.diagram.esfarchitectureconcepts.set.ESFArchitectureConceptsDiagramSet;
import org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage;

/**
 * Dialog for initialization of SPart (Property) type (the type is either selected or created).
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateOrSelectSBlockPropertyTypeDialog
    extends CreateOrSelectTypeDialog {

    /**
     *
     * Create a new dialog to initialize an ActivityParameterNode.
     *
     * @param pShell The parent Shell
     * @param pOwner The activity that owns the action
     */
    public CreateOrSelectSBlockPropertyTypeDialog(
        Shell pShell,
        NamedElement pOwner) {
        super(
            pShell,
            pOwner,
            ElementTypeRegistry.getInstance().getType(ESFArchitectureConceptsDiagramSet.ID_SBLOCK),
            UMLPackage.eINSTANCE.getTypedElement_Type(),
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock(),
            UMLElementTypes.PACKAGE,
            UMLPackage.eINSTANCE.getPackage_PackagedElement(),
            null,
            null);
    }

}
