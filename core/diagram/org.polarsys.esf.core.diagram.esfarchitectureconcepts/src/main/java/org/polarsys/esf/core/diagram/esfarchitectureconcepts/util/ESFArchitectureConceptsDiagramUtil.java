/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationModel;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramUtils;
import org.eclipse.papyrus.infra.viewpoints.policy.PolicyChecker;
import org.eclipse.papyrus.infra.viewpoints.policy.ViewPrototype;
import org.eclipse.uml2.uml.Class;
import org.polarsys.esf.core.diagram.esfarchitectureconcepts.set.ESFArchitectureConceptsDiagramSet;

/**
 * Utility class for the ESF Architecture Concepts Diagram Diagrams.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFArchitectureConceptsDiagramUtil {

    /** Underscore. */
    private static final String UNDERSCORE = "_"; //$NON-NLS-1$

    /**
     * Create ESF Architecture Concepts Diagram.
     *
     * @param pClass
     * @param pDomain
     */
    public static void createDiagram(final Class pClass, TransactionalEditingDomain pDomain) {

        // Get ESF Architecture Concept Diagram Prototype
        final ViewPrototype vDiagramProtype = getESFArchitectureConceptsDiagramPrototype(pDomain);

        RecordingCommand vCeateDiagramCmd =
            new RecordingCommand(pDomain, ESFArchitectureConceptsDiagramSet.CREATE_ESFARCHITECTURECONCEPTS_DIAGRAM) {

                @Override
                protected void doExecute() {
                    // Create a ESF Architecture Concept Diagram for the SBlock
                    vDiagramProtype.instantiateOn(
                        pClass,
                        pClass.getLabel() + UNDERSCORE
                            + ESFArchitectureConceptsDiagramSet.ESFARCHITECTURECONCEPTS_DIAGRAM_SHORT_NAME
                            + getAllDiagramsOf(pClass).size());
                }
            };

        if (vCeateDiagramCmd.canExecute()) {
            try {
                pDomain.getCommandStack().execute(vCeateDiagramCmd);
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    /**
     *
     * @param pClass Class stereotyped by SBlock
     * @return list of ESF Architecture Concepts Diagram
     */
    protected static List<Diagram> getAllDiagramsOf(Class pClass) {
        List<Diagram> vDiagramsList = new ArrayList<Diagram>();

        for (Diagram vDiagram : getAllDiagramsFromModel()) {
            if ((DiagramUtils.getOwner(vDiagram) == pClass) && (isESFArchitectureConceptsDiagram(vDiagram))) {
                vDiagramsList.add(vDiagram);
            }
        }
        return vDiagramsList;
    }

    /**
     * Get ESF Architecture Concept Diagram Prototype.
     *
     * @return ESFArchitectureConceptsDiagram Prototype
     */
    private static ViewPrototype getESFArchitectureConceptsDiagramPrototype(TransactionalEditingDomain domain) {
        ViewPrototype vDiagramPrototype = null;

        for (ViewPrototype vVP : PolicyChecker.getFor(domain.getResourceSet()).getAllPrototypes()) {
            if (vVP.getLabel().matches(ESFArchitectureConceptsDiagramSet.ESFARCHITECTURECONCEPTS_DIAGRAM)) {
                vDiagramPrototype = vVP;
            }
        }
        return vDiagramPrototype;
    }

    /**
     *
     * @param pDiagram
     * @return
     */
    public static boolean isESFArchitectureConceptsDiagram(Diagram pDiagram) {
        ViewPrototype vVp = DiagramUtils.getPrototype(pDiagram);
        boolean vIsESFArchitectureConceptsDiagram = false;

        if (vVp.getLabel().matches(ESFArchitectureConceptsDiagramSet.ESFARCHITECTURECONCEPTS_DIAGRAM)) {
            vIsESFArchitectureConceptsDiagram = true;
        }
        return vIsESFArchitectureConceptsDiagram;
    }

    /**
     *
     * @return
     */
    public static List<Diagram> getAllDiagramsFromModel() {
        List<Diagram> diagramsList = new ArrayList<Diagram>();
        Resource resource = getResource();

        if (resource != null) {
            int size = resource.getContents().size();
            for (int index = size - 1; index >= 0; index--) {
                EObject obj = resource.getContents().get(index);
                if (obj instanceof Diagram) {
                     diagramsList.add((Diagram)obj);
                }
            }
        }
        return diagramsList;
    }

    /**
     *
     * @return
     */
    private static Resource getResource() {
        NotationModel notationModel = NotationUtils.getNotationModel();
        if (notationModel != null) {
            return notationModel.getResource();
        }
        return null;
    }

}
