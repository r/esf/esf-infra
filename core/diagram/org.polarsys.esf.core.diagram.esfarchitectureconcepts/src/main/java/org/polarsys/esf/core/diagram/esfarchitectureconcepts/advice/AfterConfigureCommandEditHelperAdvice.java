/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.advice;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;

/**
 * The EditHelperAdvice class used for SPart.
 * Get the command from graphical element type.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class AfterConfigureCommandEditHelperAdvice
    extends AbstractEditHelperAdvice {

    /** Parameter id used to pass the command for setting the type of the property. */
    public static final String AFTER_CONFIGURE_COMMAND = "AFTER_CONFIGURE_COMMAND"; //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public AfterConfigureCommandEditHelperAdvice() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ICommand getAfterConfigureCommand(final ConfigureRequest pRequest) {
        Object vParameter = pRequest.getParameter(AFTER_CONFIGURE_COMMAND);
        ICommand vAfterConfigureCommand =  null;

        if (vParameter instanceof ICommand) {
            vAfterConfigureCommand = (ICommand) vParameter;
        } else {
            vAfterConfigureCommand = super.getAfterConfigureCommand(pRequest);
        }
        return vAfterConfigureCommand;
    }
}
