/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.advice;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.uml.diagram.common.dialogs.CreateOrSelectTypeDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.polarsys.esf.core.diagram.esfarchitectureconcepts.command.ConfigureSPartCommand;
import org.polarsys.esf.core.diagram.esfarchitectureconcepts.dialog.CreateOrSelectSBlockPropertyTypeDialog;

/**
 * Add a popup to create or select a SBlock for the property type of the element (SPart) using this advice.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class CreateOrSelectSBlockPropertyTypeEditHelperAdvice
    extends AbstractEditHelperAdvice {

    /**
     * Default constructor.
     */
    public CreateOrSelectSBlockPropertyTypeEditHelperAdvice() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ICommand getBeforeConfigureCommand(final ConfigureRequest pRequest) {
        configureRequest(pRequest);
        return super.getBeforeConfigureCommand(pRequest);
    }

    /**
     * @param pRequest ConfigureRequest
     */
    protected void configureRequest(final ConfigureRequest pRequest) {
        Map<String, Object> vNewParameters = new HashMap<String, Object>();

        ICommand vConfigureCommand = null;

        Shell vShell = Display.getDefault().getActiveShell();
        Property vProperty = (Property) pRequest.getElementToConfigure();
        Package vOwner = vProperty.getNearestPackage();

        CreateOrSelectTypeDialog vDialog = new CreateOrSelectSBlockPropertyTypeDialog(vShell, vOwner);

        vDialog.open();
        if (vDialog.getReturnCode() == Window.OK) {

            final ICommand vTypeCreationCommand = vDialog.getNewTypeCreateCommand();
            final Type vPartType = (Type) vDialog.getExistingType();

            // Abort if type creation command exists but is not executable
            if ((vTypeCreationCommand != null) && (!vTypeCreationCommand.canExecute())) {
                vConfigureCommand = UnexecutableCommand.INSTANCE;
            } else {
                vConfigureCommand = CompositeCommand.compose(vConfigureCommand, vTypeCreationCommand);
            }

            // Create the configure command that will set the constraint property type
            ICommand vSetTypeCommand = new ConfigureSPartCommand(pRequest, vPartType, vTypeCreationCommand);
            vConfigureCommand = CompositeCommand.compose(vConfigureCommand, vSetTypeCommand);
        } else {
            throw new OperationCanceledException("Creation cancelled by user"); //$NON-NLS-1$
        }

        vNewParameters.put(AfterConfigureCommandEditHelperAdvice.AFTER_CONFIGURE_COMMAND, vConfigureCommand);
        pRequest.addParameters(vNewParameters);
    }
}
