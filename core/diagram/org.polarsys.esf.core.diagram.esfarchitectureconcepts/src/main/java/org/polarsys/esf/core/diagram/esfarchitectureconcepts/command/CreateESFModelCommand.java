/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/

package org.polarsys.esf.core.diagram.esfarchitectureconcepts.command;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.papyrus.infra.architecture.commands.IModelCreationCommand;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.uml2.uml.UMLFactory;
import org.polarsys.esf.core.diagram.esfarchitectureconcepts.ESFArchitectureConceptsDiagramActivator;


/**
 * The Class CreateSysMLModelCommand.
 */
public class CreateESFModelCommand implements IModelCreationCommand {

	@Override
	public void createModel(final ModelSet modelSet) {
		runAsTransaction(modelSet);
	}

	protected void runAsTransaction(final ModelSet modelSet) {
		// Get the UML element to which the newly created diagram will be
		// attached.
		// Create the diagram
		final Resource modelResource = UmlUtils.getUmlResource(modelSet);
		TransactionalEditingDomain editingDomain = modelSet.getTransactionalEditingDomain();

		URI origURI = modelResource.getURI();
		try {
			modelResource.setURI(URI.createURI("platform:/plugin/org.polarsys.esf.core.diagram.esfarchitectureconcepts/src/main/resources/template/esf-template.uml"));
			modelResource.load(null);
		}
		catch (IOException e) {
			ESFArchitectureConceptsDiagramActivator.log.error(e);
			// template loading failed, fall-back to programmatic creation
			AbstractTransactionalCommand command = new AbstractTransactionalCommand(editingDomain, "Initialize model", Collections.EMPTY_LIST) {

				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					EObject model = getRootElement(modelResource);
					attachModelToResource(model, modelResource);

					return CommandResult.newOKCommandResult();
				}
			};
			editingDomain.getCommandStack().execute(new GMFtoEMFCommandWrapper(command));
		}
		finally {
			modelResource.setURI(origURI);
		}
	}

	protected EObject getRootElement(Resource modelResource) {
		EObject rootElement = null;
		if (modelResource != null && modelResource.getContents() != null && modelResource.getContents().size() > 0) {
			Object root = modelResource.getContents().get(0);
			if (root instanceof EObject) {
				rootElement = (EObject) root;
			}
		} else {
			rootElement = createRootElement();
		}
		return rootElement;
	}

	protected EObject createRootElement() {
		return UMLFactory.eINSTANCE.createModel();
	}

	protected void attachModelToResource(EObject root, Resource resource) {
		resource.getContents().add(root);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected String getModelName() {
		return "RobMoSysModel"; //$NON-NLS-1$
	}
}
