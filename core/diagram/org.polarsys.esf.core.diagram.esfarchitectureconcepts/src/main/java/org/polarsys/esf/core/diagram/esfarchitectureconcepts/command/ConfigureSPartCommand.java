/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.command;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.services.edit.utils.GMFCommandUtils;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

/**
 * Configure a SPart with the created/selected type.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ConfigureSPartCommand
    extends ConfigureElementCommand {

    /** Type of SPart. */
    private Type mSPartType;

    /** Command of type creation. */
    private ICommand mTypeCreationCommand;

    /**
     * Constructor.
     *
     * @param pRequest configuration request
     * @param pSPartType the featuring type of the SPart
     * @param pTypeCreationCommand if needed create the type
     */
    public ConfigureSPartCommand(
        final ConfigureRequest pRequest,
        final Type pSPartType,
        final ICommand pTypeCreationCommand) {
        super(pRequest);
        this.mSPartType = pSPartType;
        this.mTypeCreationCommand = pTypeCreationCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CommandResult doExecuteWithResult(final IProgressMonitor pMonitor, final IAdaptable pInfo)
        throws ExecutionException {

        Property vSPart = (Property) getElementToEdit();
        if (mSPartType != null) {
            vSPart.setType(mSPartType);
        } else {
            Type vNewType = (Type) GMFCommandUtils.getCommandEObjectResult(mTypeCreationCommand);
            vSPart.setType(vNewType);
        }
        return CommandResult.newOKCommandResult(vSPart);
    }

}
