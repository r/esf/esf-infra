/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.diagram.esfarchitectureconcepts.tester;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;

/**
 * Class dedicated to verify if it is possible to create a ESF Architecture Concepts Diagram.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFArchitectureConceptsDiagramTester
    extends PropertyTester {

    private static final String IS_SBLOCK_SPART = "isSBlockOrSPart"; //$NON-NLS-1$

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean test(Object pReceiver, String pProperty, Object[] pArgs, Object pExpectedValue) {
        if (IS_SBLOCK_SPART.equals(pProperty) && pReceiver instanceof IStructuredSelection) {
            boolean vAnswer = isSBlockOrSPart((IStructuredSelection) pReceiver);
            return new Boolean(vAnswer).equals(pExpectedValue);
        }

        return false;
    }

    /**
     *
     * @param pSelection
     * @return
     */
    private boolean isSBlockOrSPart(IStructuredSelection pSelection) {
        boolean vAnswer = false;
        final Object vFirst =pSelection.getFirstElement();
        if (vFirst instanceof EditPart) {
            EditPart vParent = (EditPart) vFirst;
            if (vParent.getModel() instanceof View) {
                View vView = (View)vParent.getModel();
                if (vView.getElement() instanceof Property) {
                    Property vProperty = (Property)vView.getElement();
                    if (UMLUtil.getStereotypeApplication(vProperty, ISPart.class) != null) {
                        vAnswer = true;
                    }
                } else if (vView.getElement() instanceof Class) {
                    Class vClass = (Class)vView.getElement();
                    if (UMLUtil.getStereotypeApplication(vClass, ISBlock.class) != null) {
                        vAnswer = true;
                    }
                }
            }
        }
        return vAnswer;
    }

}
