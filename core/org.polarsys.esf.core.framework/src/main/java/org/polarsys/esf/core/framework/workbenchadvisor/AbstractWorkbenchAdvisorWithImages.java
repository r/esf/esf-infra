/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.framework.workbenchadvisor;

import java.net.URL;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.ide.IDEInternalWorkbenchImages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.osgi.framework.Bundle;
import org.polarsys.esf.core.framework.CoreFrameworkActivator;

/**
 * Methods copied from {@link org.eclipse.ui.internal.ide.IDEWorkbenchAdvisor},
 * to build a default workbench advisor which registered all the shared images.
 *
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
@SuppressWarnings("restriction")
public abstract class AbstractWorkbenchAdvisorWithImages
    extends WorkbenchAdvisor {

    /** Paths for the icons. */
    private static final String PATH_ICONS = "$nl$/icons/full/"; //$NON-NLS-1$

    /** Paths for the local tools enabled images. */
    private static final String PATH_ENABLED_LOCAL_TOOLS = PATH_ICONS + "elcl16/"; //$NON-NLS-1$

    /** Paths for the local tools disabled images. */
    private static final String PATH_DISABLED_LOCAL_TOOLS = PATH_ICONS + "dlcl16/"; // //$NON-NLS-1$

    /** Paths for the local tools enabled images. */
    private static final String PATH_ENABLED_TOOLS = PATH_ICONS + "etool16/"; // //$NON-NLS-1$

    /** Paths for the local tools disabled images. */
    private static final String PATH_DISABLED_TOOLS = PATH_ICONS + "dtool16/"; // //$NON-NLS-1$

    /** Paths for model objects images. */
    private static final String PATH_MODEL_OBJECTS = PATH_ICONS + "obj16/"; //$NON-NLS-1$

    /** Paths for model objects images. */
    private static final String PATH_WIZARD_BANS = PATH_ICONS + "wizban/"; //$NON-NLS-1$

    /** IDEBundle in which the image will be declared. */
    private Bundle mIDEBundle = Platform.getBundle(IDEWorkbenchPlugin.IDE_WORKBENCH);

    /**
     * Default constructor.
     */
    public AbstractWorkbenchAdvisorWithImages() {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * Use a navigator root as default input for all the workbench pages created
     * without any initial input. This will try to find the right adapter according
     * to what is expected as input for this page.
     */
    @Override
    public IAdaptable getDefaultPageInput() {
        return new NavigatorRoot();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final IWorkbenchConfigurer pConfigurer) {
        // Call the parent method
        super.initialize(pConfigurer);

        // Enable the workbench save on exit, and its automatic restore
        // NB : When this function is active, the method createInitialLayout
        // in the perspective will be called only on the first application launch.
        // Thus, it's recommended to prepare an menu to reset the perspective
        pConfigurer.setSaveAndRestore(true);

        // Declare the shared and internal images
        declareInternalWorkbenchImages();
        declareSharedWorkbenchImages();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean preShutdown() {

        try {   
            // Save the full workspace before quit
            ResourcesPlugin.getWorkspace().save(true, null);
        } catch (final CoreException pException) {
            CoreFrameworkActivator.logError("Error during the shutdown action", pException); //$NON-NLS-1$
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void preStartup() {
        IDE.registerAdapters();
    }

    /**
     * Declares all the internal IDE-specific workbench images. This includes and internal images named in
     * {@link org.eclipse.ui.internal.ide.IDEInternalWorkbenchImages}.
     *
     * @see org.eclipse.ui.internal.ide.IDEWorkbenchAdvisor#declareImage
     */
    private void declareInternalWorkbenchImages() {

        // Declare the internal images ...
        // ... For the build
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_ETOOL_BUILD_EXEC,
            PATH_ENABLED_TOOLS + "build_exec.gif", //$NON-NLS-1$
            false);
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ETOOL_BUILD_EXEC_HOVER, PATH_ENABLED_TOOLS
            + "build_exec.gif", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ETOOL_BUILD_EXEC_DISABLED, PATH_DISABLED_TOOLS
            + "build_exec.gif", false); //$NON-NLS-1$

        // For the search
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_ETOOL_SEARCH_SRC,
            PATH_ENABLED_TOOLS + "search_src.gif", //$NON-NLS-1$
            false);
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ETOOL_SEARCH_SRC_HOVER, PATH_ENABLED_TOOLS
            + "search_src.gif", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ETOOL_SEARCH_SRC_DISABLED, PATH_DISABLED_TOOLS
            + "search_src.gif", false); //$NON-NLS-1$

        // ... For the navigation
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_ETOOL_NEXT_NAV,
            PATH_ENABLED_TOOLS + "next_nav.gif", //$NON-NLS-1$
            false);
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_ETOOL_PREVIOUS_NAV,
            PATH_ENABLED_TOOLS + "prev_nav.gif", //$NON-NLS-1$
            false);

        // ... For the project, folder and file creation
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_WIZBAN_NEWPRJ_WIZ,
            PATH_WIZARD_BANS + "newprj_wiz.png", //$NON-NLS-1$
            false);
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_NEWFOLDER_WIZ, PATH_WIZARD_BANS
            + "newfolder_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_WIZBAN_NEWFILE_WIZ,
            PATH_WIZARD_BANS + "newfile_wiz.png", //$NON-NLS-1$
            false);

        // ... For the import, export, resource and save wizard and dialog
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_IMPORTDIR_WIZ, PATH_WIZARD_BANS
            + "importdir_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_IMPORTZIP_WIZ, PATH_WIZARD_BANS
            + "importzip_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_EXPORTDIR_WIZ, PATH_WIZARD_BANS
            + "exportdir_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_EXPORTZIP_WIZ, PATH_WIZARD_BANS
            + "exportzip_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_WIZBAN_RESOURCEWORKINGSET_WIZ, PATH_WIZARD_BANS
            + "workset_wiz.png", false); //$NON-NLS-1$
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_DLGBAN_SAVEAS_DLG,
            PATH_WIZARD_BANS + "saveas_wiz.png", //$NON-NLS-1$
            false);
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_DLGBAN_QUICKFIX_DLG,
            PATH_WIZARD_BANS + "quick_fix.png", //$NON-NLS-1$
            false);
    }

    /**
     * Declares all shared IDE-specific workbench images. This includes images named in {@link IDE.SharedImages}
     *
     * @see org.eclipse.ui.internal.ide.IDEWorkbenchAdvisor#declareImage
     */
    private void declareSharedWorkbenchImages() {
        // Declare the shared images ...
        // ... For the projects
        declareWorkbenchImage(
            IDE.SharedImages.IMG_OBJ_PROJECT,
            PATH_MODEL_OBJECTS + "prj_obj.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDE.SharedImages.IMG_OBJ_PROJECT_CLOSED,
            PATH_MODEL_OBJECTS + "cprj_obj.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDE.SharedImages.IMG_OPEN_MARKER,
            PATH_ENABLED_LOCAL_TOOLS + "gotoobj_tsk.gif", //$NON-NLS-1$
            true);

        // ... For the quick fix
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ELCL_QUICK_FIX_ENABLED, PATH_ENABLED_LOCAL_TOOLS
            + "smartmode_co.gif", true); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_DLCL_QUICK_FIX_DISABLED, PATH_DISABLED_LOCAL_TOOLS
            + "smartmode_co.gif", true); //$NON-NLS-1$

        // ... For the task and bookmark
        declareWorkbenchImage(
            IDE.SharedImages.IMG_OBJS_TASK_TSK,
            PATH_MODEL_OBJECTS + "taskmrk_tsk.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDE.SharedImages.IMG_OBJS_BKMRK_TSK,
            PATH_MODEL_OBJECTS + "bkmrk_tsk.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_OBJS_COMPLETE_TSK,
            PATH_MODEL_OBJECTS + "complete_tsk.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_OBJS_INCOMPLETE_TSK, PATH_MODEL_OBJECTS
            + "incomplete_tsk.gif", true); //$NON-NLS-1$

        // ... For the welcome facilities
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_OBJS_WELCOME_ITEM,
            PATH_MODEL_OBJECTS + "welcome_item.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_OBJS_WELCOME_BANNER, PATH_MODEL_OBJECTS
            + "welcome_banner.gif", true); //$NON-NLS-1$

        // ... For the log tasks
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_OBJS_ERROR_PATH,
            PATH_MODEL_OBJECTS + "error_tsk.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_OBJS_WARNING_PATH,
            PATH_MODEL_OBJECTS + "warn_tsk.gif", //$NON-NLS-1$
            true);
        declareWorkbenchImage(
            IDEInternalWorkbenchImages.IMG_OBJS_INFO_PATH,
            PATH_MODEL_OBJECTS + "info_tsk.gif", //$NON-NLS-1$
            true);

        // ... For the layout
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_LCL_FLAT_LAYOUT, PATH_ENABLED_LOCAL_TOOLS
            + "flatLayout.gif", true); //$NON-NLS-1$
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_LCL_HIERARCHICAL_LAYOUT, PATH_ENABLED_LOCAL_TOOLS
            + "hierarchicalLayout.gif", true); //$NON-NLS-1$

        // ... Others
        declareWorkbenchImage(IDEInternalWorkbenchImages.IMG_ETOOL_PROBLEM_CATEGORY, PATH_ENABLED_TOOLS
            + "problem_category.gif", true); //$NON-NLS-1$
    }

    /**
     * Declares an IDE-specific workbench image.
     *
     * @param pSymbolicName The symbolic name of the image
     * @param pPath The path of the image file; this path is relative to the base of the IDE plug-in
     * @param pShared <code>true</code> if this is a shared image, and <code>false</code> if this is not a shared image
     * @see org.eclipse.ui.internal.ide.IDEWorkbenchAdvisor#declareImage
     */
    private void declareWorkbenchImage(final String pSymbolicName, final String pPath, final boolean pShared) {
        // Get the URL of the image search with its path
        URL vURL = FileLocator.find(mIDEBundle, new Path(pPath), null);

        // Build the image descriptor for this URL
        ImageDescriptor vImageDesc = ImageDescriptor.createFromURL(vURL);

        // Declare the image in the workbench
        getWorkbenchConfigurer().declareImage(pSymbolicName, vImageDesc, pShared);
    }
}
