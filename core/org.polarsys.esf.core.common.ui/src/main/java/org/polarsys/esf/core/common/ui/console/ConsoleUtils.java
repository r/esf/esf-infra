/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.console;

import java.io.PrintStream;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * 
 * Console Utils used with console view.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class ConsoleUtils {

    /**
     * Keep trace of default output stream, to restore it if needed.
     */
    private static final PrintStream DEFAULT_OUTSTREAM = System.out;

    /**
     * Keep trace of default ouput error stream, to restore it if needed.
     */
    private static final PrintStream DEFAULT_ERRSTREAM = System.err;

    /**
     * Console title.
     */
    private static final String CONSOLE_TITLE = "Console"; //$NON-NLS-1$

    /**
     * Private constructor to avoid instantiation.
     */
    private ConsoleUtils() {
    }

    /**
     * Reroute default output stream to a new console, added to console view.
     */
    public static void linkDefaultOutStreamToConsole() {
        // Create a msg console.
        final MessageConsole vConsole = new MessageConsole(CONSOLE_TITLE, null);

        // Add it to console manager. If "equivalent" console already exists, does not seem to add it.
        ConsolePlugin.getDefault().getConsoleManager().addConsoles(
            new IConsole[] {vConsole });

        final MessageConsoleStream vStream = vConsole.newMessageStream();

        PrintStream vPrintStream = new PrintStream(vStream);

        // Link standard output stream to the console.
        System.setOut(vPrintStream);

        // Link error output stream to the console.
        System.setErr(vPrintStream);

    }

    /**
     * Reset output stream to system one.
     */
    public static void unlinkDefaultOutStreamToConsole() {

        System.setOut(DEFAULT_OUTSTREAM);
        System.setErr(DEFAULT_ERRSTREAM);
    }

}
