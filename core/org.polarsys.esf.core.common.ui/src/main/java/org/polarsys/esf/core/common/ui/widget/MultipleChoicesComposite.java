/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.ui.celleditor.ExtendedComboBoxCellEditor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ItemProvider;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PatternFilter;
import org.polarsys.esf.core.common.observer.IObserver;
import org.polarsys.esf.core.common.ui.CommonUIActivator;
import org.polarsys.esf.core.common.ui.CommonUIActivator.Implementation;

/**
 *
 * Composite with dual choice list, add, remove, up and down button.
 * Inspired from multipleChoiceDialog Class (IBM).
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class MultipleChoicesComposite
    extends Composite {

    /**
     * Image descriptor for up button.
     */
    private static final Image IMG_UP = CommonUIActivator.getPlugin().getImageRegistry()
        .getDescriptor(Implementation.ICON_UP_IMG_KEY).createImage();

    /**
     * Image descriptor for down button.
     */
    private static final Image IMG_DOWN = CommonUIActivator.getPlugin().getImageRegistry()
        .getDescriptor(Implementation.ICON_DOWN_IMG_KEY).createImage();

    /**
     * Image descriptor for add button.
     */
    private static final Image IMG_ADD = CommonUIActivator.getPlugin().getImageRegistry()
        .getDescriptor(Implementation.ICON_ADD_IMG_KEY).createImage();

    /**
     * Image descriptor for remove button.
     */
    private static final Image IMG_RMV = CommonUIActivator.getPlugin().getImageRegistry()
        .getDescriptor(Implementation.ICON_REMOVE_IMG_KEY).createImage();

    /**
     * Title of left list.
     */
    private String mLeftListTitle = null;

    /**
     * Title of right list.
     */
    private String mRightListTitle = null;

    /**
     * Label provider for contents in list.
     */
    private ILabelProvider mLabelProvider = null;

    /**
     * Wrapper source (left) table.
     */
    private TableViewer mLeftTableViewer = null;

    /**
     * Item provider for available (left) list.
     */
    private ItemProvider mLeftItemProvider = null;

    /**
     * Convenient provider for table viewer.
     */
    private IContentProvider mLeftContentProvider = null;

    /**
     * Convenient provider for table viewer.
     */
    private IContentProvider mRightContentProvider = null;

    /**
     * Item provider for results (right) list.
     */
    private ItemProvider mRightItemProvider = null;

    /**
     * Wrapper for results (right) table.
     */
    private TableViewer mRightTableViewer = null;

    /**
     * Add button.
     */
    private Button mAddButton = null;

    /**
     * Remove button.
     */
    private Button mRemoveButton = null;

    /**
     * Up Button.
     */
    private Button mUpButton = null;

    /**
     * Down Button.
     */
    private Button mDownButton = null;

    /**
     * Observer updated when a modification is done.
     */
    private IObserver mObserver = null;

    /** Right choices is sort. */
    private boolean mSortChoices = false;

    /** Filter field to filter left column. */
    private Text mFilterField = null;

    /**
     * Default constructor.
     *
     * @param pParent composite where this one will be anchored.
     * @param pLabelProvider Provider for label of object in list.
     * @param pSortChoices decide if lists must be sorted or not.
     * @param pLeftListTitle title for left list.
     * @param pRightListTitle title for right list.
     * @param pObserver Observer of this window.
     */
    public MultipleChoicesComposite(
        final Composite pParent,
        final ILabelProvider pLabelProvider,
        final boolean pSortChoices,
        final String pLeftListTitle,
        final String pRightListTitle,
        final IObserver pObserver) {

        // Instantiate composite, and link it to support parent.
        super(pParent, SWT.NULL);

        // Instantiate members.
        mLabelProvider = pLabelProvider;
        mRightListTitle = pRightListTitle;
        mLeftListTitle = pLeftListTitle;
        mSortChoices = pSortChoices;
        mObserver = pObserver;

        // Now create visual part.
        createComposite();
    }

    /**
     * Fill parent composite with two table, a filter, and add/remove/up/down buttons. React to double click.
     */
    private void createComposite() {

        // Instantiate layout for main panel.
        GridLayout vContentsLayout = new GridLayout();
        vContentsLayout.numColumns = 3;

        // Set it on main composite panel.
        setLayout(vContentsLayout);

        // Create a group (subclass of composite) for filter part, which contains text and label.
        createFilterGroup(this);

        // Create left composite, with available values list.
        createLeftComposite(this);

        // Create center composite, with button.
        createCenterComposite(this);

        // Create right composite, with results values list.
        createRightComposite(this);

        computeButtonStatus();

    }

    /**
     * Create filter group.
     *
     * @param pParent parent target for the widget created.
     */
    private void createFilterGroup(final Composite pParent) {
        Group vFilterGroupComposite = new Group(pParent, SWT.NONE);

        vFilterGroupComposite.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Choices_pattern_group")); //$NON-NLS-1$

        // Set layout and data for parent layout.
        vFilterGroupComposite.setLayout(new GridLayout(2, false));
        vFilterGroupComposite.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));

        // Add label to group
        Label vLabel = new Label(vFilterGroupComposite, SWT.NONE);
        vLabel.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Choices_pattern_label")); //$NON-NLS-1$

        // Add text area to group
        // vPatternText is the high part of the window, used to fill filter.
        mFilterField = new Text(vFilterGroupComposite, SWT.BORDER);
        mFilterField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }

    /**
     * Create left composite.
     *
     * @param pParent parent for this widget
     */
    private void createLeftComposite(final Composite pParent) {

        // Now create left composite (table of available values)
        Composite vLeftComposite = new Composite(pParent, SWT.NONE);

        // Set layout data for parent composite
        GridData vData = new GridData(SWT.FILL, SWT.FILL, true, true);
        vData.horizontalAlignment = SWT.END;
        vLeftComposite.setLayoutData(vData);

        // Create a layout for current composite
        final GridLayout vLayout = new GridLayout();
        vData.horizontalAlignment = SWT.FILL;
        vLayout.marginHeight = 0;
        vLayout.marginWidth = 0;
        vLayout.numColumns = 1;
        vLeftComposite.setLayout(vLayout);

        // Create label for table
        createLeftColumnLabel(vLeftComposite);

        // Create Table
        createLeftColumn(vLeftComposite);

    }

    /**
     * Create left column for left side composite.
     *
     * @param pParent parent target for the widget created.
     */
    private void createLeftColumn(final Composite pParent) {
        Table vAvailableValuesTable = new Table(pParent, SWT.MULTI | SWT.BORDER);

        // Add layout data
        GridData vLeftTableGridData = new GridData();
        vLeftTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
        vLeftTableGridData.heightHint = Display.getCurrent().getBounds().height / 3;
        vLeftTableGridData.verticalAlignment = SWT.FILL;
        vLeftTableGridData.horizontalAlignment = SWT.FILL;
        vLeftTableGridData.grabExcessHorizontalSpace = true;
        vLeftTableGridData.grabExcessVerticalSpace = true;
        vAvailableValuesTable.setLayoutData(vLeftTableGridData);

        // Create viewer for this table.
        mLeftTableViewer = new TableViewer(vAvailableValuesTable);

        // Create Filter.
        associateFilterToColumn();

        // Add listener for double click.
        mLeftTableViewer.addDoubleClickListener(new IDoubleClickListener() {

            @Override
            public void doubleClick(final DoubleClickEvent pEvent) {
                if (mAddButton.isEnabled()) {
                    mAddButton.notifyListeners(SWT.Selection, null);
                }
            }
        });

        // Add listener to enable/disable button
        mLeftTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

            @Override
            public void selectionChanged(final SelectionChangedEvent pEvent) {
                // Refresh button enable status
                computeButtonStatus();

            }
        });
    }

    /**
     * Associate filter to left column.
     */
    private void associateFilterToColumn() {
        final PatternFilter vFilter = new PatternFilter() {

            @Override
            protected boolean isParentMatch(final Viewer pViewer, final Object pElement) {
                return (pViewer instanceof AbstractTreeViewer) && super.isParentMatch(pViewer, pElement);
            }
        };

        // Add filter to this viewer
        mLeftTableViewer.addFilter(vFilter);

        // Add listener to refresh viewer when text of filter is modified.
        mFilterField.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(final ModifyEvent pEvent) {
                vFilter.setPattern(((Text) pEvent.widget).getText());
                mLeftTableViewer.refresh();
            }
        });
    }

    /**
     * Create label for left column.
     *
     * @param pParent parent target for the widget created.
     */
    private void createLeftColumnLabel(final Composite pParent) {
        Label vLeftTableTitleLabel = new Label(pParent, SWT.NONE);
        vLeftTableTitleLabel.setText(mLeftListTitle);

        // Add grid data
        GridData vLeftTableTitleLabelGridData = new GridData();
        vLeftTableTitleLabelGridData.verticalAlignment = SWT.FILL;
        vLeftTableTitleLabelGridData.horizontalAlignment = SWT.FILL;
        vLeftTableTitleLabel.setLayoutData(vLeftTableTitleLabelGridData);
    }

    /**
     * Create right composite (results table).
     *
     * @param pParent where composite will be push.
     */
    private void createRightComposite(final Composite pParent) {

        // Create composite and push it on parent panel.
        Composite vRightComposite = new Composite(pParent, SWT.NONE);

        // Add grid data for parent layout.
        GridData vData = new GridData(SWT.FILL, SWT.FILL, true, true);
        vData.horizontalAlignment = SWT.END;
        vRightComposite.setLayoutData(vData);

        // Create a layout for this composite.
        final GridLayout vLayout = new GridLayout();
        vData.horizontalAlignment = SWT.FILL;
        vLayout.marginHeight = 0;
        vLayout.marginWidth = 0;
        vLayout.numColumns = 1;
        vRightComposite.setLayout(vLayout);

        // Create title label for right list
        createRightLabelColumn(vRightComposite);

        // Create table
        createRightColumn(vRightComposite);

    }

    /**
     * Create right column for composite.
     *
     * @param pParent parent target for the widget created.
     */
    private void createRightColumn(final Composite pParent) {
        Table vRightTable = new Table(pParent, SWT.MULTI | SWT.BORDER);

        // Create grid data for this table
        GridData vRightTableGridData = new GridData();
        vRightTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
        vRightTableGridData.heightHint = Display.getCurrent().getBounds().height / 3;
        vRightTableGridData.verticalAlignment = SWT.FILL;
        vRightTableGridData.horizontalAlignment = SWT.FILL;
        vRightTableGridData.grabExcessHorizontalSpace = true;
        vRightTableGridData.grabExcessVerticalSpace = true;
        vRightTable.setLayoutData(vRightTableGridData);

        // Create a viewer for results table
        mRightTableViewer = new TableViewer(vRightTable);

        mRightTableViewer.addDoubleClickListener(new IDoubleClickListener() {

            @Override
            public void doubleClick(final DoubleClickEvent pEvent) {
                if (mRemoveButton.isEnabled()) {
                    mRemoveButton.notifyListeners(SWT.Selection, null);
                }
            }
        });

        // Add listener to enable/disable button
        mRightTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

            @Override
            public void selectionChanged(final SelectionChangedEvent pEvent) {
                // Refresh button enable status
                computeButtonStatus();

            }
        });
    }

    /**
     * Create label for right column.
     *
     * @param pParent parent target for the widget created.
     */
    private void createRightLabelColumn(final Composite pParent) {
        final Label vRightListTitleLabel = new Label(pParent, SWT.NONE);
        vRightListTitleLabel.setText(mRightListTitle);

        // Create grid data for this title.
        final GridData vRightListTitleLabelGridData = new GridData();
        vRightListTitleLabelGridData.horizontalSpan = 1;
        vRightListTitleLabelGridData.horizontalAlignment = SWT.FILL;
        vRightListTitleLabelGridData.verticalAlignment = SWT.FILL;
        vRightListTitleLabel.setLayoutData(vRightListTitleLabelGridData);
    }

    /**
     * Create center composite (button).
     *
     * @param pParent where composite will be push.
     */
    private void createCenterComposite(final Composite pParent) {

        // Instantiate center composite and put it on main panel.
        Composite vControlButtons = new Composite(pParent, SWT.NONE);

        // Add data for parent layout.
        GridData vControlButtonsGridData = new GridData();
        vControlButtonsGridData.verticalAlignment = SWT.FILL;
        vControlButtonsGridData.horizontalAlignment = SWT.FILL;
        vControlButtons.setLayoutData(vControlButtonsGridData);

        // Create a layout for current composite.
        final GridLayout vControlsButtonGridLayout = new GridLayout();
        vControlButtons.setLayout(vControlsButtonGridLayout);

        // new Label(vControlButtons, SWT.NONE);
        // Create a space label to separate add/remove and up/down button
        Label vSpaceLabelOne = new Label(vControlButtons, SWT.NONE);
        GridData vSpaceLabelOneGridData = new GridData();
        vSpaceLabelOneGridData.verticalSpan = 2;
        vSpaceLabelOne.setLayoutData(vSpaceLabelOneGridData);

        // Create each button
        createAddButton(vControlButtons);
        createRemoveButton(vControlButtons);

        // Create a space label to separate add/remove and up/down button
        Label vSpaceLabelTwo = new Label(vControlButtons, SWT.NONE);
        GridData vSpaceLabelTwoGridData = new GridData();
        vSpaceLabelTwoGridData.verticalSpan = 2;
        vSpaceLabelTwo.setLayoutData(vSpaceLabelTwoGridData);

        createUpButton(vControlButtons);
        createDownButton(vControlButtons);
    }

    /**
     * Create add button (move selected items from available table to results table).
     *
     * @param pParent where button will be push.
     */
    private void createAddButton(final Composite pParent) {

        // Instantiate button
        mAddButton = new Button(pParent, SWT.PUSH);

        // Set icon
        mAddButton.setImage(IMG_ADD);

        // Add data grid
        final GridData vAddButtonGridData = new GridData();
        vAddButtonGridData.verticalAlignment = SWT.FILL;
        vAddButtonGridData.horizontalAlignment = SWT.FILL;
        mAddButton.setLayoutData(vAddButtonGridData);

        // Add listener
        mAddButton.addSelectionListener(new SelectionAdapter() {

            // Event is null when choiceTableViewer is double clicked
            @Override
            public void widgetSelected(final SelectionEvent pEvent) {

                // Get selected items
                final IStructuredSelection vSelection = (IStructuredSelection) mLeftTableViewer.getSelection();

                Iterator<?> vSelectionIterator = vSelection.iterator();

                // Iterate on it
                while (vSelectionIterator.hasNext()) {
                    Object vCurrentValue = vSelectionIterator.next();

                    if (!mRightItemProvider.getChildren().contains(vCurrentValue)) {
                        // Add current value to results provider
                        mRightItemProvider.getChildren().add(vCurrentValue);

                        // remove it form available values provider
                        mLeftItemProvider.getChildren().remove(vCurrentValue);

                    }
                }

                // Run asynchronous selection because Viewer refresh is asynchronous
                getDisplay().asyncExec(new Runnable() {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void run() {
                        mRightTableViewer.setSelection(vSelection);
                    }
                });

                // Notify observer
                notifyObserver();
            }

        });

    }

    /**
     * Create remove button (move selected items from results table to available table).
     *
     * @param pParent where button will be push.
     */
    private void createRemoveButton(final Composite pParent) {

        // Instantiate remove button and push it on central composite.
        mRemoveButton = new Button(pParent, SWT.PUSH);

        // Set icon
        mRemoveButton.setImage(IMG_RMV);

        // Add data layout
        final GridData vRemoveButtonGridData = new GridData();
        vRemoveButtonGridData.verticalAlignment = SWT.FILL;
        vRemoveButtonGridData.horizontalAlignment = SWT.FILL;
        mRemoveButton.setLayoutData(vRemoveButtonGridData);

        // Add listener
        mRemoveButton.addSelectionListener(new SelectionAdapter() {

            // event is null when Results TableViewer is double clicked
            @Override
            public void widgetSelected(final SelectionEvent pEvent) {
                final IStructuredSelection vSelection = (IStructuredSelection) mRightTableViewer.getSelection();
                Iterator<?> vSelectionIterator = vSelection.iterator();

                // Iterate on it
                while (vSelectionIterator.hasNext()) {
                    final Object vCurrentValue = vSelectionIterator.next();

                    // Remove it from results provider
                    mRightItemProvider.getChildren().remove(vCurrentValue);

                    // Add it to available provider
                    mLeftItemProvider.getChildren().add(vCurrentValue);

                }

                // Run asynchronous selection because Viewer refresh is asynchronous
                getDisplay().asyncExec(new Runnable() {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void run() {
                        mLeftTableViewer.setSelection(vSelection);
                    }
                });

                // Notify observer
                notifyObserver();
            }
        });

    }

    /**
     * Create up button, to change order of item in results list.
     *
     * @param pParent where button will be push.
     */
    private void createUpButton(final Composite pParent) {
        // Set up button
        mUpButton = new Button(pParent, SWT.PUSH);

        // Set icon
        mUpButton.setImage(IMG_UP);

        // set data layout
        GridData vUpButtonGridData = new GridData();
        vUpButtonGridData.verticalAlignment = SWT.FILL;
        vUpButtonGridData.horizontalAlignment = SWT.FILL;
        mUpButton.setLayoutData(vUpButtonGridData);

        // Add listener
        mUpButton.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(final SelectionEvent pEvent) {
                // Get Selection
                IStructuredSelection vSelection = (IStructuredSelection) mRightTableViewer.getSelection();

                // Iterate on it
                Iterator<?> vSelectionIterator = vSelection.iterator();

                // Estimate upper bound of list
                int vMinIndex = 0;
                EList<Object> vRightItemsList = mRightItemProvider.getChildren();

                while (vSelectionIterator.hasNext()) {
                    Object vCurrentValue = vSelectionIterator.next();

                    // Get current index
                    int vIndex = vRightItemsList.indexOf(vCurrentValue);

                    // Move item
                    vRightItemsList.move(Math.max(vIndex - 1, vMinIndex), vCurrentValue);

                }

                computeButtonStatus();
            }
        });

    }

    /**
     * Create down button, to change order of item in results list.
     *
     * @param pParent where button will be push.
     */
    private void createDownButton(final Composite pParent) {

        // Instantiate down button
        mDownButton = new Button(pParent, SWT.PUSH);

        // Set image
        mDownButton.setImage(IMG_DOWN);

        // Create data layout
        GridData vDownButtonGridData = new GridData();
        vDownButtonGridData.verticalAlignment = SWT.FILL;
        vDownButtonGridData.horizontalAlignment = SWT.FILL;
        mDownButton.setLayoutData(vDownButtonGridData);

        // Add selection listener
        mDownButton.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(final SelectionEvent pEvent) {
                // Get selected items
                IStructuredSelection vSelection = (IStructuredSelection) mRightTableViewer.getSelection();

                // Need to explore in right way
                Collections.reverse(vSelection.toList());

                // Iterate on it
                Iterator<?> vSelectionIterator = vSelection.iterator();

                // Estimate bottom bound of list
                EList<Object> vRigthItemsList = mRightItemProvider.getChildren();
                int vMaxIndex = vRigthItemsList.size() - 1;

                while (vSelectionIterator.hasNext()) {
                    Object vCurrentValue = vSelectionIterator.next();

                    // Get index of current items
                    int vIndex = vRigthItemsList.indexOf(vCurrentValue);

                    // Move it
                    vRigthItemsList.move(Math.min(vIndex + 1, vMaxIndex), vCurrentValue);

                }

                computeButtonStatus();
            }
        });
    }

    /**
     * Call this method to compute each button status (enabled / disabled) according to selection and/or table status.
     */
    private void computeButtonStatus() {

        // Add button is available if provider of available values is not empty.
        mAddButton.setEnabled(checkLeftColumn());

        // Remove button is available if provider of results is not empty.
        mRemoveButton.setEnabled(checkRightColumn());

        // Up Button is available if provider of results and selection are not empty.
        mUpButton.setEnabled(checkRankSort()
            && checkUpButton());

        // Up Button is available if provider of results and selection are not empty.
        mDownButton.setEnabled(checkRankSort()
            && checkDownButton());

    }

    /**
     * @return <code>true</code> if a selection exist in right column, otherwise <code>false</code>
     */
    private boolean checkUpButton() {
        IStructuredSelection vSelection = (IStructuredSelection) mRightTableViewer.getSelection();
        boolean vResult = !vSelection.isEmpty();

        // Case : one selection can not Up if it is on top of column
        if (vResult && vSelection.size() == 1) {
            EList<Object> vRightElements = mRightItemProvider.getChildren();
            vResult = vRightElements.indexOf(vSelection.getFirstElement()) != 0;
        }

        return vResult;
    }

    /**
     * @return <code>true</code> if a selection exist in right column, otherwise <code>false</code>
     */
    private boolean checkDownButton() {
        IStructuredSelection vSelection = (IStructuredSelection) mRightTableViewer.getSelection();
        boolean vResult = !vSelection.isEmpty();

        // Case : one selection can not Down if it is on bottom of column
        if (vResult && vSelection.size() == 1) {
            EList<Object> vRightElements = mRightItemProvider.getChildren();
            vResult = vRightElements.indexOf(vSelection.getFirstElement()) != (vRightElements.size() - 1);
        }

        return vResult;
    }

    /**
     * @return <code>true</code> if there are elements in right column, otherwise <code>false</code>
     */
    private boolean checkRightColumn() {
        return mRightItemProvider != null && !mRightItemProvider.getChildren().isEmpty();
    }

    /**
     * @return <code>true</code> if there are at least two elements in right column, otherwise <code>false</code>
     */
    private boolean checkRankSort() {
        return mRightItemProvider != null && mRightItemProvider.getChildren().size() > 1;
    }

    /**
     * @return <code>true</code> if there are children in left column, otherwise <code>false</code>
     */
    private boolean checkLeftColumn() {
        return mLeftItemProvider != null && !mLeftItemProvider.getChildren().isEmpty();
    }

    /**
     * Notify observer. Method call when results list is modified.
     *
     */
    private void notifyObserver() {
        if (mObserver != null) {
            mObserver.update(this);
        }
    }

    /**
     * Set left column input.
     */
    private void setLeftColumnInput() {
        // Associate content provider and viewer
        mLeftTableViewer.setContentProvider(mLeftContentProvider);

        // Set Label provider
        mLeftTableViewer.setLabelProvider(mLabelProvider);

        // Now set item provider.
        mLeftTableViewer.setInput(mLeftItemProvider);
    }

    /**
     * Set right column input.
     */
    private void setRightColumnInput() {

        // Set a content provider.
        mRightTableViewer.setContentProvider(mRightContentProvider);

        // IContentProvider vContentProvider = new AdapterFactoryContentProvider(new AdapterFactoryImpl());
        // vFeatureTableViewer.setContentProvider(vContentProvider);

        // Set a label provider
        mRightTableViewer.setLabelProvider(mLabelProvider);

        // This will call getElements in the content provider
        mRightTableViewer.setInput(mRightItemProvider);

    }

    /**
     * Set intput of composite.
     *
     * @param pLeftList Input for left column.
     * @param pRightList Input for right column.
     */
    public void setInputList(final List<? extends Object> pLeftList, final List<? extends Object> pRightList) {

        // Get a generic and empty adapter factory
        ComposedAdapterFactory vAdapterFactory = new ComposedAdapterFactory();
        // TODO : Change for the right adapter factory type
        vAdapterFactory.addAdapterFactory(new AdapterFactoryImpl());

        if (pLeftList != null) {
            // Order action will modify values list. Then it is necessary to create a new list, to not modify
            // one give in parameter.
            List<Object> vAvailableValuesList = new ArrayList<Object>(pLeftList);

            // To order entries, alphabetic order of Label provider contents is used.
            if (mSortChoices) {
                // This one ordered results.
                // This method directly modify List gave in parameter. (...)
                ExtendedComboBoxCellEditor.createItems(vAvailableValuesList, mLabelProvider, true);
            }

            // Get an item provider
            mLeftItemProvider = new ItemProvider(vAdapterFactory, vAvailableValuesList);

            // Get a content provider
            mLeftContentProvider = new AdapterFactoryContentProvider(vAdapterFactory);

            setLeftColumnInput();
        }

        if (pRightList != null) {
            // ItemProvider will create a new list.
            // Obtain an item provider for results list.
            mRightItemProvider = new ItemProvider(vAdapterFactory, pRightList);

            // Create a content provider which wraps adapterFactory and will be used by table viewer
            mRightContentProvider = new AdapterFactoryContentProvider(vAdapterFactory);

            setRightColumnInput();
        }

        computeButtonStatus();
    }

    /**
     * @return list of results values. Create a new list each time.
     */
    public List<Object> getResults() {
        return new ArrayList<Object>(mRightItemProvider.getChildren());
    }

    /**
     * @return list of Available values. Create a new list each time.
     */
    public List<Object> getAvailableValues() {
        return new ArrayList<Object>(mLeftItemProvider.getChildren());
    }

}
