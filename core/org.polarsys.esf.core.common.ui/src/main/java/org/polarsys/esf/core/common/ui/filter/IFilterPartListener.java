/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.filter;

import org.eclipse.ui.IWorkbenchPart;


/**
 * Interface for all the workbench parts which are listening to the changes on the filter text box.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IFilterPartListener extends IWorkbenchPart {

    /**
     * Method called when the filter text changed.
     * 
     * @param pText The new filter text
     */
    void filterTextChanged(String pText);
}
