/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.provider;

import org.eclipse.jface.action.ToolBarManager;

/**
 * An interface for all the editors which want to provide their own tool bar manager.
 * This allows for external classes (like the actions contributors) to access to the
 * toolbar manager getter even if they don't know their concrete implementation.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IToolBarManagerProvider {
    
    /**
     * Return the tool bar manager provided by the concrete instance.
     * @return The tool bar manager provided
     */
    ToolBarManager getToolBarManager();

}
