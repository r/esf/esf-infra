/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.statusline;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorActionBarContributor;
import org.polarsys.esf.core.common.ui.constants.ImageConstants;

/**
 * Utility class used to work with the status lines.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class StatusLinesUtil {

    /**
     * Private constructor, as it's a utility class.
     */
    private StatusLinesUtil() {
    }

    /**
     * Clear the message in the active status line.
     */
    public static void clearMessage() {
        // Get the active part if possible
        IWorkbenchPart vWorkbenchPart = 
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
        
        if (vWorkbenchPart != null) {
            // Clear the message
            outputMessage(vWorkbenchPart, null, IStatus.OK);
        }
    }
    
    /**
     * Clear the message in the status line of the given part.
     * @param pPart The part from which the status line is found
     */
    public static void clearMessage(final IWorkbenchPart pPart) {
        // Clear the message
        outputMessage(pPart, null, IStatus.OK);
    }
    
    /**
     * Outputs a message to the active part's status line. Does nothing if the
     * status line manager cannot be determined from the part.
     * The message severity is given in parameter, based on the constants
     * from {@link IStatus}.
     * <p>
     * Can be invoked from a non-UI thread.
     * 
     * @param pMessage The message to display
     * @param pSeverity The severity of the message (ie : IStatus.OK, etc.)
     */
    public static void outputMessage(final String pMessage, final int pSeverity) {
        // Get the active part if possible
        IWorkbenchPart vWorkbenchPart = 
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
        
        if (vWorkbenchPart != null) {
            // Display the message
            outputMessage(vWorkbenchPart, pMessage, pSeverity);
        }
    }
    
    /**
     * Outputs a message to the part's status line. Does nothing if the
     * status line manager cannot be determined from the part.
     * The message severity is given in parameter, based on the constants
     * from {@link IStatus}.
     * <p>
     * Can be invoked from a non-UI thread.
     * 
     * @param pPart The part from which the status line can be found
     * @param pMessage The message to display
     * @param pSeverity The severity of the message (ie : IStatus.OK, etc.)
     */
    public static void outputMessage(final IWorkbenchPart pPart, final String pMessage, final int pSeverity) {

        // Get the status line manager from the part
        final IStatusLineManager vStatusLineManager = getStatusLineManager(pPart);

        // Ensure that the status line manager can be found
        if (vStatusLineManager != null) {

            // Get the current display
            Display vWorkbenchDisplay = null;
            if (PlatformUI.isWorkbenchRunning()) {
                vWorkbenchDisplay = PlatformUI.getWorkbench().getDisplay();
            } else {
                vWorkbenchDisplay = Display.getDefault();
            }
            
            // Check if the current thread is an UI thread
            if (Thread.currentThread().equals(vWorkbenchDisplay.getThread())) {
                // Already on the UI thread, the message can be set directly
                StatusLinesUtil.setMessage(vStatusLineManager, pMessage, pSeverity);
            
            } else {
                // Not on the UI thread, must use an asynchronous update
                vWorkbenchDisplay.asyncExec(new Runnable() {
                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void run() {
                        // Set the message
                        StatusLinesUtil.setMessage(vStatusLineManager, pMessage, pSeverity);
                    }
                });
            }
        }
    }
    
    /**
     * 
     * @param pStatusLineManager The status line manager used to display the message.
     * @param pMessage The message to display
     * @param pSeverity The message severity
     */
    private static void setMessage(
        final IStatusLineManager pStatusLineManager, 
        final String pMessage, 
        final int pSeverity) {
        
        switch (pSeverity) {
            case IStatus.ERROR:
                pStatusLineManager.setErrorMessage(pMessage);
                break;

            case IStatus.WARNING:
                pStatusLineManager.setMessage(ImageConstants.ICON_WARNING, pMessage);
                break;
                
            case IStatus.INFO:
                pStatusLineManager.setMessage(ImageConstants.ICON_INFO, pMessage);
                break;
                
            case IStatus.OK:          
            default:
                // Simply display the message
                pStatusLineManager.setMessage(pMessage);
                break;
        }
    }

    /**
     * Return the status line manager linked to the actions bar of the given part.
     * 
     * @param pPart The part from which the status line is searched
     * @return The status line manager found, or null.
     */
    private static IStatusLineManager getStatusLineManager(final IWorkbenchPart pPart) {
        
        IStatusLineManager vStatusLineManager = null;

        if (pPart instanceof IViewPart) {
            // ... Get the status line from a simple view
            
            // Cast the part
            IViewPart vViewPart = (IViewPart) pPart;

            // Get the status line manager
            vStatusLineManager = vViewPart.getViewSite().getActionBars().getStatusLineManager();

        } else if (pPart instanceof IEditorPart) {
            // ... Get the status line from an editor
            
            // Cast the part
            IEditorPart vEditorPart = (IEditorPart) pPart;

            // Get the status line manager from the editor action bar contributor
            IEditorActionBarContributor vContributor = vEditorPart.getEditorSite().getActionBarContributor();
            if (vContributor instanceof EditorActionBarContributor) {
                vStatusLineManager = ((EditorActionBarContributor) vContributor).getActionBars().getStatusLineManager();
            }
        }
        
        return vStatusLineManager;
    }

}
