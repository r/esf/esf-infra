/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.treeviewer;

/**
 * Behaviour used to check if a node is final or not in a tree.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IFinalNodeStrategy {

    /**
     * Check if the current node is final or not.
     * What is final is just up to the user of this contract. Final can be more than just "no child". By example, if the
     * user must choose a file, a folder without files is not a final node, even if it has no child.
     * 
     * @param pNode The node to evaluate
     * @return <code>true</code> if it is a final node, <code>false</code> otherwise
     */
    boolean isFinal(final Object pNode);

}
