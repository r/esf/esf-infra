/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.ImageData;
import org.polarsys.esf.core.common.ui.filter.FilterPartManager;
import org.polarsys.esf.core.common.ui.filter.IFilterPartListener;


/**
 * Custom action which allow to activate of deactivate the filter
 * on a listener instance. The action representation is linked to the
 * listener image.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class FilterPartAction
    extends Action {

    /** Reference to the singleton which manages the filter mechanisms. */
    private static final FilterPartManager FILTER_PART_MANAGER = FilterPartManager.INSTANCE;
    
    /** The filter part listener linked to this action instance. */
    private IFilterPartListener mListener = null;

    /**
     * Constructor which build an action associated to a listener instance,
     * and specify if the action is already checked (activated) or not.
     * 
     * @param pListener The listener associated to this action instance
     * @param pChecked If <code>true</code> this action will be checked directly
     */
    public FilterPartAction(final IFilterPartListener pListener, final boolean pChecked) {
        // Call the standard constructor
        this(pListener);
        
        // Update directly the action status
        setChecked(pChecked);
    }

    /**
     * Constructor which build an action associated to a listener instance.
     * 
     * @param pListener The listener
     */
    public FilterPartAction(final IFilterPartListener pListener) {
        // Call the parent method
        super(pListener.getTitle(), IAction.AS_CHECK_BOX);
        
        // Remember of the given listener
        mListener = pListener;
        
        // Set the action image with the listener image
        // Like this, the action representation is directly adapted
        // to the linked listener instance representation
        setImageDescriptor(new ImageDescriptor() {
            /**
             * {@inheritDoc}
             */
            @Override
            public ImageData getImageData() {
                return mListener.getTitleImage().getImageData();
            }
        });

    }

    /**
     * {@inheritDoc}
     * 
     * When the action is run, add or remove the listener
     * according to this action status (checked or not). 
     */
    @Override
    public void run() {
        if (isChecked()) {
            FILTER_PART_MANAGER.addFilterPartListener(mListener);
        } else {
            FILTER_PART_MANAGER.removeFilterPartListener(mListener);
        }
    }
}
