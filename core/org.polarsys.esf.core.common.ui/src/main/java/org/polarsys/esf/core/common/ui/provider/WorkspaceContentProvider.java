/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.provider;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 *
 * Workspace content provider for Import Wizard.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class WorkspaceContentProvider
    implements ITreeContentProvider {

    /**
     * Creates a new ContainerContentProvider.
     */
    public WorkspaceContentProvider() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dispose() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getChildren(final Object pElement) {

        // Default result
        Object[] vChildren = new Object[0];

        if (pElement instanceof IWorkspace) {

            IWorkspace vWorkspace = (IWorkspace) pElement;

            // Visit workspace
            WorkspaceVisitor vWorkspaceVisitor = new WorkspaceVisitor();
            try {
                vWorkspace.getRoot().accept(vWorkspaceVisitor, 1, false);
            } catch (final CoreException pException) {
                CommonUIActivator.logError("Fail to visit workspace", //$NON-NLS-1$
                    pException);
            }

            IProject[] vAllProjects = vWorkspaceVisitor.getSAProjects();

            vChildren = vAllProjects;

        }

        return vChildren;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getElements(final Object pElement) {
        return getChildren(pElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getParent(final Object pElement) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasChildren(final Object pElement) {
        return getChildren(pElement).length > 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inputChanged(final Viewer pViewer, final Object pOldInput, final Object pNewInput) {
    }

    /**
     *
     * Implemented Resource Visitor which return all project containing ESF file.
     *
     *
     * @author $Author: jdumont $
     * @version $Revision: 83 $
     */
    private class WorkspaceVisitor
        implements IResourceVisitor {

        /** ESF Projects list to fill. */
        private List<IProject> mSAProjects = new ArrayList<IProject>();

        /**
         * Default constructor.
         */
        WorkspaceVisitor() {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {

            // Check Project resource
            if (pResource instanceof IProject) {
                IProject vProject = (IProject) pResource;

                // It is not possible to visit closed project.
                if (vProject.isAccessible()) {

                    // Visit project to determine if it is ESF project
                    ProjectVisitor vVisitor = new ProjectVisitor();
                    vProject.accept(vVisitor);

                    if (vVisitor.isSAProject()) {
                        mSAProjects.add(vProject);
                    }
                }
            }

            // Need to explore all project
            return true;
        }

        /**
         * @return All ESF Project in workspace.
         */
        public IProject[] getSAProjects() {
            return mSAProjects.toArray(new IProject[mSAProjects.size()]);
        }

    }

    /**
     * Implement resource visitor to explore Project and detect a file ESF.
     *
     * @author $Author: jdumont $
     * @version $Revision: 83 $
     */
    private class ProjectVisitor
        implements IResourceVisitor {

        /** If Project contain ESF file. */
        private boolean mContainSAFile = false;

        /**
         * Default constructor.
         */
        ProjectVisitor() {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean visit(final IResource pResource) throws CoreException {
            boolean vContinueVisit = !mContainSAFile;

            // Check other file only while a sa file is not found.
            // Visitor explore all neighbours before stop itself.
            if (vContinueVisit && pResource instanceof IFile) {
                IFile vFile = (IFile) pResource;
                // Check if extension file is sa
                // TODO : Check the right extension ...
                mContainSAFile = "uml".equals(vFile.getFileExtension());

                vContinueVisit = !mContainSAFile;
            }

            // if ESF file found, visit is stopped
            return vContinueVisit;
        }

        /**
         * @return if project contain a ESF file.
         */
        public boolean isSAProject() {
            return mContainSAFile;
        }

    }
}
