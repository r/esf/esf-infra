/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.widget;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.edit.provider.ItemProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.polarsys.esf.core.common.ui.CommonUIActivator;
import org.polarsys.esf.core.common.ui.provider.FileLabelProvider;
import org.polarsys.esf.core.common.ui.selection.ISelectionHelper;

/**
 * Group file model widget.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class GroupFileModelCreator {

    /** Title of model selection group. */
    private static final String MODEL_GROUP_TITLE = CommonUIActivator.getMessages().getString(
        "GroupFileModelCreator.group.title"); //$NON-NLS-1$

    /** Label for ESF File combo. */
    private static final String SA_FILE_LABEL = CommonUIActivator.getMessages()
        .getString("GroupFileModelCreator.label"); //$NON-NLS-1$

    /** Helper used to manage files which can be proposed in combo box. */
    private ISelectionHelper mSelectionHelper = null;

    /** Viewer create to manage link between data and control. */
    private Viewer mViewer = null;

    /**
     * Default constructor.
     * 
     * @param pSelectionHelper Helper used to get file which can be proposed to user.
     */
    public GroupFileModelCreator(final ISelectionHelper pSelectionHelper) {
        mSelectionHelper = pSelectionHelper;
    }

    /**
     * Create a group composite with a combo box, and add it to given parent composite.
     * 
     * @param pParent target composite for this group
     * @return viewer of combobox
     */
    public Viewer createGroupFileModel(final Composite pParent) {

        // Create composite which group model component
        Group vGroupComposite = new Group(pParent, SWT.NONE);
        vGroupComposite.setText(MODEL_GROUP_TITLE);

        // Set Layout
        GridLayout vLayout = new GridLayout(2, false);
        vGroupComposite.setLayout(vLayout);
        vGroupComposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

        Label vLabel = new Label(vGroupComposite, SWT.NONE);
        vLabel.setText(SA_FILE_LABEL);

        // Create Item Provider
        List<IFile> vFilesList = mSelectionHelper.getFilteredFiles();
        ItemProvider vFileProvider = new ItemProvider(vFilesList);

        // Then create a content provider, it is a link between viewer and item provider.
        IContentProvider vContentProvider = new AdapterFactoryContentProvider(new AdapterFactoryImpl());

        ILabelProvider vLabelProvider = new FileLabelProvider();

        ComboViewer vComboViewer = new ComboViewer(vGroupComposite);
        vComboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

        vComboViewer.setContentProvider(vContentProvider);
        vComboViewer.setLabelProvider(vLabelProvider);
        vComboViewer.setInput(vFileProvider);

        mViewer = vComboViewer;

        initialiseSelectedFile();

        return vComboViewer;
    }

    /**
     * Initialise selected file.
     */
    private void initialiseSelectedFile() {
        IFile vSelectedFile = null;
        IFile[] vSelectedFiles = mSelectionHelper.getSelectedFiles();

        if (vSelectedFiles.length > 0) {
            vSelectedFile = vSelectedFiles[0];
        }

        List<IFile> vFilesList = mSelectionHelper.getFilteredFiles();

        if (!vFilesList.isEmpty()) {

            /*
             * Load model from selection when it is known, otherwise load first of list.
             */
            int vIndex = 0;
            if (vSelectedFile != null) {
                vIndex = vFilesList.indexOf(vSelectedFile);
            }

            mViewer.setSelection(new StructuredSelection(vFilesList.get(vIndex)));

        }
    }
}
