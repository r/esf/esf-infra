/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.job;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 * UI Job which display and activate desired view.
 * 
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class FocusViewJob
    extends UIJob {

    /** Message when it is impossible to activate desired view. */
    private static final String ERROR_LOG_MESSAGE = "Impossible to activate view with ID {0}"; //$NON-NLS-1$

    /** ID of view to show. */
    private String mIdDisplayedView = null;

    /**
     * Default constructor.
     * 
     * @param pJobName Name of Job.
     * @param pSelectedViewId ID of view to display.
     */
    public FocusViewJob(final String pJobName, final String pSelectedViewId) {
        super(pJobName);
        mIdDisplayedView = pSelectedViewId;
    }

    /**
     * Default constructor with defined display.
     * 
     * @param pJobDisplay Display in which job is executed.
     * @param pJobName Name for job.
     * @param pSelectedViewId Id of view to display.
     */
    public FocusViewJob(final Display pJobDisplay, final String pJobName, final String pSelectedViewId) {
        super(pJobDisplay, pJobName);
        mIdDisplayedView = pSelectedViewId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IStatus runInUIThread(final IProgressMonitor pMonitor) {
        // OK Status
        IStatus vReturnStatus = Status.OK_STATUS;

        try {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                .showView(mIdDisplayedView);
        } catch (final PartInitException pException) {
            vReturnStatus =
                new Status(IStatus.ERROR, CommonUIActivator.getPlugin().getSymbolicName(), MessageFormat.format(
                    ERROR_LOG_MESSAGE,
                    mIdDisplayedView), pException);

        }
        return vReturnStatus;
    }
}
