/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.widget;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Wrap a file dialog to ensure that the returned file name
 * will be suffixed by the filtered extension, if one is set.
 *
 * For example, if the user type 'test' and the dialog has been built
 * to select only xml file, the returned value must be 'test.xml'
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class FileDialogWrapper {

    /** The file dialog instance wrapped, to adapt its behaviour. */
    private FileDialog mFileDialog = null;

    /** The filters array to apply. */
    private FileFilterEnum[] mFileFiltersArray = null;

    /**
     * Constructs a new instance of this class given only the parent of the wrapped file dialog,
     * and the filters to apply.
     *
     * @param pParent Shell which will be the parent of the wrapped file dialog
     * @param pFileFiltersArray The array of file filters to apply on the wrapped dialog, or null
     */
    public FileDialogWrapper(final Shell pParent, final FileFilterEnum[] pFileFiltersArray) {
        mFileFiltersArray = pFileFiltersArray;
        mFileDialog = new FileDialog(pParent);

        // Set the filters on the wrapped dialog
        initialiseDialogFilters();
    }

    /**
     * Constructs a new instance of this class given the parent of the wrapped file dialog,
     * a style value describing its behaviour and appearance, and the filters to apply.
     *
     * @param pParent Shell which will be the parent of the wrapped file dialog
     * @param pStyle The style of wrapped dialog
     * @param pFileFiltersArray The array of file filters to apply on the wrapped dialog, or null
     */
    public FileDialogWrapper(final Shell pParent, final int pStyle, final FileFilterEnum[] pFileFiltersArray) {
        mFileFiltersArray = pFileFiltersArray;
        mFileDialog = new FileDialog(pParent, pStyle);

        // Set the filters on the wrapped dialog
        initialiseDialogFilters();
    }

    /**
     * Initialise the filters on the wrapped dialog.
     */
    private void initialiseDialogFilters() {

        if (mFileFiltersArray != null) {

            // Loop on the filters to apply to build an array of filters
            // compatible with the standard file dialog, which uses only String values
            String[] vFiltersLabelArray = new String[mFileFiltersArray.length];
            for (int i = 0; i < mFileFiltersArray.length; i++) {
                vFiltersLabelArray[i] = mFileFiltersArray[i].getFilterLabel();
            }

            // Finally apply the filters on the wrapped dialog
            mFileDialog.setFilterExtensions(vFiltersLabelArray);
        }
    }

    /**
     * Open the wrapped file dialog and ensure that its returned file name
     * is suffixed by the selected filter, if one is set.
     *
     * @return The selected file, suffixed is necessary
     */
    public String openFileDialog() {
        // Call the wrapped dialog method to get the first selected file,
        // and suffix the name is necessary
        return suffixFileName(mFileDialog.open());
    }

    /**
     * Get the wrapped file dialog.
     *
     * @return The wrapped file dialog instance
     */
    public FileDialog getFileDialog() {
        return mFileDialog;
    }

    /**
     * Get the file name selected in the wrapped file dialog
     * and ensure that it is suffixed by the selected filter, if one is set.
     *
     * @return The selected file, suffixed is necessary
     */
    public String getFileName() {
        // Call the parent method to get the first selected file,
        // and suffix the name is necessary
        return suffixFileName(mFileDialog.getFileName());
    }

    /**
     * Get the file names selected in the wrapped file dialog
     * and ensure that they are suffixed by the selected filter, if one is set.
     *
     * @return The selected files, suffixed is necessary
     */
    public String[] getFileNames() {

        // Get the array of file names selected in the dialog
        String[] vFileNamesArray = mFileDialog.getFileNames();

        // Loop on the file names to suffix each one if necessary
        List<String> vSuffixedFileNamesList = new ArrayList<String>(vFileNamesArray.length);
        for (String vCurrentFileName : vFileNamesArray) {
            // Suffix the current name and add it in the list
            vSuffixedFileNamesList.add(suffixFileName(vCurrentFileName));
        }

        // Convert the list to an array and return it
        return vSuffixedFileNamesList.toArray(new String[vSuffixedFileNamesList.size()]);
    }

    /**
     * Return a String corresponding to the given file name suffixed by
     * the filter extension selected. If no filter is selected, or if the
     * name is already correctly suffixed, the name is not changed.
     *
     * @param pFileName The file name to suffix by the selected filter
     * @return The file name suffixed
     */
    private String suffixFileName(final String pFileName) {

        String vSuffixedFileName = pFileName;

        // Ensure that the initial file name is valid
        if (StringUtils.isNotEmpty(vSuffixedFileName)) {

            // Get the index of the filter currently selected, and get the
            // corresponding filter enumeration instance, to find its file extension
            FileFilterEnum vFilterFound = null;
            if (mFileFiltersArray != null && mFileDialog.getFilterIndex() >= 0) {
                vFilterFound = mFileFiltersArray[mFileDialog.getFilterIndex()];
            }

            // Ensure that a filter is found
            if (vFilterFound != null) {
                // Build the corresponding extension value
                String vExtensionWithDot = '.' + vFilterFound.getFilterExtension();

                // Adapt the file name to ensure that it finishes by the filtered extension
                if (!vSuffixedFileName.endsWith(vExtensionWithDot)) {
                    vSuffixedFileName += vExtensionWithDot;
                }
            }
        }

        return vSuffixedFileName;
    }

    /**
     * Define location where dialog will open.
     *
     * @param pDirectoryPath Path of directory
     */
    public void setDefaultDirectory(final String pDirectoryPath) {
        String vLocation = pDirectoryPath;

        // If location is empty, workspace location will be used
        if (StringUtils.isEmpty(pDirectoryPath)) {
            vLocation = ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString();
        }

        mFileDialog.setFilterPath(vLocation);
    }

    /**
     * Enumeration for available file filters.
     *
     * @author $Author: jdumont $
     * @version $Revision: 83 $
     */
    public enum FileFilterEnum {

        /** XML filter. */
        XML("xml", "*.xml"), //$NON-NLS-1$ //$NON-NLS-2$

        /** UML filter. */
        UML("uml", "*.uml"); //$NON-NLS-1$ //$NON-NLS-2$

        /** The filter label displayed. */
        private String mFilterLabel = null;

        /** The filter extension. */
        private String mFilterExtension = null;

        /**
         * Default constructor.
         *
         * @param pFilterExtension The file extension filtered (like 'xml')
         * @param pFilterLabel The filter label displayed for the user (like '*.xml')
         */
        FileFilterEnum(final String pFilterExtension, final String pFilterLabel) {
            mFilterExtension = pFilterExtension;
            mFilterLabel = pFilterLabel;
        }

        /**
         * @return The filter label
         */
        public String getFilterLabel() {
            return mFilterLabel;
        }

        /**
         * @return The filter extension
         */
        public String getFilterExtension() {
            return mFilterExtension;
        }
    }
}
