/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.provider;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

/**
 * Label provider for display name file in combo box.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class FileLabelProvider
    extends LabelProvider
    implements ILabelProvider {

    /**
     * Default constructor.
     */
    public FileLabelProvider() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getText(final Object pElement) {
        String vDiplayedText = StringUtils.EMPTY;

        // If element is an file, return file name
        if (pElement instanceof IFile) {
            IFile vFile = (IFile) pElement;

            vDiplayedText = vFile.getProjectRelativePath().toOSString();

        }

        return vDiplayedText;
    }

}
