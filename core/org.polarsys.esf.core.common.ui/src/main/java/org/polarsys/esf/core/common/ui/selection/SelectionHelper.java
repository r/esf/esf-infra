/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.selection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.polarsys.esf.core.common.project.ProjectFinder;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 * Helper to define models on which action will be executed. Models are found in function of Active part
 * selection.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class SelectionHelper
    implements ISelectionHelper {

    /** Message when project visit failed. */
    private static final String ERROR_PROJECT_VISIT_MESSAGE = "Impossible to visit current project"; //$NON-NLS-1$

    /** File filters extension. */
    private String[] mExtensionFilters = null;

    /** List of filtered files in root project of selection. */
    private List<IFile> mModelFiles = null;

    /** Selected files. */
    private IFile[] mSelectedFiles = new IFile[] {};

    /** Current project. */
    private IProject mCurrentProject = null;

    /**
     * Default constructor.
     */
    public SelectionHelper() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initFromSelection(final ExecutionEvent pEvent) {
        // Protect against null parameter
        if (pEvent != null) {
            mModelFiles = new ArrayList<IFile>();
            // Get active part
            IWorkbenchWindow vActivePart = HandlerUtil.getActiveWorkbenchWindow(pEvent);

            if (vActivePart != null) {
                // Get selection
                ISelection vCurrentSelection = getCurrentSelection(vActivePart);

                // Ensure that the selection is a tree selection, to be able to
                // work with the path of the selected object
                if (vCurrentSelection instanceof ITreeSelection) {

                    // Get the current project from the selection
                    IProject vCurrentProject = getCurrentProject((ITreeSelection) vCurrentSelection);

                    // Get all the ESF files included in the project
                    mModelFiles.addAll(getFilesFromProject(vCurrentProject));

                    // Check if ESF file is selected
                    initCurrentSelectedFile(vCurrentSelection);
                }
            }
        }
    }

    /**
     * Check current selection to determine if user selected a file matching the extension. If yes, this selection
     * is saved.
     *
     * @param pSelection Selection to check.
     */
    private void initCurrentSelectedFile(final ISelection pSelection) {
        IStructuredSelection vSelection = (IStructuredSelection) pSelection;
        List<String> vFiltersList = Arrays.asList(mExtensionFilters);

        List<IFile> vSelectedFiles = new ArrayList<>();

        for (Object vElementUnderSelection : vSelection.toArray()) {
            if (vElementUnderSelection instanceof IFile
                && (vFiltersList.contains(((IFile) vElementUnderSelection).getFileExtension()) || vFiltersList
                    .isEmpty())) {
                vSelectedFiles.add((IFile) vElementUnderSelection);
            }
        }

        mSelectedFiles = vSelectedFiles.toArray(mSelectedFiles);
    }

    /**
     * Get current selection from active part.
     *
     * @param pActivePart Part from which get current selection.
     * @return Current selection.
     */
    private ISelection getCurrentSelection(final IWorkbenchWindow pActivePart) {
        return pActivePart.getSelectionService().getSelection();
    }

    /**
     * Get the current project from the given selection.
     *
     * @param pTreeSelection Selection from where the project is searched. Must not be <code>null</code>
     * @return Current project in which selection is made, otherwise <code>null</code>
     */
    private IProject getCurrentProject(final ITreeSelection pTreeSelection) {
        mCurrentProject = null;

        // Get the paths of the selected objects
        TreePath[] vPathsArray = pTreeSelection.getPaths();

        // Ensure that at least one path has been found
        // NB : It means that at least one object is selected
        if (vPathsArray.length > 0) {
            // Get the path of the first selected object
            TreePath vSelectedPath = vPathsArray[0];

            int vIndex = 0;
            Object vCurrentObject = null;

            // Loop on the selected object of this tree selection until a project is found
            // NB : In most of the case it's directly the root, but the user can show working sets
            //      as root elements, etc.
            while (mCurrentProject == null && vIndex < vSelectedPath.getSegmentCount()) {
                // Get the current object
                vCurrentObject = vSelectedPath.getSegment(vIndex);

                // Check if this is a resource (project or file) and get its project
                if (vCurrentObject instanceof IResource) {
                    // Remember of the project, this will stop the loop
                    mCurrentProject = ((IResource) vCurrentObject).getProject();
                }

                vIndex++;
            }
        }

        return mCurrentProject;
    }

    /**
     * Get all files according to extension filter into project.
     *
     * @param pProject Project which is explored
     * @return List of found files.
     */
    private List<IFile> getFilesFromProject(final IProject pProject) {
        List<IFile> vProjectSAFiles = new ArrayList<IFile>();

        // Protect against null parameter and closed project
        if (pProject != null && pProject.isAccessible()) {
            ProjectFinder vFinder = new ProjectFinder(pProject);

            try {
                vProjectSAFiles.addAll(vFinder.findAllFiles(mExtensionFilters));
            } catch (final CoreException pException) {
                CommonUIActivator.logError(ERROR_PROJECT_VISIT_MESSAGE, pException);
            }
        }

        return vProjectSAFiles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IFile> getFilteredFiles() {
        return mModelFiles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IFile[] getSelectedFiles() {
        return mSelectedFiles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setExtensionFilters(final String[] pExtensionFilter) {
        mExtensionFilters = pExtensionFilter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getExtensionFilters() {
        return mExtensionFilters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IProject getSelectedProject() {
        return mCurrentProject;
    }

}
