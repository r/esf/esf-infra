/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.selection;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

/**
 * 
 * Used to get a set of file, for an event or a projet.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface ISelectionHelper {

    /**
     * @param pExtensionFilters Extension filters for selection
     */
    void setExtensionFilters(String[] pExtensionFilters);

    /**
     * @return The extension filters
     */
    String[] getExtensionFilters();

    /**
     * Initialise helper with event.
     * 
     * @param pEvent Execution event from where files are found
     */
    void initFromSelection(ExecutionEvent pEvent);

    /**
     * @return All files from root project selection according to extension filter
     */
    List<IFile> getFilteredFiles();

    /**
     * Get the files under selection.
     * 
     * @return Files under selection, otherwise an empty array
     */
    IFile[] getSelectedFiles();

    /**
     * @return The project selected based on current file selection.
     */
    IProject getSelectedProject();

}
