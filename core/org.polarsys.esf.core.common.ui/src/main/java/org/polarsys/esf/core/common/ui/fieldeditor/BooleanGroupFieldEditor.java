/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.fieldeditor;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;

/**
 * A field editor for an enumeration type preference.
 * The choices are presented as a list of check buttons.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class BooleanGroupFieldEditor
    extends FieldEditor {

    /**
     * List of check button entries of the form [label,key].
     */
    private String[][] mLabelsAndValues = null;

    /**
     * Number of columns into which to arrange the check buttons.
     */
    private int mNumColumns = 1;

    /**
     * Indent used for the first column of the check button matrix.
     */
    private int mIndent = HORIZONTAL_GAP;

    /**
     * The box of check buttons, or <code>null</code> if none
     * (before creation and after disposal).
     */
    private Composite mCheckBox = null;

    /**
     * The check buttons, or <code>null</code> if none
     * (before creation and after disposal).
     */
    private Button[] mBooleanButtons = null;

    /**
     * Whether to use a Group control.
     */
    private boolean mUseGroup = false;

    /**
     * Constructor with default presentation.
     * 
     * @param pLabelText the label text of the field editor
     * @param pNumColumns the number of columns for the check button presentation
     * @param pLabelAndValues list of check button [label, value] entries;
     * the value is returned when the check button is selected
     * @param pParent the parent of the field editor's control
     */
    public BooleanGroupFieldEditor(
        final String pLabelText,
        final int pNumColumns,
        final String[][] pLabelAndValues,
        final Composite pParent) {
        this(pLabelText, pNumColumns, pLabelAndValues, pParent, false);
    }

    /**
     * Constructor with choice to display group presentation.
     * 
     * @param pLabelText the label text of the group field editor
     * @param pNumColumns the number of columns for the check button presentation
     * @param pLabelAndValues list of check button [label, key]
     * @param pParent the parent of the field editor's control
     * @param pUseGroup whether to use a Group control to contain the check buttons
     */
    public BooleanGroupFieldEditor(
        final String pLabelText,
        final int pNumColumns,
        final String[][] pLabelAndValues,
        final Composite pParent,
        final boolean pUseGroup) {
        mLabelsAndValues = pLabelAndValues;
        mNumColumns = pNumColumns;
        mUseGroup = pUseGroup;
        setLabelText(pLabelText);
        createControl(pParent);
    }

    /**
     * Returns this field editor's check group control.
     * 
     * @param pParent The parent to create the checkBox in
     * @return the check group control
     */
    public Composite getCheckBoxControl(final Composite pParent) {
        if (mCheckBox == null) {

            Font vFont = pParent.getFont();

            if (mUseGroup) {
                Group vGroup = new Group(pParent, SWT.NONE);
                vGroup.setFont(vFont);
                String vText = getLabelText();
                if (vText != null) {
                    vGroup.setText(vText);
                }

                mCheckBox = vGroup;
                GridLayout vLayout = new GridLayout();
                vLayout.horizontalSpacing = HORIZONTAL_GAP;
                vLayout.numColumns = mNumColumns;
                mCheckBox.setLayout(vLayout);
            } else {

                mCheckBox = new Composite(pParent, SWT.NONE);
                GridLayout vLayout = new GridLayout();
                vLayout.marginWidth = 0;
                vLayout.marginHeight = 0;
                vLayout.horizontalSpacing = HORIZONTAL_GAP;
                vLayout.numColumns = mNumColumns;
                mCheckBox.setLayout(vLayout);
                mCheckBox.setFont(vFont);
            }

            initialiseButtonsArray(pParent);

        } else {
            checkParent(mCheckBox, pParent);
        }
        return mCheckBox;
    }

    /**
     * Initialise all button of group with associated label value.
     * 
     * @param pParent The parent to create CheckBoxes in
     */
    private void initialiseButtonsArray(final Composite pParent) {
        mBooleanButtons = new Button[mLabelsAndValues.length];
        Font vFont = pParent.getFont();
        for (int i = 0; i < mLabelsAndValues.length; i++) {
            Button vCheckButton = new Button(mCheckBox, SWT.CHECK | SWT.LEFT);
            mBooleanButtons[i] = vCheckButton;
            String[] vLabelAndValue = mLabelsAndValues[i];
            vCheckButton.setText(vLabelAndValue[0]);
            vCheckButton.setData(vLabelAndValue[1]);
            vCheckButton.setFont(vFont);

        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected void adjustForNumColumns(final int pNumColumns) {
        Control vControl = getLabelControl();
        if (vControl != null) {
            ((GridData) vControl.getLayoutData()).horizontalSpan = pNumColumns;
        }
        ((GridData) mCheckBox.getLayoutData()).horizontalSpan = pNumColumns;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFillIntoGrid(final Composite pParent, final int pNumColumns) {

        if (mUseGroup) {
            Control vControl = getCheckBoxControl(pParent);
            GridData vDataLayout = new GridData(GridData.FILL_HORIZONTAL);
            vControl.setLayoutData(vDataLayout);
        } else {

            Control vControl = getLabelControl(pParent);
            GridData vDataLayout = new GridData();
            vDataLayout.horizontalSpan = pNumColumns;
            vControl.setLayoutData(vDataLayout);
            vControl = getCheckBoxControl(pParent);
            vDataLayout = new GridData();
            vDataLayout.horizontalSpan = pNumColumns;
            vDataLayout.horizontalIndent = mIndent;
            vControl.setLayoutData(vDataLayout);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doLoad() {
        for (int vIndex = 0; vIndex < mLabelsAndValues.length; vIndex++) {
            String[] vBoxValues = mLabelsAndValues[vIndex];
            Button vCheckBox = mBooleanButtons[vIndex];
            boolean vStoreValue = getPreferenceStore().getBoolean(vBoxValues[1]);
            vCheckBox.setSelection(vStoreValue);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doLoadDefault() {
        for (int vIndex = 0; vIndex < mLabelsAndValues.length; vIndex++) {
            String[] vBoxValues = mLabelsAndValues[vIndex];
            Button vCheckBox = mBooleanButtons[vIndex];
            boolean vStoreValue = getPreferenceStore().getDefaultBoolean(vBoxValues[1]);
            vCheckBox.setSelection(vStoreValue);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doStore() {

        for (int vIndex = 0; vIndex < mBooleanButtons.length; vIndex++) {
            Button vCheckbox = mBooleanButtons[vIndex];
            String[] vBoxValues = mLabelsAndValues[vIndex];
            getPreferenceStore().setValue(vBoxValues[1], vCheckbox.getSelection());
        }

    }
    
    /**
     * {@inheritDoc}
     * 
     * This is overridden to be able to manage the case of this group field editor,
     * which hasn't only one preference name, but one by boolean field. Thus, the standard
     * mechanism to restore the default value is not working. 
     */
    @Override
    public void store() {
        // Ensure that a valid preference store is known
        if (getPreferenceStore() != null) {

            // Check if the values must be restored to the default ones
            if (presentsDefaultValue()) {
                // Loop on all the boolean fields to restore each of them to their default value
                for (int vIndex = 0; vIndex < mBooleanButtons.length; vIndex++) {
                    String[] vBoxValues = mLabelsAndValues[vIndex];
                    getPreferenceStore().setToDefault(vBoxValues[1]);
                }
                
            } else {
                // Otherwise, call the standard method to store the current values
                doStore();
            }
        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public int getNumberOfControls() {
        return 1;
    }

    /**
     * Sets the indent used for the first column of the boolean button matrix.
     * 
     * @param pIndent the indent (in pixels)
     */
    public void setIndent(final int pIndent) {
        if (pIndent < 0) {
            mIndent = 0;
        } else {
            mIndent = pIndent;
        }
    }

}
