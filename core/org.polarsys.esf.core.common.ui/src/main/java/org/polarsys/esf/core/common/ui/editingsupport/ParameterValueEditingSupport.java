/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.editingsupport;

import java.math.BigDecimal;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.widgets.Composite;
import org.polarsys.esf.core.common.ui.CommonUIActivator;
import org.polarsys.esf.core.common.ui.statusline.StatusLinesUtil;


/**
 * Implementation of editing support for set value of probability rule parameters.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ParameterValueEditingSupport 
    extends EditingSupport {

    /** Cell editor for parameter value. */
    private CellEditor mCellEditor = null;
    
    /**
     * Default constructor.
     * 
     * @param pViewer Viewer used to edit the rules parameter
     */
    public ParameterValueEditingSupport(final ColumnViewer pViewer) {
        super(pViewer);

        // Initialise the cell editor to use
        initialiseCellEditor();
    }

    /**
     * Initialise the cell editor, which will manage the value validation.
     */
    private void initialiseCellEditor() {
        // Create the cell editor
        mCellEditor = new TextCellEditor((Composite) getViewer().getControl());
        
        // Add a new validator to ensure that the value is correct
        mCellEditor.setValidator(new ICellEditorValidator() {

            /**
             * {@inheritDoc}
             * 
             * A parameter value must be a numerical value.
             */
            @Override
            public String isValid(final Object pValue) {

                // Initialise result with valid state
                String vMessage = null;

                // Verify if value is a non empty string
                if ((pValue instanceof String) && StringUtils.isNotEmpty((String) pValue)) {


                    try {
                        // Check that value can be converted in big decimal
                        new BigDecimal((String) pValue);
                    
                    } catch (final NumberFormatException pException) {
                        // Set the error message top warn the user 
                        vMessage = CommonUIActivator.getMessages()
                            .getString("ParameterValueEditingSupport.warning.notnumerical"); //$NON-NLS-1$ 
                    }
                }
                
                return vMessage;
            }
        });
        
        
        // Add a new listener on the cell editor to react to the value validation
        // and display any message to the user
        mCellEditor.addListener(new ICellEditorListener() {

            /**
             * {@inheritDoc}
             * 
             * This implementation aim is to display error message when the value is not valid.
             */
            @Override
            public void editorValueChanged(final boolean pOldValidState, final boolean pNewValidState) {
                if (!pNewValidState) {
                    // The new state is not valid, thus display an error message
                    StatusLinesUtil.outputMessage(mCellEditor.getErrorMessage(), IStatus.WARNING);
                } else {
                    // The new state is valid, clear the status message
                    StatusLinesUtil.clearMessage();
                }
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public void cancelEditor() {
                // The new state is valid, clear the status message
                StatusLinesUtil.clearMessage();
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public void applyEditorValue() {
                // Nothing to do
            }
        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CellEditor getCellEditor(final Object pElement) {
        return mCellEditor;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canEdit(final Object pElement) {
        return true;
    }

    /**
     * {@inheritDoc}
     * 
     * The rule's parameters are stored in a map, with the parameter
     * name as key, and its value as value. The edited object is an entry
     * of this map, and the value returned by this method must thus be the 
     * value of this entry.
     */
    @Override
    protected Object getValue(final Object pElement) {
        Object vValue = null;
        
        // Check if the given object is an entry of a map
        if (pElement instanceof Entry<?, ?>) {
            // Get the value of this entry, which corresponds to the 
            // value of the edited parameter, as String
            vValue = ((Entry<?, ?>) pElement).getValue();
        }
        return vValue;
    }

    /**
     * {@inheritDoc}
     * 
     * The rule's parameters are stored in a map, with the parameter
     * name as key, and its value as value. The edited object is an entry
     * of this map, and the value to set must thus be the value of this entry.
     */
    @Override
    protected void setValue(final Object pElement, final Object pValue) {
        // Check if the given object is an entry of a map
        if (pElement instanceof Entry<?, ?>) {
            // Update the entry value with the given value
            ((Entry) pElement).setValue(pValue);
        }

        // Update the viewer
        getViewer().update(pElement, null);
    }
    
    /**
     * {@inheritDoc}
     * 
     * If there is no error the value is saved and the status line message is cleared.
     * Otherwise, display an error message.
     * 
     * @param pCellEditor The cell editor
     * @param pViewerCell The cell on which the cell editor is working
     */
    @Override
    protected void saveCellEditorValue(final CellEditor pCellEditor, final ViewerCell pViewerCell) {
        
        // Check if the cell editor consider the value as valid
        if (pCellEditor.isValueValid()) {
            // Clear the status message
            StatusLinesUtil.clearMessage();
                        
            // Call the parent method to save the value in the model
            super.saveCellEditorValue(pCellEditor, pViewerCell);    
            
        } else {
            // The value is not valid, display an error message and don't save the new value
            StatusLinesUtil.outputMessage(pCellEditor.getErrorMessage(), IStatus.WARNING);
        }
    }
}
