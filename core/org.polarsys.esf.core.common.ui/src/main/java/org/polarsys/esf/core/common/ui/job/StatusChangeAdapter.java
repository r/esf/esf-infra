/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.job;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Listener adapter check returned status job to display OK status.
 * Eclipse display automatically Error status.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class StatusChangeAdapter
    extends JobChangeAdapter {

    /**
     * Default constructor.
     */
    public StatusChangeAdapter() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void done(final IJobChangeEvent pEvent) {

        // Get status from listened job
        IStatus vStatusJob = pEvent.getResult();

        // Check if status must be display
        if (vStatusJob.isOK()) {
            StatusManager.getManager().handle(vStatusJob);
        }
    }

}
