/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.view.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;

/**
 * Property source provider used to work with an array as input element.
 * 
 * It extends {@link PropertySourceProvider} to be able to work also on generic.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ArrayInputPropertySourceProvider
    extends PropertySourceProvider {

    /**
     * Default constructor.
     * 
     * @param pAdapterFactory The adapter factory
     */
    public ArrayInputPropertySourceProvider(final AdapterFactory pAdapterFactory) {
        super(pAdapterFactory);
    }

    /**
     * {@inheritDoc}
     * 
     * This method is overridden to be able to manage the arrays as input element.
     */
    @Override
    public Object[] getElements(final Object pInputElement) {
        Object[] vElementsArray = new Object[] {};

        // Check if the input element is an array
        if (pInputElement instanceof Object[]) {
            // If the input is an array, loop on each input object in it
            // to get its corresponding elements
            final List<Object> vElementsList = new ArrayList<Object>();
            for (final Object vInputElement : (Object[]) pInputElement) {
                vElementsList.addAll(Arrays.asList(super.getElements(vInputElement)));
            }

            // Build the final elements array from the list
            if (!vElementsList.isEmpty()) {
                vElementsArray = vElementsList.toArray(new Object[vElementsList.size()]);
            }

        } else {
            // If the input is a single object, get its element by calling the parent method
            vElementsArray = super.getElements(pInputElement);
        }

        return vElementsArray;
    }

}
