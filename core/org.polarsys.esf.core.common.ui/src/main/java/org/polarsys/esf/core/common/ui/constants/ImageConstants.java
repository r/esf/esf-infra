/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.constants;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * Utility class used to store several constants for the images used.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class ImageConstants {
        
    /** The info icon, shared in the workbench. */
    public static final Image ICON_INFO = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_INFO_TSK);
    
    /** The warning icon, shared in the workbench. */
    public static final Image ICON_WARNING = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_WARN_TSK);
    
    /** The error icon, shared in the workbench. */
    public static final Image ICON_ERROR = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
    
    /** The disabled clear image. */
    public static final Image ICON_CLEAR_DISABLED = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_CLEAR_DISABLED);

    /** The clear image. */
    public static final Image ICON_CLEAR = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_CLEAR);

    /** The pressed clear image (its the same as the disabled image). */
    public static final Image ICON_CLEAR_PRESSED = 
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_CLEAR_DISABLED);
    
    /**
     * Default constructor, private as it's a utility class. 
     */
    private ImageConstants() {
    }
    
}
