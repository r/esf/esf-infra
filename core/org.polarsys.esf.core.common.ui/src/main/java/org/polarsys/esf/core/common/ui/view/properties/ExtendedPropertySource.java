/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.view.properties;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

/**
 * This class is a {@link PropertySource} that manages the Java Generic.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ExtendedPropertySource
    extends PropertySource {

    /**
     * Default constructor.
     * An instance is constructed from an object and its item property source.
     *
     * @param pObject The object
     * @param pItemPropertySource The item property source
     */
    public ExtendedPropertySource(final Object pObject, final IItemPropertySource pItemPropertySource) {
        super(pObject, pItemPropertySource);
    }

    /**
     * Create and return an {@link IPropertyDescriptor}.
     *
     * @param pItemPropertyDescriptor The item property descriptor
     * @return The created {@link IPropertyDescriptor}
     * @see #createPropertyDescriptor(Object, IItemPropertyDescriptor)
     */
    @Override
    protected IPropertyDescriptor createPropertyDescriptor(final IItemPropertyDescriptor pItemPropertyDescriptor) {
        return createPropertyDescriptor(object, pItemPropertyDescriptor);
    }

    /**
     * Create and return the property descriptor {@link IItemPropertyDescriptor} of an object.
     *
     * @param pObject The object
     * @param pItemPropertyDescriptor The item property descriptor
     * @return An {@link ExtendedPropertyDescriptor} descriptor of an object
     */
    protected IPropertyDescriptor createPropertyDescriptor(
        final Object pObject,
        final IItemPropertyDescriptor pItemPropertyDescriptor) {
        return new ExtendedPropertyDescriptor(pObject, pItemPropertyDescriptor);
    }
}
