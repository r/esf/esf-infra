/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.job;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.uml2.uml.Model;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 *
 * Show a progress while ESF file load.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class RunnableLoadModel
    implements IRunnableWithProgress {

    /** Label of loading ESF file task. */
    private static final String TASK_SA_LOADING = CommonUIActivator.getMessages()
        .getString("RunnableLoadModel.task.sa"); //$NON-NLS-1$

    /** Model file URI. */
    private URI mModelUri = null;

    /** Loaded model. */
    private Model mModel = null;

    /**
     * Default constructor.
     *
     * @param pURI Model file path.
     */
    public RunnableLoadModel(final URI pURI) {
        mModelUri = pURI;
    }

    /**
     * @return Loaded model.
     */
    public Model getLoadingModel() {
        return mModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(final IProgressMonitor pMonitor) throws InvocationTargetException, InterruptedException {
        pMonitor.beginTask(TASK_SA_LOADING, IProgressMonitor.UNKNOWN);
        // TODO : Load the model ...
        // mModel = ResourceLoaderFactory.getInstance().getModelLoader().loadFromURI(mModelUri);
        pMonitor.done();
    }
}
