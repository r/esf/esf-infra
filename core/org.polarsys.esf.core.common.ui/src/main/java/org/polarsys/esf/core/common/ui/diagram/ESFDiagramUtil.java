/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.diagram;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.papyrus.infra.viewpoints.policy.PolicyChecker;
import org.eclipse.papyrus.infra.viewpoints.policy.ViewPrototype;
import org.eclipse.uml2.uml.Element;

/**
 * Utilities for working with ESFLocalAnalysis Diagram.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFDiagramUtil {

	/**
	 * Default constructor.
	 */
	private ESFDiagramUtil() {
	}

	/**
	 * Get The ViewPrototype of the Table by its name.
	 *
	 * @param pDiagramName The Diagram name
	 * @return The Diagram ViewPrototype
	 */
	public static ViewPrototype getViewPrototype(final String pDiagramName, final Element pRootElement) {
		Collection<ViewPrototype> vPrototypes = PolicyChecker.getFor(pRootElement).getAllPrototypes();
		ViewPrototype vDiagram = null;
		String vPotentialDiagramName = null;
		Iterator<ViewPrototype> vIterator = vPrototypes.iterator();
		while (vDiagram == null && vIterator.hasNext()) {
			ViewPrototype vPotentialPrototype = vIterator.next();
			vPotentialDiagramName = vPotentialPrototype.getRepresentationKind().getName();
			if (pDiagramName.equals(vPotentialDiagramName)) {
				vDiagram = vPotentialPrototype;
			}
		}

		return vDiagram;
	}

	/**
	 * Create a table.
	 *
	 * @param pDiagramName The Diagram Name
	 * @param pRootElement The element where should be created the table.
	 */
	public static void createDiagram(final String pDiagramName, final Element pRootElement) {
		ViewPrototype vDiagram = getViewPrototype(pDiagramName, pRootElement);
		if (vDiagram != null) {
			vDiagram.instantiateOn(pRootElement, pDiagramName + vDiagram.getViewCountOn(pRootElement));
		}
	}
}
