/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.provider;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Overridden because first column (index 0) must used getText and not getColumnText on label provider, 
 * which return null.
 * 
 * It is because ItemProviderAdapter (Supertype of all item provider of model) implementation of getColumnText
 * return <code>null</code>.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class AdapterFactoryColumnLabelProvider
    extends AdapterFactoryLabelProvider {

    /**
     * Default constructor.
     * 
     * @param pAdapterFactory Factory able to create label provider for model.
     */
    public AdapterFactoryColumnLabelProvider(final AdapterFactory pAdapterFactory) {
        super(pAdapterFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getColumnText(final Object pObject, final int pColumnIndex) {
        String vText = super.getText(pObject);

        return vText;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Image getColumnImage(final Object pObject, final int pColumnIndex) {
        Image vImage = super.getImage(pObject);

        return vImage;
    }
    
}
