/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.view.properties;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.ui.views.properties.IPropertySource;

/**
 * This class is a {@link org.eclipse.ui.views.properties.IPropertySourceProvider} that manage the Generic.
 * This class give data for display in :
 * <ul>
 * <li>Properties View instancing correct {@link IPropertySource} according to {@link ItemPropertyDescriptor}</li>
 * <li>Tree Viewer</li>
 * </ul>
 *
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class PropertySourceProvider
    extends AdapterFactoryContentProvider {

    /**
     * This constructs an instance that wraps this factory. The factory should yield adapters that implement the various
     * IItemContentProvider interfaces. If the adapter factory is an {@link IChangeNotifier}, a listener is added to it,
     * so it's important to call {@link #dispose()}.
     *
     * @param pAdapterFactory The adapter factory
     */
    public PropertySourceProvider(final AdapterFactory pAdapterFactory) {
        super(pAdapterFactory);
    }

    /**
     * Create and return an instance of {@link ExtendedPropertySource}.
     *
     * @param pObject Object to display in property view
     * @param pItemPropertySource Delegated implementation
     * @return The created {@link ExtendedPropertySource}
     */
    @Override
    protected IPropertySource createPropertySource(
        final Object pObject,
        final IItemPropertySource pItemPropertySource) {

        return new ExtendedPropertySource(pObject, pItemPropertySource);
    }
}
