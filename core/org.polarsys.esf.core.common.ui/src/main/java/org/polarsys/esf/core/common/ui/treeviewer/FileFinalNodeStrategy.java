/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.treeviewer;

import org.eclipse.core.resources.IFile;

/**
 * Validate node only if it is a file.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class FileFinalNodeStrategy
    implements IFinalNodeStrategy {

    /**
     * Default constructor.
     */
    public FileFinalNodeStrategy() {
    }

    /**
     * {@inheritDoc}
     * 
     * @return <code>true</code> if pNode is a file, <code>false</code> otherwise
     */
    @Override
    public boolean isFinal(final Object pNode) {
        return pNode instanceof IFile;
    }
}
