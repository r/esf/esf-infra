/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.resources.graphical;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;

/**
 * The activator class controls the plug-in life cycle.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFCoreResourcesGraphicalActivator extends EMFPlugin {

    /** The plug-in ID. */
	public static final String PLUGIN_ID = "org.polarsys.esf.core.resources.graphical"; //$NON-NLS-1$

	/** Keep track of this singleton activator. */
    private static final ESFCoreResourcesGraphicalActivator INSTANCE =
        new ESFCoreResourcesGraphicalActivator();

    /** The shared instance of this plugin. */
    private static Implementation sPlugin = null;

    /**
     * The default constructor.
     */
    public ESFCoreResourcesGraphicalActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance.
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the shared instance.
     *
     * @return The shared instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * Create an Error status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logError(final String pMessage, final Exception pException) {
        // Create the Error status
        IStatus vStatus = new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create a Warning status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logWarning(final String pMessage, final Exception pException) {
        // Create the Warning status
        IStatus vStatus = new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create an Info status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logInfo(final String pMessage, final Exception pException) {
        // Create the Info status
        IStatus vStatus = new Status(IStatus.INFO, PLUGIN_ID, IStatus.INFO, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * The actual implementation of the Eclipse <b>UIPlugin</b>.
     */
    public static class Implementation
        extends EclipseUIPlugin {

        /**
         * Default constructor.
         */
        public Implementation() {
            super();

            // Remember the static instance
            sPlugin = this;
        }
    }

}

