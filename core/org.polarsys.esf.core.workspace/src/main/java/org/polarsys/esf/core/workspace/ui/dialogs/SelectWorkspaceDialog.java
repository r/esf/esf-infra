/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.workspace.ui.dialogs;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.polarsys.esf.core.common.messages.Messages;
import org.polarsys.esf.core.workspace.WorkspaceActivator;
import org.polarsys.esf.core.workspace.utils.WorkspaceUtils;

/**
 * Dialog that lets/forces a user to enter/select a workspace that will be used
 * when saving all configuration files and settings.
 *
 * This dialog is shown at startup of the GUI just after the splash screen has shown.
 *
 * @see http://hexapixel.com/2009/01/12/rcp-workspaces
 * @author Emil Crumhorn
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class SelectWorkspaceDialog
    extends TitleAreaDialog {

    /** Messages provider. */
    private static final Messages MESSAGE_PROVIDER = WorkspaceActivator.getMessages();

    /** The dialog title, for a workspace switch. */
    private static final String DIALOG_TITLE_SWITCH_WORKSPACE =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.switch.title"); //$NON-NLS-1$

    /** The dialog title, for a workspace choice, during the application opening. */
    private static final String DIALOG_TITLE_SELECT_WORKSPACE =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.select.title"); //$NON-NLS-1$

    /** The dialog title, for a workspace choice, during the application opening. */
    private static final String DIALOG_BROWSE_TITLE =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.browse.title"); //$NON-NLS-1$

    /** The default message displayed in the dialog. */
    private static final String DIALOG_MESSAGE =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.switch.message"); //$NON-NLS-1$

    /** The information message, used to help the user. */
    private static final String DIALOG_INFO =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.switch.info"); //$NON-NLS-1$

    /** The error message when the user select an invalid path. */
    private static final String DIALOG_ERROR =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.error.message"); //$NON-NLS-1$

    /** The label used to introduce the workspace selection. */
    private static final String LABEL_WORKSPACE =
        MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.label.workspace"); //$NON-NLS-1$

    /** The combo viewer used to choose or edit a workspace path. */
    private ComboViewer mWorkspacePathsCombo = null;

    /** The checkbox specifying if the workspace choice must be remembered for the next time. */
    private Button mRememberCheckbox = null;

    /**
     * The workspace path selected.
     * NB : This property is used to cache the selection made in the combobox,
     * and ensure that this one will be available even after the widget is disposed.
     */
    private String mSelectedWorkspacePath = null;

    /** Link to the singleton instance of the utility class. */
    private WorkspaceUtils mWorkspaceUtils = WorkspaceUtils.getInstance();

    /**
     * Flag used to specify if the context is a workspace switch
     * manually asked by the user, or the automatic choice during
     * the application opening.
     */
    private boolean mSwitchWorkspace = false;

    /**
     * Creates a new workspace selection dialog.
     *
     * @param pSwitchWorkspace <code>true</code> if the user asked manually to switch of workspace
     */
    public SelectWorkspaceDialog(final boolean pSwitchWorkspace) {
        // Call the parent method
        super(Display.getDefault().getActiveShell());

        // Remember of the context
        mSwitchWorkspace = pSwitchWorkspace;
    }

    /**
     * {@inheritDoc}
     *
     * Customised to set the shell text.
     */
    @Override
    protected void configureShell(final Shell pShell) {
        super.configureShell(pShell);
        if (mSwitchWorkspace) {
            pShell.setText(DIALOG_TITLE_SWITCH_WORKSPACE);
        } else {
            pShell.setText(DIALOG_TITLE_SELECT_WORKSPACE);
        }
    }

    /**
     * {@inheritDoc}
     *
     * The dialog presentation is customised according to the context :
     * - If the application is opening.
     * - If the user asked specifically to switch of workspace.
     *
     */
    @Override
    protected Control createContents(final Composite pParent) {
        // Call the parent method
        Control vControl = super.createContents(pParent);

        // Set the dialog image
        setTitleImage(WorkspaceActivator.getPlugin().getImageRegistry().get(
            WorkspaceActivator.Implementation.ICON_WORKSPACE_KEY));

        // Set the dialog title and message according to the context
        if (mSwitchWorkspace) {
            setTitle(DIALOG_TITLE_SWITCH_WORKSPACE);
        } else {
            setTitle(DIALOG_TITLE_SELECT_WORKSPACE);
        }
        setMessage(DIALOG_MESSAGE);

        return vControl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Control createDialogArea(final Composite pParent) {
        // Call the parent method and get the dialog area composite built
        Composite vDialogArea = (Composite) super.createDialogArea(pParent);

        // Create a main composite to store all the widgets and set its layout
        Composite vMainComposite = new Composite(vDialogArea, SWT.NONE);
        GridLayout vGridLayout = new GridLayout(3, false);
        vGridLayout.marginTop = 10;
        vMainComposite.setLayout(vGridLayout);
        vMainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        // Create the introduction label
        Label vLabel = new Label(vMainComposite, SWT.NONE);
        vLabel.setText(LABEL_WORKSPACE);

        // Create the combo containing the workspace paths
        createWorkspacesPathsCombo(vMainComposite);

        // Create the browse button
        createBrowseButton(vMainComposite);

        // Create the checkbox to remember the workspace choice
        createRememberCheckbox(vMainComposite);

        return vMainComposite;

    }

    /**
     * Get the workspace path selected by the user in the combobox.
     *
     * @return The workspace path selected in the combobox.
     */
    public String getSelectedWorkspacePath() {
        return mSelectedWorkspacePath;
    }

    /**
     * Create the browse button, which will open a dialog
     * to allow the user to select a new workspace directory.
     *
     * @param pParent The parent composite
     */
    private void createBrowseButton(final Composite pParent) {
        // Create the browse button
        Button vBrowseButton = new Button(pParent, SWT.PUSH);
        vBrowseButton.setText(MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.button.browse")); //$NON-NLS-1$

        // Add the behaviour to react to a click
        vBrowseButton.addListener(SWT.Selection, new Listener() {

            /**
             * {@inheritDoc}
             */
            @Override
            public void handleEvent(final Event pEvent) {

                // Build a dialog to select a directory as workspace root
                DirectoryDialog vDirectoryDialog = new DirectoryDialog(getParentShell());
                vDirectoryDialog.setText(DIALOG_BROWSE_TITLE);
                vDirectoryDialog.setMessage(DIALOG_INFO);

                // Get the value currently selected in the combobox
                // If a directory is specified, open the dialog to this location
                String vWorkspacePath = getSelectedWorkspacePath();
                if (vWorkspacePath != null) {
                    vDirectoryDialog.setFilterPath(vWorkspacePath);
                }

                // Open the dialog and get its returned value
                String vDirectorySelected = vDirectoryDialog.open();
                if (StringUtils.isNotBlank(vDirectorySelected)) {

                    // Get the set of available workspaces
                    Set<String> vAvailableWorkspacesSet = mWorkspaceUtils.getAvailableWorkspacesSet();

                    // Update the set of available workspace by adding
                    // the new directory (this won't change anything if it's already contained)
                    vAvailableWorkspacesSet.add(vDirectorySelected);

                    // Update the combobox input, and select the workspace specified
                    mWorkspacePathsCombo.setInput(vAvailableWorkspacesSet);
                    mWorkspacePathsCombo.setSelection(new StructuredSelection(vDirectorySelected));
                }
            }

        });
    }

    /**
     * Create the combobox used to select a workspace amongst those available.
     *
     * @param pParent The parent composite
     */
    private void createWorkspacesPathsCombo(final Composite pParent) {
        // Create the combobox
        mWorkspacePathsCombo = new ComboViewer(pParent, SWT.DROP_DOWN | SWT.READ_ONLY);

        // Set its layout data to ensure a minimum width
        GridData vGridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        vGridData.minimumWidth = 340;
        mWorkspacePathsCombo.getCombo().setLayoutData(vGridData);

        // Add a listener to react to the selection change
        mWorkspacePathsCombo.addSelectionChangedListener(new ISelectionChangedListener() {

            /**
             * {@inheritDoc}
             *
             * Remember of the selected value in a cached property.
             */
            @Override
            public void selectionChanged(final SelectionChangedEvent pEvent) {
                // Get the selection from the event
                ISelection vSelection = pEvent.getSelection();

                // Reset the cached value
                mSelectedWorkspacePath = null;

                // Ensure that the selection is valid
                if (vSelection instanceof IStructuredSelection) {
                    IStructuredSelection vStructuredSelection = (IStructuredSelection) vSelection;

                    if (!vStructuredSelection.isEmpty()) {
                        // Update the cached value with the new selected path
                        mSelectedWorkspacePath = vStructuredSelection.getFirstElement().toString();

                        // Remove the potential error message already displayed
                        setErrorMessage(null);
                    }
                }
            }
        });

        // Populate the combo with all the workspaces available
        mWorkspacePathsCombo.setContentProvider(new ArrayContentProvider());
        mWorkspacePathsCombo.setInput(mWorkspaceUtils.getAvailableWorkspacesSet());

        // Get the workspace to select by default, it can be, by order :
        // - the current workspace,
        // - the last workspace saved in the preference,
        // - or the default value
        String vWorkspacePathSelected = mWorkspaceUtils.getCurrentWorkspacePath();
        if (StringUtils.isBlank(vWorkspacePathSelected)) {
            // No workspace is currently used, get the last saved location
            vWorkspacePathSelected = mWorkspaceUtils.getLastWorkspacePath();

            if (StringUtils.isBlank(vWorkspacePathSelected)) {
                // No preference has been found, get the default value
                vWorkspacePathSelected = WorkspaceUtils.DEFAULT_WORKSPACE_PATH;
            }
        }

        // Finally select the right workspace path in the combo
        mWorkspacePathsCombo.setSelection(new StructuredSelection(vWorkspacePathSelected));

    }

    /**
     * Create the checkbox used to specify if the workspace choice must be remembered
     * for the next time.
     *
     * @param pParent The parent composite
     */
    private void createRememberCheckbox(final Composite pParent) {

        // First add an empty label to fill the first cell in the
        // grid layout and place correctly the checkbox
        new Label(pParent, SWT.NONE);

        // Create the checkbox used to let the user select if he wants to apply
        // his choice directly the next times
        mRememberCheckbox = new Button(pParent, SWT.CHECK);
        mRememberCheckbox.setText(MESSAGE_PROVIDER.getString("SelectWorkspaceDialog.checkbox.remember")); //$NON-NLS-1$

        // Initialise the checkbox state with the value stored
        // in the preferences
        mRememberCheckbox.setSelection(mWorkspaceUtils.isRememberWorkspace());
    }

    /**
     * {@inheritDoc}
     *
     * When the user validate the dialog, his choices are saved in the preferences
     * and the workspace is validated and created if necessary.
     */
    @Override
    protected void okPressed() {
        // Get the workspace currently selected
        String vSelectedWorkspacePath = getSelectedWorkspacePath();

        // Get the current state of the checkbox used to known if the workspace choice must be remembered
        boolean vRememberWorkspace = mRememberCheckbox.getSelection();

        // Ensure that at least something is selected
        if (StringUtils.isNotBlank(vSelectedWorkspacePath)) {

            // Check if the workspace directory is valid and get the returned message
            String vErrorMsg = WorkspaceUtils.checkAndCreateWorkspaceDirectory(vSelectedWorkspacePath);
            if (StringUtils.isBlank(vErrorMsg)) {

                // All is ok, save the user's choices in the preferences
                mWorkspaceUtils.saveWorkspacePreferences(vSelectedWorkspacePath, vRememberWorkspace);

                // Call the parent method to close the dialog and continue
                super.okPressed();

            } else {
                // Display the error message to warn the user
                setErrorMessage(vErrorMsg);
            }

        } else {
            // Display the standard error as no path is selected
            setErrorMessage(DIALOG_ERROR);
        }
    }
}
