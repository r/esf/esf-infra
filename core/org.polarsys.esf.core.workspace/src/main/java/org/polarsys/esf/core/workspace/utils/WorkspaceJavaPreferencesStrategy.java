/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.workspace.utils;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.commons.lang3.StringUtils;
import org.polarsys.esf.core.workspace.WorkspaceActivator;

/**
 * Implementation of the strategy allowing to save and load the workspace properties
 * using the standard Java Preferences API mechanism.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class WorkspaceJavaPreferencesStrategy
    implements IWorkspacePreferencesStrategy {

    /** Key of the preference used to store the last workspace path. */
    private static final String KEY_LAST_WORKSPACE = "LAST_WORKSPACE"; //$NON-NLS-1$

    /** Key of the preference used to store the flag specifying if we must remember of the last workspace used. */
    private static final String KEY_REMEMBER_WORKSPACE = "REMEMBER_WORKSPACE"; //$NON-NLS-1$

    /** Key of the preference used to store the concatenation of all the available workspaces paths. */
    private static final String KEY_AVAILABLE_WORKSPACES = "AVAILABLE_WORKSPACES"; //$NON-NLS-1$

    /** Separator used in the concatenation all all the available workspace paths. */
    private static final String PATH_SEPARATOR = "\n"; //$NON-NLS-1$

    /** Maximum number of workspace paths stored in the preferences. */
    private static final int MAX_NB_PATHS_STORED = 10;

    /** Preferences store used to save all the preferences relative to the workspace. */
    private static final Preferences PREFERENCES = Preferences
        .userNodeForPackage(WorkspaceJavaPreferencesStrategy.class);

    /**
     * Default constructor.
     */
    public WorkspaceJavaPreferencesStrategy() {
    }

    /**
     * {@inheritDoc}
     * 
     * To store the workspaces set in the preferences, the path are concatenated with
     * a custom separator '\n'.
     */
    @Override
    public Set<String> getAvailableWorkspacesSet() {

        // Get the preferences value, containing all
        // the workspaces paths concatenated
        String vAvailableWorkspacesAsString = PREFERENCES.get(KEY_AVAILABLE_WORKSPACES, StringUtils.EMPTY);

        // Build the set by splitting the paths
        // TODO : The TreeSet will organise the paths alphabetically but we must
        // manage a different order : from the most recent to the older one
        Set<String> vAvailableWorkspacesSet = new TreeSet<String>();
        if (StringUtils.isNotBlank(vAvailableWorkspacesAsString)) {
            String[] vAvailableWorkspacesArray = vAvailableWorkspacesAsString.split(PATH_SEPARATOR);
            for (String vCurrentPath : vAvailableWorkspacesArray) {
                // Ensure that the path is not empty and add it
                if (StringUtils.isNotEmpty(vCurrentPath)) {
                    vAvailableWorkspacesSet.add(vCurrentPath);
                }
            }
        }

        // Add also the default path in the set
        vAvailableWorkspacesSet.add(WorkspaceUtils.DEFAULT_WORKSPACE_PATH);

        // Ensure that the current workspace is also in the set
        // NB : This may not be the case if the workspace location was given with a command line argument
        String vCurrentWorkspacePath = WorkspaceUtils.getInstance().getCurrentWorkspacePath();
        if (StringUtils.isNotEmpty(vCurrentWorkspacePath)) {
            vAvailableWorkspacesSet.add(vCurrentWorkspacePath);
        }

        // And finally add the last workspace used
        // NB : It will be normally already in the list. But if the maximum
        // size has been reached, it may have been removed
        String vLastWorkspace = getLastWorkspacePath();
        if (StringUtils.isNotEmpty(vLastWorkspace)) {
            vAvailableWorkspacesSet.add(vLastWorkspace);
        }

        return vAvailableWorkspacesSet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRememberWorkspace() {
        return PREFERENCES.getBoolean(KEY_REMEMBER_WORKSPACE, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLastWorkspacePath() {
        return PREFERENCES.get(KEY_LAST_WORKSPACE, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveWorkspacePreferences(
        final String pWorkspacePath,
        final boolean pRememberWorkspace,
        final Set<String> pAvailableWorkspacesSet) {

        // Create a string concatenation of all the available workspaces
        // But keep only the maximum number of workspaces allowed
        StringBuilder vWorkspacePathsConcatenation = new StringBuilder();
        if (pAvailableWorkspacesSet != null) {
            Iterator<String> vPathsIterator = pAvailableWorkspacesSet.iterator();
            int i = 0;
            while (vPathsIterator.hasNext() && i < MAX_NB_PATHS_STORED) {
                // Get the next value and append it
                vWorkspacePathsConcatenation.append(vPathsIterator.next());
                vWorkspacePathsConcatenation.append(PATH_SEPARATOR);

                // Prepare the next iteration
                i++;
            }
        }

        // Finally save all the preferences values
        // ... The flag to remember the last workspace
        PREFERENCES.putBoolean(KEY_REMEMBER_WORKSPACE, pRememberWorkspace);
        // ... The last workspace used
        PREFERENCES.put(KEY_LAST_WORKSPACE, pWorkspacePath);
        // ... The concatenation of all the available workspaces
        PREFERENCES.put(KEY_AVAILABLE_WORKSPACES, vWorkspacePathsConcatenation.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearWorkspacePreferences() {
        try {
            // Clear all the preferences in this node
            PREFERENCES.clear();
        } catch (final BackingStoreException pException) {
            WorkspaceActivator.logError("Error during the preferences clear", pException);
        }
    }

}
