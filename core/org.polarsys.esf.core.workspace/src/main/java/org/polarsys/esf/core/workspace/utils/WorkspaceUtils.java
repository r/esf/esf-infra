/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.workspace.utils;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.service.datalocation.Location;
import org.polarsys.esf.core.workspace.WorkspaceActivator;
import org.polarsys.esf.core.workspace.ui.dialogs.SelectWorkspaceDialog;

/**
 * Utility class used to store all the available tools and methods
 * to work with the workspaces.
 * 
 * @see http://hexapixel.com/2009/01/12/rcp-workspaces
 * @author Emil Crumhorn
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class WorkspaceUtils {
   
    /** The default path of the workspace, located to the user home directory. */
    public static final String DEFAULT_WORKSPACE_PATH =
        System.getProperty("user.home") //$NON-NLS-1$
            + File.separatorChar
            + WorkspaceActivator.getMessages().getString("WorkspaceUtils.default.workspace"); //$NON-NLS-1$

    /** The error message displayed when the workspace content is not accessible. */
    private static final String ERROR_MSG_CONTENT_NOT_WRITABLE = 
        WorkspaceActivator.getMessages().getString("WorkspaceUtils.error.content.rights"); //$NON-NLS-1$
    
    /** The error message displayed when the workspace is not accessible. */
    private static final String ERROR_MSG_NOT_WRITABLE = 
        WorkspaceActivator.getMessages().getString("WorkspaceUtils.error.rights"); //$NON-NLS-1$
    
    /** The error message displayed when the workspace path is not a valid directory. */
    private static final String ERROR_MSG_NOT_DIRECTORY = 
        WorkspaceActivator.getMessages().getString("WorkspaceUtils.error.notdirectory"); //$NON-NLS-1$
     
    /** Instance of the class. */
    private static WorkspaceUtils sInstance = new WorkspaceUtils();

    /**
     * Cached set of workspace paths.
     * It must not be accessed directly, but by the linked getter
     * to manage correctly its initialisation.
     */
    private Set<String> mAvailableWorkspacesSet = null;

    /** Implementation of the strategy used to store and load the preferences relative to the workspace. */
    private IWorkspacePreferencesStrategy mPreferencesStrategy = new WorkspaceJavaPreferencesStrategy();

    /**
     * Default constructor.
     * Private as it's a utility class with a singleton.
     */
    private WorkspaceUtils() {
    }

    /**
     * Getter for the unique class instance.
     * 
     * @return The singleton instance
     */
    public static WorkspaceUtils getInstance() {
        return sInstance;
    }

    /**
     * Return the set of workspaces available.
     * This set is initialised with the workspace previously used and known,
     * and the default workspace value.
     * 
     * The returned list is computed if necessary from the preferences,
     * and then cached.
     * 
     * @return The set of workspaces known
     */
    public Set<String> getAvailableWorkspacesSet() {

        if (mAvailableWorkspacesSet == null) {
            // Initialise the cached set with the values stored
            // in the preferences
            mAvailableWorkspacesSet = mPreferencesStrategy.getAvailableWorkspacesSet();
        }

        return mAvailableWorkspacesSet;
    }

    /**
     * Return a flag specifying if the user wants to save his choice
     * for the next time.
     * 
     * If he saves his choice, he won't be prompted to select a workspace on
     * the next start.
     * 
     * @return The value of the flag
     */
    public boolean isRememberWorkspace() {
        // Search the flag value in the preferences
        return mPreferencesStrategy.isRememberWorkspace();
    }

    /**
     * Return the last workspace path used.
     * 
     * @return The last workspace path used, or null if none
     */
    public String getLastWorkspacePath() {
        // Search the path in the preferences
        return mPreferencesStrategy.getLastWorkspacePath();
    }

    /**
     * Return the current workspace path.
     * It may be different from the last workspace path stored in the preference
     * as the user may have start its application with a workspace location specified
     * in the command line.
     *
     * @return The current workspace path, or null if it's not initialised
     */
    public String getCurrentWorkspacePath() {
        String vWorkspacePath = null;

        // Ensure that the workspace location is initialised
        Location vWorkspaceLocation = Platform.getInstanceLocation();
        if (vWorkspaceLocation.isSet()) {
            // Get the workspace path
            vWorkspacePath = vWorkspaceLocation.getURL().getPath();
        }

        // Return its path as String
        return vWorkspacePath;
    }

    /**
     * Ensures that a workspace path is valid in regards of reading/writing rights, etc.
     * and try to create if necessary the directory if it doesn't exist.
     * 
     * @param pWorkspacePath The path of the workspace to check
     * @return null if everything is ok, or an error message if not
     */
    public static String checkAndCreateWorkspaceDirectory(final String pWorkspacePath) {

        String vErrorMsg = null;

        if (pWorkspacePath != null) {
            
            // Create a File pointing to the given path
            File vWorkspaceDirectory = new File(pWorkspacePath);
    
            // Ensure that the workspace directory exists and that the application can
            // read and write in this directory
            if (!vWorkspaceDirectory.isDirectory()) {
                // ... The path doesn't correspond to an existing directory
                //     Thus try to create it
                
                boolean vIsDirectoryCreated = false;
                try {
                    // Try to create the directory
                    vIsDirectoryCreated = vWorkspaceDirectory.mkdir();
                } catch (final SecurityException pException) {
                    // The user doesn't have the rights to create the directory 
                    vErrorMsg = ERROR_MSG_CONTENT_NOT_WRITABLE;
                }
                
                if (!vIsDirectoryCreated) {
                    // The directory has not been created, but not because of the rights access
                    vErrorMsg = ERROR_MSG_NOT_DIRECTORY;    
                }
                
            } else if (!(vWorkspaceDirectory.canRead() && vWorkspaceDirectory.canWrite())) {
                // ... The directory exists but can be accessed for read and write operations
                vErrorMsg = ERROR_MSG_NOT_WRITABLE;
    
            } else {
                // ... The directory exists but verify if its content can be accessed for read and write operations
                // TODO : This is mandatory to check with a real file creation as in Java 1.6, a bug is known :
                //        http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6203387
                //        On Java 1.7, the bug is corrected
                
                // Create an empty file object located in the selected workspace directory
                File vCheckRightsFile = new File(vWorkspaceDirectory, "checkRights.tmp"); //$NON-NLS-1$
                
                try {
                    // Try to create the empty file and delete it directly
                    vCheckRightsFile.createNewFile();
                    vCheckRightsFile.delete();
                
                } catch (final IOException pException) {
                    vErrorMsg = ERROR_MSG_CONTENT_NOT_WRITABLE;
                } catch (final SecurityException pException) {
                    vErrorMsg = ERROR_MSG_CONTENT_NOT_WRITABLE;
                }
            }
        
        } else {
            // ... The path is null and thus doesn't correspond to a directory
            vErrorMsg = ERROR_MSG_NOT_DIRECTORY;
        }
            
        return vErrorMsg;
    }

    /**
     * Get the workspace path directly from the last execution if the user wanted to remember of his choice,
     * or ask to the user which workspace must be opened using a specific dialog.
     * 
     * @return The workspace path, or null if no valid workspace has been specified
     */
    public String getWorkspacePath() {

        String vWorkspacePath = null;

        // Get the preference value to know if the user wanted to remember of the chosen workspace location
        boolean vRememberWorkspace = isRememberWorkspace();

        // Get the preference value corresponding to the last used workspace path
        String vLastWorkspacePath = getLastWorkspacePath();

        // Check if the last workspace has been saved to remember it
        if (vRememberWorkspace) {

            // Ensure that the last workspace location is valid
            if (StringUtils.isBlank(vLastWorkspacePath)) {
                // If the location is not valid, no need to continue to believe that we remember it
                vRememberWorkspace = false;

            } else {

                // Check the workspace location and get the potential error message returned
                String vCheckWorkspaceErrorMsg = checkAndCreateWorkspaceDirectory(vLastWorkspacePath);

                if (vCheckWorkspaceErrorMsg != null) {
                    // If the location is not valid, no need to continue to believe
                    // that we remember it
                    vRememberWorkspace = false;
                }
            }
        }

        // If we don't remember the workspace, show the dialog to let the user choose one
        if (!vRememberWorkspace) {
            // Build the dialog instance
            SelectWorkspaceDialog vSelectWorkspaceDialog = new SelectWorkspaceDialog(false);

            // Open the dialog and get the returned value
            int vAnswer = vSelectWorkspaceDialog.open();

            // If the user cancelled or if the workspace path is not valid, we can't do anything
            // so the path is reset
            if (vAnswer == Window.CANCEL
                || StringUtils.isBlank(vSelectWorkspaceDialog.getSelectedWorkspacePath())) {

                // Reset the workspace location
                vWorkspacePath = null;

            } else {
                // The workspace selected is valid, update the location and continue
                vWorkspacePath = vSelectWorkspaceDialog.getSelectedWorkspacePath();
            }

        } else {
            // Update the location with the last one used and continue
            vWorkspacePath = vLastWorkspacePath;
        }

        return vWorkspacePath;
    }

    /**
     * Save the preferences relative to the workspace properties.
     * 
     * The last workspace path, and the flag used to remember or not of the user choice are given
     * in parameter. The list of available workspaces is also saved, and taken directly from the
     * cached list in this instance.
     * 
     * @param pWorkspacePath The path of the last workspace used
     * @param pRememberWorkspace <code>true</code> if the last workspace choice must be remembered for the next time
     */
    public void saveWorkspacePreferences(final String pWorkspacePath, final boolean pRememberWorkspace) {
        // Delegate the save to the strategy implementation
        mPreferencesStrategy.saveWorkspacePreferences(pWorkspacePath, pRememberWorkspace, mAvailableWorkspacesSet);
    }

    /**
     * Utility method used to clear the preferences.
     * This is not currently accessible to the user, but it may be the case in the future.
     */
    public void clearWorkspacePreferences() {
        // Delegate the clear to the strategy implementation
        mPreferencesStrategy.clearWorkspacePreferences();
    }
}
