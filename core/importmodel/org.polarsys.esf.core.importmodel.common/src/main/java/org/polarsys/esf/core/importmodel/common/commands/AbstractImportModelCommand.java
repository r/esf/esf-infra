/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.importmodel.common.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.uml2.uml.Package;

/**
 * Abstract command class for import model.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public abstract class AbstractImportModelCommand
    extends AbstractTransactionalCommand {

    /** Selected Elements. */
    private List<EObject> mSelectedElements = null;

    /** Runnable. */
    private Runnable mRunnable = null;

    /** Description. */
    private String mDescription = null;

    /**
     * Default constructor.
     *
     * @param pEditingDomain The editing domain
     * @param pSelectedElements The selected elements
     * @param pRunnable The runnable instance
     * @param pLabel The label
     * @param pDescription The description
     */
    protected AbstractImportModelCommand(
        final TransactionalEditingDomain pEditingDomain,
        final List<EObject> pSelectedElements,
        final Runnable pRunnable,
        final String pLabel,
        final String pDescription) {

        super(pEditingDomain, pLabel, computeAffectedFiles(pSelectedElements));

        mSelectedElements = pSelectedElements;
        mRunnable = pRunnable;
        mDescription = pDescription;
    }

    /**
     * Check if the selected element is a Package before confirming the execution.
     *
     * {@inheritDoc}
     */
    @Override
    public boolean canExecute() {
        boolean vExecutable = false;
        if (!mSelectedElements.isEmpty()) {
            vExecutable = mSelectedElements.get(0) instanceof Package;
        }
        return vExecutable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CommandResult doExecuteWithResult(final IProgressMonitor pMonitor, final IAdaptable pInfo)
        throws ExecutionException {
        if (mRunnable != null) {
           mRunnable.run();
        }
        return CommandResult.newOKCommandResult();
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Retrieve the files of selected elements.
     *
     * @param pSelected The selected elements
     * @return The list of files of the selected elements
     */
    static List<IFile> computeAffectedFiles(final Collection<?> pSelected) {
        Set<IFile> vFilesSet = new LinkedHashSet<IFile>();
        EObject vEObject = null;
        Resource vResource = null;
        
        for (Object vNext : pSelected) {
            if (vNext instanceof EObject) {
                vEObject = (EObject) vNext;
                vResource = vEObject.eResource();
                if (vResource != null) {
                    IFile vFile = WorkspaceSynchronizer.getFile(vResource);
                    if (vFile != null) {
                        vFilesSet.add(vFile);
                    }
                }
            }
        }

        return new ArrayList<IFile>(vFilesSet);
    }
}
