/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.importmodel.common.handlers;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.polarsys.esf.core.importmodel.common.commands.UMLImportModelFromFileCommand;

/**
 * Handler class for import UML models.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class UMLImportModelHandler
    extends AbstractImportModelHandler {

    /**
     * Default constructor.
     */
    public UMLImportModelHandler() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ICommand getGMFCommand(TransactionalEditingDomain domain) {
        return new UMLImportModelFromFileCommand(
			domain,
            getSelectedElements(),
            getSelection());
    }
}
