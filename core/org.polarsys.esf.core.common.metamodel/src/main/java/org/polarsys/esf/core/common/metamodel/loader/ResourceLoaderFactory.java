/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel.loader;

import org.eclipse.uml2.uml.Model;

/**
 * Factory for metamodel resource loader.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class ResourceLoaderFactory {

    /** Unique instance. */
    private static ResourceLoaderFactory sInstance = new ResourceLoaderFactory();

    /**
     * Default constructor.
     */
    private ResourceLoaderFactory() {
    }

    /**
     * @return Singleton instance of resource loader factory
     */
    public static ResourceLoaderFactory getInstance() {
        return sInstance;
    }

    /**
     * @return Resource loader for ESF model instance file
     */
    public IResourceLoader<Model> getModelLoader() {
        // TODO : Create the implementation of IResourceLoader and use them here
        return null; // new ModelLoader();
    }
}
