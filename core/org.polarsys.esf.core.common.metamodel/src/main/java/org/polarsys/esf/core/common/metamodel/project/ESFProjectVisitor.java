/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel.project;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;

/**
 * Implemented resource visitor to find files in a project or folder.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ESFProjectVisitor
    implements IResourceVisitor {

    /** UML file found in project. */
    private IFile mUMLFile = null;

    /**
     * Default constructor.
     */
    public ESFProjectVisitor() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean visit(final IResource pResource) throws CoreException {
        boolean vContinue = checkContinue();

        if (vContinue && pResource instanceof IFile) {
            IFile vFile = (IFile) pResource;

            // TODO : Use the right constant for file extension, from UML Package
            boolean vIsSAFile = "uml".equals(vFile.getFileExtension());

            if (vIsSAFile) {
                mUMLFile = vFile;
            }

            vContinue = checkContinue();
        }

        return vContinue;
    }

    /**
     * Check condition for continuing visit.
     *
     * @return false if all file is found, otherwise true.
     */
    private boolean checkContinue() {
        return mUMLFile == null;
    }

    /**
     * @return First found sa file in project.
     */
    public IFile getUMLFile() {
        return mUMLFile;
    }
}
