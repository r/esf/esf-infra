/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel.loader.impl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

/**
 * Abstract resource loader that contain common implementation to load a EMF resource.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public abstract class AbstractResourceLoader {

    /**
     * Default constructor.
     */
    public AbstractResourceLoader() {
    }

    /**
     * Get resource from URI.
     * 
     * @param pURI URI where resource is located.
     * @return Model from resource.
     */
    protected EObject getResource(final URI pURI) {
        EObject vResultModel = null;

        // Obtain a new resource set
        ResourceSet vResourceSet = new ResourceSetImpl();

        // Get the resource
        Resource vResource = vResourceSet.getResource(pURI, true);

        // Get the first model element and cast it to the right type
        // As model is full hierarchical metamodel, first found is master one
        vResultModel = vResource.getContents().get(0);

        vResource.setTrackingModification(true);

        return vResultModel;
    }
}
