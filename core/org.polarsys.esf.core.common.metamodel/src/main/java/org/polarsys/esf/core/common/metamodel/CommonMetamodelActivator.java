/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel;

import java.util.ResourceBundle;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.osgi.framework.BundleContext;
import org.polarsys.esf.core.common.messages.Messages;

/**
 * 
 * Central singleton for the Common Metamodel plugin.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class CommonMetamodelActivator
    extends EMFPlugin {

    /**
     * Keep track of the encapsulating singleton.
     */
    public static final CommonMetamodelActivator INSTANCE = new CommonMetamodelActivator();

    /**
     * Keep track of the implementation singleton.
     */
    private static Implementation sPlugin = null;

    /** Messages class used to find localised string. */
    private static Messages sMessages = new Messages(ResourceBundle.getBundle(Messages.BUNDLE_NAME));

    /**
     * Create the instance.
     * 
     */
    public CommonMetamodelActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     * 
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * @return The messages class used to return localised string
     */
    public static Messages getMessages() {
        return sMessages;
    }

    /**
     * Create an Error status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logError(final String pMessage, final Exception pException) {
        // Create the Error status
        IStatus vStatus = new Status(IStatus.ERROR, sPlugin.getSymbolicName(), IStatus.ERROR, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create a Warning status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logWarning(final String pMessage, final Exception pException) {
        // Create the Warning status
        IStatus vStatus = new Status(IStatus.WARNING, sPlugin.getSymbolicName(), IStatus.WARNING, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create an Info status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logInfo(final String pMessage, final Exception pException) {
        // Create the Info status
        IStatus vStatus = new Status(IStatus.INFO, sPlugin.getSymbolicName(), IStatus.INFO, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * The actual implementation of the Eclipse <b>UIPlugin</b>.
     */
    public static class Implementation
        extends EclipsePlugin {

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember the static instance
            sPlugin = this;

        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void start(final BundleContext pContext) throws Exception {
            super.start(pContext);

        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void stop(final BundleContext pContext) throws Exception {

        }
    }

}
