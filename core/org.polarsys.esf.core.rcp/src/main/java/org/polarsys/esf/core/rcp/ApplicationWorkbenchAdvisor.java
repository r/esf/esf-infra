/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.rcp;

import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.polarsys.esf.core.framework.workbenchadvisor.AbstractWorkbenchAdvisorWithImages;

/**
 * First class to be called during the workbench initialisation.
 * It allows the customisation of some methods linked to
 * the Workbench life cycle.
 *
 * <p>
 * It's in this class that the default perspective ID is defined.
 * </p>
 *
 * @author $Author: jdumont $
 * @version $Revision: 90 $
 */
public class ApplicationWorkbenchAdvisor
    extends AbstractWorkbenchAdvisorWithImages {

    /** Id of the perspective to open by default. */
    private static final String PERSPECTIVE_ID = "org.polarsys.esf.core.ui.perspective.design"; //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public ApplicationWorkbenchAdvisor() {
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchAdvisor#createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer)
     *
     * @param pConfigurer The configuration class
     * @return WorkbenchWindowAdvisor The created object
     */
    @Override
    public final WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer pConfigurer) {
        return new ApplicationWorkbenchWindowAdvisor(pConfigurer);
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchAdvisor#getInitialWindowPerspectiveId()
     *
     * Method used to get the initial perspective id, which will be opened with the application
     * @return String Id of the default perspective
     */
    @Override
    public final String getInitialWindowPerspectiveId() {
        return PERSPECTIVE_ID;
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchAdvisor#initialize(org.eclipse.ui.application.IWorkbenchConfigurer)
     *
     * Method used to initialise the ApplicationWorkbench with some specific configurations.
     * @param pConfigurer The configuration class
     */
    @Override
    public final void initialize(final IWorkbenchConfigurer pConfigurer) {
        // Call the parent method
        super.initialize(pConfigurer);
    }
}
