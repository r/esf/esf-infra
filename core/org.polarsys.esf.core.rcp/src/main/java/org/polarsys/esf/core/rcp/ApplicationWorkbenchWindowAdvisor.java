/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.rcp;

import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

/**
 * Second class to be called during the workbench initialization. For example, it allows the customisation of how the
 * window is displayed, just before its opening
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ApplicationWorkbenchWindowAdvisor
    extends WorkbenchWindowAdvisor {

    /**
     * Default constructor.
     *
     * @param pConfigurer The configuration class
     */
    public ApplicationWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer pConfigurer) {
        super(pConfigurer);
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#createActionBarAdvisor(org.eclipse.ui.application.IActionBarConfigurer)
     * @param pConfigurer The configuration class
     * @return ActionBarAdvisor The object created
     */
    @Override
    public final ActionBarAdvisor createActionBarAdvisor(final IActionBarConfigurer pConfigurer) {
        return new ApplicationActionBarAdvisor(pConfigurer);
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#preWindowOpen()
     *
     * This method is called before the window opening. Thus, it's here that the window presentation must be customised.
     */
    @Override
    public final void preWindowOpen() {
        // Get the windows configurer
        final IWorkbenchWindowConfigurer vConfigurer = getWindowConfigurer();

        // Set the window presentation
        vConfigurer.setInitialSize(new Point(800, 600));
        vConfigurer.setShowCoolBar(true);
        vConfigurer.setShowStatusLine(true);
        vConfigurer.setShowPerspectiveBar(true);
        vConfigurer.setShowProgressIndicator(true);

        // The values in product file are unreachable. Then the window title management must be done manually to
        // personalize it
        vConfigurer.setTitle("ESF " + CoreRootActivator.getMessages().getString("Application.version"));
    }

    /**
     * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
     *
     * This method is called after the window opening. It can be used to set some values, etc.
     */
    @Override
    public final void postWindowOpen() {

    }

}
