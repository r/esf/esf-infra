/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.rcp;

import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.polarsys.esf.core.workspace.utils.WorkspaceUtils;

/**
 * This class controls all aspects of the application's execution.
 *
 * When the application is started, it will ensure that the user can select a valid workspace.
 *
 * <p>
 * As this class is the one specified in the extension point 'org.eclipse.core.runtime.applications', it will be the
 * first called, just after the Eclipse RCP framework initialisation. It can be modified, for example to add a login
 * dialog.
 * </p>
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class Application
    implements IApplication {

    /** Prefix used to build an URL with a file context. */
    private static final String URL_FILE_PREFIX = "file"; //$NON-NLS-1$

    /** Message title when an error occurred with the workspace selection. */
    private static final String WORKSPACE_ERROR_TITLE =
        CoreRootActivator.getMessages().getString("Application.workspace.error.title"); //$NON-NLS-1$

    /** Message when no valid workspace is found. */
    private static final String WORKSPACE_ERROR_MESSAGE =
        CoreRootActivator.getMessages().getString("Application.workspace.error.message"); //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public Application() {
    }

    /**
     * {@inheritDoc}
     *
     * Customise the start behaviour, to let the user choose the workspace location with
     * a prompt dialog is no default location is given with the "-data" argument. If a location
     * is directly given in the command line, use it.
     *
     * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
     */
    @Override
    public final Object start(final IApplicationContext pContext) throws Exception {

        // Create the exit object indicating the application state when exiting
        Integer vExitObject = IApplication.EXIT_OK;
        boolean vRunWorkbench = true;

        // First, create the platform display
        final Display vDisplay = PlatformUI.createDisplay();

        // Get the current workspace location, which may be modified after
        Location vWorkspaceLocation = Platform.getInstanceLocation();

        // Check if a default location is already given or not with the command line argument
        // If no default location is set, use the user preferences to define the workspace location
        if (!vWorkspaceLocation.isSet()) {

            // Get the workspace location wanted by the user or saved previously as a preference
            String vWorkspacePath = WorkspaceUtils.getInstance().getWorkspacePath();

            if (StringUtils.isNotBlank(vWorkspacePath)) {
                // Update the workspace location with the value found and continue
                vWorkspaceLocation.set(new URL(URL_FILE_PREFIX, null, vWorkspacePath), false);

            } else {
                // Open a dialog message to display the error
                MessageDialog.openError(vDisplay.getActiveShell(), WORKSPACE_ERROR_TITLE, WORKSPACE_ERROR_MESSAGE);

                // Remember to not run the workbench
                vRunWorkbench = false;
            }
        }

        // Finally, if the configuration is correct, try to create and run the application workbench
        if (vRunWorkbench) {
            try {
                final int vReturnCode =
                    PlatformUI.createAndRunWorkbench(vDisplay, new ApplicationWorkbenchAdvisor());
                if (vReturnCode == PlatformUI.RETURN_RESTART) {
                    vExitObject = IApplication.EXIT_RESTART;
                }
            } finally {
                vDisplay.dispose();
            }

        } else {
            // Close the application as there is no valid workspace
            System.exit(0);
        }

        return vExitObject;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.eclipse.equinox.app.IApplication#stop()
     */
    @Override
    public final void stop() {
        if (!PlatformUI.isWorkbenchRunning()) {
            return;
        }
        final IWorkbench vWorkbench = PlatformUI.getWorkbench();
        final Display vDisplay = vWorkbench.getDisplay();
        vDisplay.syncExec(new Runnable() {

            @Override
            public void run() {
                if (!vDisplay.isDisposed()) {
                    vWorkbench.close();
                }
            }
        });
    }
}
