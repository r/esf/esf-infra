/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.rcp.nature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

/**
 * Configure ESF nature for project in workspace.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ESFProjectNature
    implements IProjectNature {

    /** Project register as ESF project. */
    private IProject mProject = null;

    /**
     * Default constructor.
     */
    public ESFProjectNature() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure() throws CoreException {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deconfigure() throws CoreException {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IProject getProject() {
        return mProject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setProject(final IProject pProject) {
        mProject = pProject;

    }

}
