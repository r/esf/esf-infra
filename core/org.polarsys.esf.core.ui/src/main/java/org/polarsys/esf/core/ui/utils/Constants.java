/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui.utils;

import org.polarsys.esf.core.ui.CoreUIActivator;

/**
 * Contains constants values used in several classes.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class Constants {

    /** Resource folder containing all the wizards icons. */
    public static final String FOLDER_WIZARD_IMAGES = "wizards/"; //$NON-NLS-1$

    /** Id of the perspective Design. */
    public static final String PERSPECTIVE_DESIGN_ID =
        CoreUIActivator.getPlugin().getString("perspective.design.id"); //$NON-NLS-1$

    /** Id of the perspective Dashboard. */
    public static final String PERSPECTIVE_DASHBOARD_ID =
        CoreUIActivator.getPlugin().getString("perspective.dashboard.id"); //$NON-NLS-1$
    
    /** Id of the perspective Reports. */
    public static final String PERSPECTIVE_REPORTS_ID =
        CoreUIActivator.getPlugin().getString("perspective.reports.id"); //$NON-NLS-1$

    /**
     * Private constructor, as it's a final class which doesn't need
     * to be instantiated.
     */
    private Constants() {
    }
}
