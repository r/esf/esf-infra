/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Perspective Reports definition.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 90 $
 */
public class PerspectiveReportsFactory
    implements IPerspectiveFactory {
    
    /** Base used to build the ID of the report views, with a wild card to match all the potential secondary ids. */
    private static final String ID_REPORT_VIEW_WITH_WILDCARD = "org.polarsys.esf.report.ui.view.report:*"; //$NON-NLS-1$
    
    /** ID of ESF log view. */
    private static final String ID_SA_LOG_VIEW = "org.polarsys.esf.view.log"; //$NON-NLS-1$
    
    /** ID of FMEA view. */
    private static final String ID_FMEA_VIEW = "org.polarsys.esf.report.fmea.ui"; //$NON-NLS-1$

    /** ID of the folder placed on the top. */
    private static final String ID_FOLDER_TOP = "topFolderId"; //$NON-NLS-1$
    
    /** ID of the folder placed on the top right. */
    private static final String ID_FOLDER_TOPRIGHT = "topRightFolderId"; //$NON-NLS-1$
    
    /** ID of the folder placed on the bottom. */
    private static final String ID_FOLDER_BOTTOM = "bottomFolderId"; //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public PerspectiveReportsFactory() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void createInitialLayout(final IPageLayout pLayout) {
        // Get the editor area
        String vEditorArea = pLayout.getEditorArea();

        // At top right: Prepare the Report location and add the FMEA view 
        IFolderLayout vTopRightFolder = 
            pLayout.createFolder(ID_FOLDER_TOPRIGHT, IPageLayout.RIGHT, 0.25f, vEditorArea);
        vTopRightFolder.addPlaceholder(ID_REPORT_VIEW_WITH_WILDCARD);
        vTopRightFolder.addView(ID_FMEA_VIEW);
                
        // At bottom : Add the properties and log views
        IFolderLayout vBottomRightFolder = 
            pLayout.createFolder(ID_FOLDER_BOTTOM, IPageLayout.BOTTOM, 0.66f, vEditorArea);
        vBottomRightFolder.addView(IPageLayout.ID_PROP_SHEET);
        vBottomRightFolder.addView(ID_SA_LOG_VIEW);
        
        // On the top : Add the Project Explorer view
        IFolderLayout vTopLeftPlace = pLayout.createFolder(ID_FOLDER_TOP, IPageLayout.TOP, 0.5f, vEditorArea);
        vTopLeftPlace.addView(IPageLayout.ID_PROJECT_EXPLORER);

        // Finally hide the editor area in this perspective
        pLayout.setEditorAreaVisible(false);
    }
}
