/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui.rights;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ui.AbstractSourceProvider;

/**
 * Provider used to get the rights values of the user, to know if
 * he can access to forbidden activities.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class RightsSourceProvider
    extends AbstractSourceProvider {

    /** Name of the rights value used to allow the show of forbidden activities. */
    public static final String RIGHT_SHOW_FORBIDDEN = "showForbidden"; //$NON-NLS-1$

    /** Name of the rights value used to avoid the show of forbidden activities. */
    public static final String RIGHT_NOT_SHOW_FORBIDDEN = "notShowForbidden"; //$NON-NLS-1$

    /** Name of the variable used in the org.eclipse.ui.activities extensions. */
    public static final String RIGHTS_VARIABLE = "rightsVariable"; //$NON-NLS-1$

    /** Array of the variable names, considered as sources for this provider. */
    private static final String[] PROVIDED_SOURCE_NAMES = new String[] {RIGHTS_VARIABLE};

    /** Map of the variable states. */
    private static final Map<String, List<String>> VARIABLE_STATES_MAP = new HashMap<String, List<String>>();

    static {
        // Initialize the values for the rights variable,
        // to specify the user rights, as strings
        // For the moment, avoid the show of forbidden activities
        List<String> vRightsList = new ArrayList<String>();
        vRightsList.add(RIGHT_NOT_SHOW_FORBIDDEN);

        // Add the right value in the list of variable states
        VARIABLE_STATES_MAP.put(RIGHTS_VARIABLE, vRightsList); //$NON-NLS-1$
    }

    /**
     * Default constructor.
     */
    public RightsSourceProvider() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Map<String, List<String>> getCurrentState() {
        return VARIABLE_STATES_MAP;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String[] getProvidedSourceNames() {
        return PROVIDED_SOURCE_NAMES;
    }

    /**
     * This triggers an update of the rights variable state,
     * and will update also all listeners to the evaluation
     * service. So that every menu point, which is also expression
     * controlled, gets updated too.
     */
    public final void updateRights() {
        fireSourceChanged(0, getCurrentState());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dispose() {
    }
}
