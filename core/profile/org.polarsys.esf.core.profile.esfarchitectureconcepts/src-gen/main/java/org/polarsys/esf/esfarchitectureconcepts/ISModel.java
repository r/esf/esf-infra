/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SModel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getBase_Package <em>Base Package</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getOwnedSBlocksList <em>Owned SBlocks List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSModel()
 * @model
 * @generated
 */
public interface ISModel
		extends IAbstractSArchitectureElement {

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSModel_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Owned SBlocks List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISBlock}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel <em>SModel</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned SBlocks List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owned SBlocks List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSModel_OwnedSBlocksList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel
	 * @model opposite="sModel" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISBlock> getOwnedSBlocksList();

} // ISModel
