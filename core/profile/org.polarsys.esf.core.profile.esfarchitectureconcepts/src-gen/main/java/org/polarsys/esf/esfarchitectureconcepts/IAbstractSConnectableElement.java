/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SConnectable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSConnectorsList <em>SConnectors List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirection <em>SDirection</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirectionManual <em>SDirection Manual</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getAbstractSConnectableElement()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSConnectableElement
		extends IAbstractSArchitectureElement {

	/**
	 * Returns the value of the '<em><b>SConnectors List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISConnector}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getEndsList <em>Ends List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SConnectors List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SConnectors List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getAbstractSConnectableElement_SConnectorsList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getEndsList
	 * @model opposite="endsList" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISConnector> getSConnectorsList();

	/**
	 * Returns the value of the '<em><b>SDirection Manual</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link org.polarsys.esf.esfarchitectureconcepts.SDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SDirection Manual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SDirection Manual</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see #setSDirectionManual(SDirection)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getAbstractSConnectableElement_SDirectionManual()
	 * @model default="UNDEFINED" ordered="false"
	 * @generated
	 */
	SDirection getSDirectionManual();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirectionManual <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SDirection Manual</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see #getSDirectionManual()
	 * @generated
	 */
	void setSDirectionManual(SDirection value);

	/**
	 * Returns the value of the '<em><b>SDirection</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polarsys.esf.esfarchitectureconcepts.SDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SDirection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SDirection</em>' attribute.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getAbstractSConnectableElement_SDirection()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SDirection getSDirection();

} // IAbstractSConnectableElement
