/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getBase_Property <em>Base Property</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getType <em>Type</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getSPortRolesList <em>SPort Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPart()
 * @model
 * @generated
 */
public interface ISPart
		extends IAbstractSArchitectureElement {

	/**
	 * Returns the value of the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Property</em>' reference.
	 * @see #setBase_Property(Property)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPart_Base_Property()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Property getBase_Property();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getBase_Property <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Property</em>' reference.
	 * @see #getBase_Property()
	 * @generated
	 */
	void setBase_Property(Property value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getUsagesList <em>Usages List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ISBlock)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPart_Type()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getUsagesList
	 * @model opposite="usagesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlock getType();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ISBlock value);

	/**
	 * Returns the value of the '<em><b>SPort Roles List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext <em>Usage Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SPort Roles List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SPort Roles List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPart_SPortRolesList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext
	 * @model opposite="usageContext" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPortRole> getSPortRolesList();

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPartsList <em>Owned SParts List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(ISBlock)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPart_Owner()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPartsList
	 * @model opposite="ownedSPartsList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlock getOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(ISBlock value);

} // ISPart
