/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Port;

import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPort</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPort#getRolesList <em>Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPort#getOwner <em>Owner</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPort#getBase_Port <em>Base Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SPort
		extends AbstractSConnectableElement
		implements ISPort {

	/**
	 * The cached value of the '{@link #getBase_Port() <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Port()
	 * @generated
	 * @ordered
	 */
	protected Port base_Port;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SPort() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFArchitectureConceptsPackage.Literals.SPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPortRole> getRolesList() {
		// TODO: implement this method to return the 'Roles List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlock getOwner() {
		ISBlock owner = basicGetOwner();
		return owner != null && owner.eIsProxy() ? (ISBlock) eResolveProxy((InternalEObject) owner) : owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlock basicGetOwner() {
		// TODO: implement this method to return the 'Owner' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setOwner(ISBlock newOwner) {
		// TODO: implement this method to set the 'Owner' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Port getBase_Port() {
		if (base_Port != null && base_Port.eIsProxy()) {
			InternalEObject oldBase_Port = (InternalEObject) base_Port;
			base_Port = (Port) eResolveProxy(oldBase_Port);
			if (base_Port != oldBase_Port) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFArchitectureConceptsPackage.SPORT__BASE_PORT, oldBase_Port, base_Port));
			}
		}
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Port basicGetBase_Port() {
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Port(Port newBase_Port) {
		Port oldBase_Port = base_Port;
		base_Port = newBase_Port;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.SPORT__BASE_PORT, oldBase_Port, base_Port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPORT__ROLES_LIST:
			return getRolesList();
		case IESFArchitectureConceptsPackage.SPORT__OWNER:
			if (resolve)
				return getOwner();
			return basicGetOwner();
		case IESFArchitectureConceptsPackage.SPORT__BASE_PORT:
			if (resolve)
				return getBase_Port();
			return basicGetBase_Port();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPORT__ROLES_LIST:
			getRolesList().clear();
			getRolesList().addAll((Collection<? extends ISPortRole>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SPORT__OWNER:
			setOwner((ISBlock) newValue);
			return;
		case IESFArchitectureConceptsPackage.SPORT__BASE_PORT:
			setBase_Port((Port) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPORT__ROLES_LIST:
			getRolesList().clear();
			return;
		case IESFArchitectureConceptsPackage.SPORT__OWNER:
			setOwner((ISBlock) null);
			return;
		case IESFArchitectureConceptsPackage.SPORT__BASE_PORT:
			setBase_Port((Port) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPORT__ROLES_LIST:
			return !getRolesList().isEmpty();
		case IESFArchitectureConceptsPackage.SPORT__OWNER:
			return basicGetOwner() != null;
		case IESFArchitectureConceptsPackage.SPORT__BASE_PORT:
			return base_Port != null;
		}
		return super.eIsSet(featureID);
	}

} // SPort
