/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.uml2.uml.Connector;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SConnector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getBase_Connector <em>Base Connector</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner <em>Owner</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getSourcesList <em>Sources List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getTargetsList <em>Targets List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getEndsList <em>Ends List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector()
 * @model
 * @generated
 */
public interface ISConnector
		extends IAbstractSArchitectureElement {

	/**
	 * Returns the value of the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Connector</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Connector</em>' reference.
	 * @see #setBase_Connector(Connector)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector_Base_Connector()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Connector getBase_Connector();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getBase_Connector <em>Base Connector</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Connector</em>' reference.
	 * @see #getBase_Connector()
	 * @generated
	 */
	void setBase_Connector(Connector value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSConnectorsList <em>SConnectors List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(ISBlock)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector_Owner()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSConnectorsList
	 * @model opposite="sConnectorsList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlock getOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(ISBlock value);

	/**
	 * Returns the value of the '<em><b>Sources List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Sources List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector_SourcesList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSConnectableElement> getSourcesList();

	/**
	 * Returns the value of the '<em><b>Targets List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Targets List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector_TargetsList()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSConnectableElement> getTargetsList();

	/**
	 * Returns the value of the '<em><b>Ends List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSConnectorsList <em>SConnectors List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ends List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ends List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSConnector_EndsList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSConnectorsList
	 * @model opposite="sConnectorsList" lower="2" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<IAbstractSConnectableElement> getEndsList();

} // ISConnector
