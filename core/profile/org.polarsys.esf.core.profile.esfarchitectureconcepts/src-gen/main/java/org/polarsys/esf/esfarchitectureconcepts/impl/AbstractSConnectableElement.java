/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;

import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SConnectable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement#getSConnectorsList <em>SConnectors List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement#getSDirection <em>SDirection</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement#getSDirectionManual <em>SDirection Manual</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSConnectableElement
		extends AbstractSArchitectureElement
		implements IAbstractSConnectableElement {

	/**
	 * The default value of the '{@link #getSDirection() <em>SDirection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirection()
	 * @generated
	 * @ordered
	 */
	protected static final SDirection SDIRECTION_EDEFAULT = SDirection.IN;

	/**
	 * The default value of the '{@link #getSDirectionManual() <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirectionManual()
	 * @generated
	 * @ordered
	 */
	protected static final SDirection SDIRECTION_MANUAL_EDEFAULT = SDirection.UNDEFINED;

	/**
	 * The cached value of the '{@link #getSDirectionManual() <em>SDirection Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSDirectionManual()
	 * @generated
	 * @ordered
	 */
	protected SDirection sDirectionManual = SDIRECTION_MANUAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AbstractSConnectableElement() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFArchitectureConceptsPackage.Literals.ABSTRACT_SCONNECTABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISConnector> getSConnectorsList() {
		// TODO: implement this method to return the 'SConnectors List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public SDirection getSDirectionManual() {
		return sDirectionManual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSDirectionManual(SDirection newSDirectionManual) {
		SDirection oldSDirectionManual = sDirectionManual;
		sDirectionManual = newSDirectionManual == null ? SDIRECTION_MANUAL_EDEFAULT : newSDirectionManual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL, oldSDirectionManual, sDirectionManual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public SDirection getSDirection() {
		// TODO: implement this method to return the 'SDirection' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST:
			return getSConnectorsList();
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION:
			return getSDirection();
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL:
			return getSDirectionManual();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST:
			getSConnectorsList().clear();
			getSConnectorsList().addAll((Collection<? extends ISConnector>) newValue);
			return;
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL:
			setSDirectionManual((SDirection) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST:
			getSConnectorsList().clear();
			return;
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL:
			setSDirectionManual(SDIRECTION_MANUAL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST:
			return !getSConnectorsList().isEmpty();
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION:
			return getSDirection() != SDIRECTION_EDEFAULT;
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL:
			return sDirectionManual != SDIRECTION_MANUAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sDirectionManual: "); //$NON-NLS-1$
		result.append(sDirectionManual);
		result.append(')');
		return result.toString();
	}

} // AbstractSConnectableElement
