/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Property;

import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;

import org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPart</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart#getBase_Property <em>Base Property</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart#getType <em>Type</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart#getSPortRolesList <em>SPort Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart#getOwner <em>Owner</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SPart
		extends AbstractSArchitectureElement
		implements ISPart {

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SPart() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFArchitectureConceptsPackage.Literals.SPART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject) base_Property;
			base_Property = (Property) eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlock getType() {
		ISBlock type = basicGetType();
		return type != null && type.eIsProxy() ? (ISBlock) eResolveProxy((InternalEObject) type) : type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlock basicGetType() {
		// TODO: implement this method to return the 'Type' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setType(ISBlock newType) {
		// TODO: implement this method to set the 'Type' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPortRole> getSPortRolesList() {
		// TODO: implement this method to return the 'SPort Roles List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlock getOwner() {
		ISBlock owner = basicGetOwner();
		return owner != null && owner.eIsProxy() ? (ISBlock) eResolveProxy((InternalEObject) owner) : owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlock basicGetOwner() {
		// TODO: implement this method to return the 'Owner' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setOwner(ISBlock newOwner) {
		// TODO: implement this method to set the 'Owner' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY:
			if (resolve)
				return getBase_Property();
			return basicGetBase_Property();
		case IESFArchitectureConceptsPackage.SPART__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case IESFArchitectureConceptsPackage.SPART__SPORT_ROLES_LIST:
			return getSPortRolesList();
		case IESFArchitectureConceptsPackage.SPART__OWNER:
			if (resolve)
				return getOwner();
			return basicGetOwner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY:
			setBase_Property((Property) newValue);
			return;
		case IESFArchitectureConceptsPackage.SPART__TYPE:
			setType((ISBlock) newValue);
			return;
		case IESFArchitectureConceptsPackage.SPART__SPORT_ROLES_LIST:
			getSPortRolesList().clear();
			getSPortRolesList().addAll((Collection<? extends ISPortRole>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SPART__OWNER:
			setOwner((ISBlock) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY:
			setBase_Property((Property) null);
			return;
		case IESFArchitectureConceptsPackage.SPART__TYPE:
			setType((ISBlock) null);
			return;
		case IESFArchitectureConceptsPackage.SPART__SPORT_ROLES_LIST:
			getSPortRolesList().clear();
			return;
		case IESFArchitectureConceptsPackage.SPART__OWNER:
			setOwner((ISBlock) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SPART__BASE_PROPERTY:
			return base_Property != null;
		case IESFArchitectureConceptsPackage.SPART__TYPE:
			return basicGetType() != null;
		case IESFArchitectureConceptsPackage.SPART__SPORT_ROLES_LIST:
			return !getSPortRolesList().isEmpty();
		case IESFArchitectureConceptsPackage.SPART__OWNER:
			return basicGetOwner() != null;
		}
		return super.eIsSet(featureID);
	}

} // SPart
