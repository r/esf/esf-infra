/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;

import org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SBlock</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#isTopBlock <em>Top Block</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getOwnedSPartsList <em>Owned SParts List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getUsagesList <em>Usages List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getOwnedSPortRolesList <em>Owned SPort Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getOwnedSPortsList <em>Owned SPorts List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getSModel <em>SModel</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock#getSConnectorsList <em>SConnectors List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SBlock
		extends AbstractSArchitectureElement
		implements ISBlock {

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * The default value of the '{@link #isTopBlock() <em>Top Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #isTopBlock()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TOP_BLOCK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTopBlock() <em>Top Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #isTopBlock()
	 * @generated
	 * @ordered
	 */
	protected boolean topBlock = TOP_BLOCK_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SBlock() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFArchitectureConceptsPackage.Literals.SBLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject) base_Class;
			base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean isTopBlock() {
		return topBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setTopBlock(boolean newTopBlock) {
		boolean oldTopBlock = topBlock;
		topBlock = newTopBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.SBLOCK__TOP_BLOCK, oldTopBlock, topBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPart> getOwnedSPartsList() {
		// TODO: implement this method to return the 'Owned SParts List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPart> getUsagesList() {
		// TODO: implement this method to return the 'Usages List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPortRole> getOwnedSPortRolesList() {
		// TODO: implement this method to return the 'Owned SPort Roles List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISPort> getOwnedSPortsList() {
		// TODO: implement this method to return the 'Owned SPorts List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISModel getSModel() {
		ISModel sModel = basicGetSModel();
		return sModel != null && sModel.eIsProxy() ? (ISModel) eResolveProxy((InternalEObject) sModel) : sModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISModel basicGetSModel() {
		// TODO: implement this method to return the 'SModel' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setSModel(ISModel newSModel) {
		// TODO: implement this method to set the 'SModel' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<ISConnector> getSConnectorsList() {
		// TODO: implement this method to return the 'SConnectors List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS:
			if (resolve)
				return getBase_Class();
			return basicGetBase_Class();
		case IESFArchitectureConceptsPackage.SBLOCK__TOP_BLOCK:
			return isTopBlock();
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPARTS_LIST:
			return getOwnedSPartsList();
		case IESFArchitectureConceptsPackage.SBLOCK__USAGES_LIST:
			return getUsagesList();
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORT_ROLES_LIST:
			return getOwnedSPortRolesList();
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORTS_LIST:
			return getOwnedSPortsList();
		case IESFArchitectureConceptsPackage.SBLOCK__SMODEL:
			if (resolve)
				return getSModel();
			return basicGetSModel();
		case IESFArchitectureConceptsPackage.SBLOCK__SCONNECTORS_LIST:
			return getSConnectorsList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__TOP_BLOCK:
			setTopBlock((Boolean) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPARTS_LIST:
			getOwnedSPartsList().clear();
			getOwnedSPartsList().addAll((Collection<? extends ISPart>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__USAGES_LIST:
			getUsagesList().clear();
			getUsagesList().addAll((Collection<? extends ISPart>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORT_ROLES_LIST:
			getOwnedSPortRolesList().clear();
			getOwnedSPortRolesList().addAll((Collection<? extends ISPortRole>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORTS_LIST:
			getOwnedSPortsList().clear();
			getOwnedSPortsList().addAll((Collection<? extends ISPort>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__SMODEL:
			setSModel((ISModel) newValue);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__SCONNECTORS_LIST:
			getSConnectorsList().clear();
			getSConnectorsList().addAll((Collection<? extends ISConnector>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS:
			setBase_Class((org.eclipse.uml2.uml.Class) null);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__TOP_BLOCK:
			setTopBlock(TOP_BLOCK_EDEFAULT);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPARTS_LIST:
			getOwnedSPartsList().clear();
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__USAGES_LIST:
			getUsagesList().clear();
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORT_ROLES_LIST:
			getOwnedSPortRolesList().clear();
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORTS_LIST:
			getOwnedSPortsList().clear();
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__SMODEL:
			setSModel((ISModel) null);
			return;
		case IESFArchitectureConceptsPackage.SBLOCK__SCONNECTORS_LIST:
			getSConnectorsList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SBLOCK__BASE_CLASS:
			return base_Class != null;
		case IESFArchitectureConceptsPackage.SBLOCK__TOP_BLOCK:
			return topBlock != TOP_BLOCK_EDEFAULT;
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPARTS_LIST:
			return !getOwnedSPartsList().isEmpty();
		case IESFArchitectureConceptsPackage.SBLOCK__USAGES_LIST:
			return !getUsagesList().isEmpty();
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORT_ROLES_LIST:
			return !getOwnedSPortRolesList().isEmpty();
		case IESFArchitectureConceptsPackage.SBLOCK__OWNED_SPORTS_LIST:
			return !getOwnedSPortsList().isEmpty();
		case IESFArchitectureConceptsPackage.SBLOCK__SMODEL:
			return basicGetSModel() != null;
		case IESFArchitectureConceptsPackage.SBLOCK__SCONNECTORS_LIST:
			return !getSConnectorsList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (topBlock: "); //$NON-NLS-1$
		result.append(topBlock);
		result.append(')');
		return result.toString();
	}

} // SBlock
