/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFArchitectureConcepts'"
 * @generated
 */
public interface IESFArchitectureConceptsPackage
		extends EPackage {

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNAME = "esfarchitectureconcepts"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFArchitectureConcepts"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String eNS_PREFIX = "ESFArchitectureConcepts"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	IESFArchitectureConceptsPackage eINSTANCE = org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage.init();

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement <em>Abstract SConnectable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getAbstractSConnectableElement()
	 * @generated
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__UUID = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__NAME = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__DESCRIPTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>SConnectors List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SDirection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SDirection Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract SConnectable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract SConnectable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SCONNECTABLE_ELEMENT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPort <em>SPort</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPort
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPort()
	 * @generated
	 */
	int SPORT = 0;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__UUID = ABSTRACT_SCONNECTABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__NAME = ABSTRACT_SCONNECTABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__DESCRIPTION = ABSTRACT_SCONNECTABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__SSAFETY_CONCEPTS_LIST = ABSTRACT_SCONNECTABLE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>SConnectors List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__SCONNECTORS_LIST = ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST;

	/**
	 * The feature id for the '<em><b>SDirection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__SDIRECTION = ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION;

	/**
	 * The feature id for the '<em><b>SDirection Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__SDIRECTION_MANUAL = ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL;

	/**
	 * The feature id for the '<em><b>Roles List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__ROLES_LIST = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__OWNER = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT__BASE_PORT = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SPort</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_FEATURE_COUNT = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SPort</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_OPERATION_COUNT = ABSTRACT_SCONNECTABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector <em>SConnector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SConnector
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSConnector()
	 * @generated
	 */
	int SCONNECTOR = 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__UUID = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__NAME = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__DESCRIPTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__BASE_CONNECTOR = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__OWNER = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sources List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__SOURCES_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Targets List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__TARGETS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ends List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR__ENDS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>SConnector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>SConnector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SCONNECTOR_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock <em>SBlock</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SBlock
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSBlock()
	 * @generated
	 */
	int SBLOCK = 3;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__UUID = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__NAME = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__DESCRIPTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__BASE_CLASS = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Top Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__TOP_BLOCK = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned SParts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__OWNED_SPARTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Usages List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__USAGES_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Owned SPort Roles List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__OWNED_SPORT_ROLES_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Owned SPorts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__OWNED_SPORTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>SModel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__SMODEL = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>SConnectors List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK__SCONNECTORS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>SBlock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>SBlock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SBLOCK_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart <em>SPart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPart
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPart()
	 * @generated
	 */
	int SPART = 4;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__UUID = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__NAME = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__DESCRIPTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__BASE_PROPERTY = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__TYPE = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>SPort Roles List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__SPORT_ROLES_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART__OWNER = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SPart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SPart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPART_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPortRole <em>SPort Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPortRole
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPortRole()
	 * @generated
	 */
	int SPORT_ROLE = 5;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__UUID = ABSTRACT_SCONNECTABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__NAME = ABSTRACT_SCONNECTABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__DESCRIPTION = ABSTRACT_SCONNECTABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__SSAFETY_CONCEPTS_LIST = ABSTRACT_SCONNECTABLE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>SConnectors List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__SCONNECTORS_LIST = ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST;

	/**
	 * The feature id for the '<em><b>SDirection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__SDIRECTION = ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION;

	/**
	 * The feature id for the '<em><b>SDirection Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__SDIRECTION_MANUAL = ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL;

	/**
	 * The feature id for the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__BASE_PORT = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__TYPE = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__OWNER = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Usage Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE__USAGE_CONTEXT = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SPort Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE_FEATURE_COUNT = ABSTRACT_SCONNECTABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SPort Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SPORT_ROLE_OPERATION_COUNT = ABSTRACT_SCONNECTABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SModel <em>SModel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SModel
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSModel()
	 * @generated
	 */
	int SMODEL = 6;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__UUID = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__NAME = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__DESCRIPTION = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__SSAFETY_CONCEPTS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__BASE_PACKAGE = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owned SBlocks List</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL__OWNED_SBLOCKS_LIST = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SModel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SModel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int SMODEL_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.esf.esfarchitectureconcepts.SDirection <em>SDirection</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSDirection()
	 * @generated
	 */
	int SDIRECTION = 7;

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort <em>SPort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SPort</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort
	 * @generated
	 */
	EClass getSPort();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getRolesList <em>Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Roles List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort#getRolesList()
	 * @see #getSPort()
	 * @generated
	 */
	EReference getSPort_RolesList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner()
	 * @see #getSPort()
	 * @generated
	 */
	EReference getSPort_Owner();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getBase_Port <em>Base Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Port</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort#getBase_Port()
	 * @see #getSPort()
	 * @generated
	 */
	EReference getSPort_Base_Port();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement <em>Abstract SConnectable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Abstract SConnectable Element</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement
	 * @generated
	 */
	EClass getAbstractSConnectableElement();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSConnectorsList <em>SConnectors List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SConnectors List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSConnectorsList()
	 * @see #getAbstractSConnectableElement()
	 * @generated
	 */
	EReference getAbstractSConnectableElement_SConnectorsList();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirectionManual <em>SDirection Manual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>SDirection Manual</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirectionManual()
	 * @see #getAbstractSConnectableElement()
	 * @generated
	 */
	EAttribute getAbstractSConnectableElement_SDirectionManual();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirection <em>SDirection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>SDirection</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement#getSDirection()
	 * @see #getAbstractSConnectableElement()
	 * @generated
	 */
	EAttribute getAbstractSConnectableElement_SDirection();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector <em>SConnector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SConnector</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector
	 * @generated
	 */
	EClass getSConnector();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getBase_Connector <em>Base Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Connector</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getBase_Connector()
	 * @see #getSConnector()
	 * @generated
	 */
	EReference getSConnector_Base_Connector();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner()
	 * @see #getSConnector()
	 * @generated
	 */
	EReference getSConnector_Owner();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getSourcesList <em>Sources List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Sources List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getSourcesList()
	 * @see #getSConnector()
	 * @generated
	 */
	EReference getSConnector_SourcesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getTargetsList <em>Targets List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Targets List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getTargetsList()
	 * @see #getSConnector()
	 * @generated
	 */
	EReference getSConnector_TargetsList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getEndsList <em>Ends List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Ends List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getEndsList()
	 * @see #getSConnector()
	 * @generated
	 */
	EReference getSConnector_EndsList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock <em>SBlock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SBlock</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock
	 * @generated
	 */
	EClass getSBlock();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getBase_Class()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_Base_Class();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#isTopBlock <em>Top Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Top Block</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#isTopBlock()
	 * @see #getSBlock()
	 * @generated
	 */
	EAttribute getSBlock_TopBlock();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPartsList <em>Owned SParts List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Owned SParts List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPartsList()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_OwnedSPartsList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getUsagesList <em>Usages List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Usages List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getUsagesList()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_UsagesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortRolesList <em>Owned SPort Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Owned SPort Roles List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortRolesList()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_OwnedSPortRolesList();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortsList <em>Owned SPorts List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Owned SPorts List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortsList()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_OwnedSPortsList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel <em>SModel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>SModel</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_SModel();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSConnectorsList <em>SConnectors List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SConnectors List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSConnectorsList()
	 * @see #getSBlock()
	 * @generated
	 */
	EReference getSBlock_SConnectorsList();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart <em>SPart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SPart</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart
	 * @generated
	 */
	EClass getSPart();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getBase_Property()
	 * @see #getSPart()
	 * @generated
	 */
	EReference getSPart_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getType()
	 * @see #getSPart()
	 * @generated
	 */
	EReference getSPart_Type();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getSPortRolesList <em>SPort Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>SPort Roles List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getSPortRolesList()
	 * @see #getSPart()
	 * @generated
	 */
	EReference getSPart_SPortRolesList();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner()
	 * @see #getSPart()
	 * @generated
	 */
	EReference getSPart_Owner();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole <em>SPort Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SPort Role</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole
	 * @generated
	 */
	EClass getSPortRole();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getBase_Port <em>Base Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Port</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getBase_Port()
	 * @see #getSPortRole()
	 * @generated
	 */
	EReference getSPortRole_Base_Port();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType()
	 * @see #getSPortRole()
	 * @generated
	 */
	EReference getSPortRole_Type();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner()
	 * @see #getSPortRole()
	 * @generated
	 */
	EReference getSPortRole_Owner();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext <em>Usage Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Usage Context</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext()
	 * @see #getSPortRole()
	 * @generated
	 */
	EReference getSPortRole_UsageContext();

	/**
	 * Returns the meta object for class '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel <em>SModel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SModel</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISModel
	 * @generated
	 */
	EClass getSModel();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISModel#getBase_Package()
	 * @see #getSModel()
	 * @generated
	 */
	EReference getSModel_Base_Package();

	/**
	 * Returns the meta object for the reference list '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getOwnedSBlocksList <em>Owned SBlocks List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Owned SBlocks List</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISModel#getOwnedSBlocksList()
	 * @see #getSModel()
	 * @generated
	 */
	EReference getSModel_OwnedSBlocksList();

	/**
	 * Returns the meta object for enum '{@link org.polarsys.esf.esfarchitectureconcepts.SDirection <em>SDirection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>SDirection</em>'.
	 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
	 * @generated
	 */
	EEnum getSDirection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IESFArchitectureConceptsFactory getESFArchitectureConceptsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals {

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPort <em>SPort</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPort
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPort()
		 * @generated
		 */
		EClass SPORT = eINSTANCE.getSPort();

		/**
		 * The meta object literal for the '<em><b>Roles List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT__ROLES_LIST = eINSTANCE.getSPort_RolesList();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT__OWNER = eINSTANCE.getSPort_Owner();

		/**
		 * The meta object literal for the '<em><b>Base Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT__BASE_PORT = eINSTANCE.getSPort_Base_Port();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement <em>Abstract SConnectable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.AbstractSConnectableElement
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getAbstractSConnectableElement()
		 * @generated
		 */
		EClass ABSTRACT_SCONNECTABLE_ELEMENT = eINSTANCE.getAbstractSConnectableElement();

		/**
		 * The meta object literal for the '<em><b>SConnectors List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST = eINSTANCE.getAbstractSConnectableElement_SConnectorsList();

		/**
		 * The meta object literal for the '<em><b>SDirection Manual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL = eINSTANCE.getAbstractSConnectableElement_SDirectionManual();

		/**
		 * The meta object literal for the '<em><b>SDirection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION = eINSTANCE.getAbstractSConnectableElement_SDirection();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector <em>SConnector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SConnector
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSConnector()
		 * @generated
		 */
		EClass SCONNECTOR = eINSTANCE.getSConnector();

		/**
		 * The meta object literal for the '<em><b>Base Connector</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SCONNECTOR__BASE_CONNECTOR = eINSTANCE.getSConnector_Base_Connector();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SCONNECTOR__OWNER = eINSTANCE.getSConnector_Owner();

		/**
		 * The meta object literal for the '<em><b>Sources List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SCONNECTOR__SOURCES_LIST = eINSTANCE.getSConnector_SourcesList();

		/**
		 * The meta object literal for the '<em><b>Targets List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SCONNECTOR__TARGETS_LIST = eINSTANCE.getSConnector_TargetsList();

		/**
		 * The meta object literal for the '<em><b>Ends List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SCONNECTOR__ENDS_LIST = eINSTANCE.getSConnector_EndsList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SBlock <em>SBlock</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SBlock
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSBlock()
		 * @generated
		 */
		EClass SBLOCK = eINSTANCE.getSBlock();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__BASE_CLASS = eINSTANCE.getSBlock_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Top Block</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute SBLOCK__TOP_BLOCK = eINSTANCE.getSBlock_TopBlock();

		/**
		 * The meta object literal for the '<em><b>Owned SParts List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__OWNED_SPARTS_LIST = eINSTANCE.getSBlock_OwnedSPartsList();

		/**
		 * The meta object literal for the '<em><b>Usages List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__USAGES_LIST = eINSTANCE.getSBlock_UsagesList();

		/**
		 * The meta object literal for the '<em><b>Owned SPort Roles List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__OWNED_SPORT_ROLES_LIST = eINSTANCE.getSBlock_OwnedSPortRolesList();

		/**
		 * The meta object literal for the '<em><b>Owned SPorts List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__OWNED_SPORTS_LIST = eINSTANCE.getSBlock_OwnedSPortsList();

		/**
		 * The meta object literal for the '<em><b>SModel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__SMODEL = eINSTANCE.getSBlock_SModel();

		/**
		 * The meta object literal for the '<em><b>SConnectors List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SBLOCK__SCONNECTORS_LIST = eINSTANCE.getSBlock_SConnectorsList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPart <em>SPart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPart
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPart()
		 * @generated
		 */
		EClass SPART = eINSTANCE.getSPart();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPART__BASE_PROPERTY = eINSTANCE.getSPart_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPART__TYPE = eINSTANCE.getSPart_Type();

		/**
		 * The meta object literal for the '<em><b>SPort Roles List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPART__SPORT_ROLES_LIST = eINSTANCE.getSPart_SPortRolesList();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPART__OWNER = eINSTANCE.getSPart_Owner();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SPortRole <em>SPort Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SPortRole
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSPortRole()
		 * @generated
		 */
		EClass SPORT_ROLE = eINSTANCE.getSPortRole();

		/**
		 * The meta object literal for the '<em><b>Base Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_ROLE__BASE_PORT = eINSTANCE.getSPortRole_Base_Port();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_ROLE__TYPE = eINSTANCE.getSPortRole_Type();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_ROLE__OWNER = eINSTANCE.getSPortRole_Owner();

		/**
		 * The meta object literal for the '<em><b>Usage Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SPORT_ROLE__USAGE_CONTEXT = eINSTANCE.getSPortRole_UsageContext();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.impl.SModel <em>SModel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.SModel
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSModel()
		 * @generated
		 */
		EClass SMODEL = eINSTANCE.getSModel();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SMODEL__BASE_PACKAGE = eINSTANCE.getSModel_Base_Package();

		/**
		 * The meta object literal for the '<em><b>Owned SBlocks List</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference SMODEL__OWNED_SBLOCKS_LIST = eINSTANCE.getSModel_OwnedSBlocksList();

		/**
		 * The meta object literal for the '{@link org.polarsys.esf.esfarchitectureconcepts.SDirection <em>SDirection</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see org.polarsys.esf.esfarchitectureconcepts.SDirection
		 * @see org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage#getSDirection()
		 * @generated
		 */
		EEnum SDIRECTION = eINSTANCE.getSDirection();

	}

} // IESFArchitectureConceptsPackage
