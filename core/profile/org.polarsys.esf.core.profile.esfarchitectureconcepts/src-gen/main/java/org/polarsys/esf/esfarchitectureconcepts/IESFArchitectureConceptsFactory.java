/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage
 * @generated
 */
public interface IESFArchitectureConceptsFactory
		extends EFactory {

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	IESFArchitectureConceptsFactory eINSTANCE = org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsFactory.init();

	/**
	 * Returns a new object of class '<em>SPort</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SPort</em>'.
	 * @generated
	 */
	ISPort createSPort();

	/**
	 * Returns a new object of class '<em>SConnector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SConnector</em>'.
	 * @generated
	 */
	ISConnector createSConnector();

	/**
	 * Returns a new object of class '<em>SBlock</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SBlock</em>'.
	 * @generated
	 */
	ISBlock createSBlock();

	/**
	 * Returns a new object of class '<em>SPart</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SPart</em>'.
	 * @generated
	 */
	ISPart createSPart();

	/**
	 * Returns a new object of class '<em>SPort Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SPort Role</em>'.
	 * @generated
	 */
	ISPortRole createSPortRole();

	/**
	 * Returns a new object of class '<em>SModel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SModel</em>'.
	 * @generated
	 */
	ISModel createSModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	IESFArchitectureConceptsPackage getESFArchitectureConceptsPackage();

} // IESFArchitectureConceptsFactory
