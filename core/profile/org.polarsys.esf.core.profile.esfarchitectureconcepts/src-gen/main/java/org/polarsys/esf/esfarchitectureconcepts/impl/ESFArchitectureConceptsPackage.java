/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsFactory;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;

import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFArchitectureConceptsPackage
		extends EPackageImpl
		implements IESFArchitectureConceptsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass abstractSConnectableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sConnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sPartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sPortRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass sModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum sDirectionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ESFArchitectureConceptsPackage() {
		super(eNS_URI, IESFArchitectureConceptsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link IESFArchitectureConceptsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IESFArchitectureConceptsPackage init() {
		if (isInited)
			return (IESFArchitectureConceptsPackage) EPackage.Registry.INSTANCE.getEPackage(IESFArchitectureConceptsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredESFArchitectureConceptsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ESFArchitectureConceptsPackage theESFArchitectureConceptsPackage = registeredESFArchitectureConceptsPackage instanceof ESFArchitectureConceptsPackage ? (ESFArchitectureConceptsPackage) registeredESFArchitectureConceptsPackage
				: new ESFArchitectureConceptsPackage();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		IESFCorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theESFArchitectureConceptsPackage.createPackageContents();

		// Initialize created meta-data
		theESFArchitectureConceptsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theESFArchitectureConceptsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IESFArchitectureConceptsPackage.eNS_URI, theESFArchitectureConceptsPackage);
		return theESFArchitectureConceptsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSPort() {
		return sPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPort_RolesList() {
		return (EReference) sPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPort_Owner() {
		return (EReference) sPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPort_Base_Port() {
		return (EReference) sPortEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getAbstractSConnectableElement() {
		return abstractSConnectableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getAbstractSConnectableElement_SConnectorsList() {
		return (EReference) abstractSConnectableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getAbstractSConnectableElement_SDirectionManual() {
		return (EAttribute) abstractSConnectableElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getAbstractSConnectableElement_SDirection() {
		return (EAttribute) abstractSConnectableElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSConnector() {
		return sConnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSConnector_Base_Connector() {
		return (EReference) sConnectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSConnector_Owner() {
		return (EReference) sConnectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSConnector_SourcesList() {
		return (EReference) sConnectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSConnector_TargetsList() {
		return (EReference) sConnectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSConnector_EndsList() {
		return (EReference) sConnectorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSBlock() {
		return sBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_Base_Class() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EAttribute getSBlock_TopBlock() {
		return (EAttribute) sBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_OwnedSPartsList() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_UsagesList() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_OwnedSPortRolesList() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_OwnedSPortsList() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_SModel() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSBlock_SConnectorsList() {
		return (EReference) sBlockEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSPart() {
		return sPartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPart_Base_Property() {
		return (EReference) sPartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPart_Type() {
		return (EReference) sPartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPart_SPortRolesList() {
		return (EReference) sPartEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPart_Owner() {
		return (EReference) sPartEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSPortRole() {
		return sPortRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortRole_Base_Port() {
		return (EReference) sPortRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortRole_Type() {
		return (EReference) sPortRoleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortRole_Owner() {
		return (EReference) sPortRoleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSPortRole_UsageContext() {
		return (EReference) sPortRoleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EClass getSModel() {
		return sModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSModel_Base_Package() {
		return (EReference) sModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EReference getSModel_OwnedSBlocksList() {
		return (EReference) sModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EEnum getSDirection() {
		return sDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IESFArchitectureConceptsFactory getESFArchitectureConceptsFactory() {
		return (IESFArchitectureConceptsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		sPortEClass = createEClass(SPORT);
		createEReference(sPortEClass, SPORT__ROLES_LIST);
		createEReference(sPortEClass, SPORT__OWNER);
		createEReference(sPortEClass, SPORT__BASE_PORT);

		abstractSConnectableElementEClass = createEClass(ABSTRACT_SCONNECTABLE_ELEMENT);
		createEReference(abstractSConnectableElementEClass, ABSTRACT_SCONNECTABLE_ELEMENT__SCONNECTORS_LIST);
		createEAttribute(abstractSConnectableElementEClass, ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION);
		createEAttribute(abstractSConnectableElementEClass, ABSTRACT_SCONNECTABLE_ELEMENT__SDIRECTION_MANUAL);

		sConnectorEClass = createEClass(SCONNECTOR);
		createEReference(sConnectorEClass, SCONNECTOR__BASE_CONNECTOR);
		createEReference(sConnectorEClass, SCONNECTOR__OWNER);
		createEReference(sConnectorEClass, SCONNECTOR__SOURCES_LIST);
		createEReference(sConnectorEClass, SCONNECTOR__TARGETS_LIST);
		createEReference(sConnectorEClass, SCONNECTOR__ENDS_LIST);

		sBlockEClass = createEClass(SBLOCK);
		createEReference(sBlockEClass, SBLOCK__BASE_CLASS);
		createEAttribute(sBlockEClass, SBLOCK__TOP_BLOCK);
		createEReference(sBlockEClass, SBLOCK__OWNED_SPARTS_LIST);
		createEReference(sBlockEClass, SBLOCK__USAGES_LIST);
		createEReference(sBlockEClass, SBLOCK__OWNED_SPORT_ROLES_LIST);
		createEReference(sBlockEClass, SBLOCK__OWNED_SPORTS_LIST);
		createEReference(sBlockEClass, SBLOCK__SMODEL);
		createEReference(sBlockEClass, SBLOCK__SCONNECTORS_LIST);

		sPartEClass = createEClass(SPART);
		createEReference(sPartEClass, SPART__BASE_PROPERTY);
		createEReference(sPartEClass, SPART__TYPE);
		createEReference(sPartEClass, SPART__SPORT_ROLES_LIST);
		createEReference(sPartEClass, SPART__OWNER);

		sPortRoleEClass = createEClass(SPORT_ROLE);
		createEReference(sPortRoleEClass, SPORT_ROLE__BASE_PORT);
		createEReference(sPortRoleEClass, SPORT_ROLE__TYPE);
		createEReference(sPortRoleEClass, SPORT_ROLE__OWNER);
		createEReference(sPortRoleEClass, SPORT_ROLE__USAGE_CONTEXT);

		sModelEClass = createEClass(SMODEL);
		createEReference(sModelEClass, SMODEL__BASE_PACKAGE);
		createEReference(sModelEClass, SMODEL__OWNED_SBLOCKS_LIST);

		// Create enums
		sDirectionEEnum = createEEnum(SDIRECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		IESFCorePackage theESFCorePackage = (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sPortEClass.getESuperTypes().add(this.getAbstractSConnectableElement());
		abstractSConnectableElementEClass.getESuperTypes().add(theESFCorePackage.getAbstractSArchitectureElement());
		sConnectorEClass.getESuperTypes().add(theESFCorePackage.getAbstractSArchitectureElement());
		sBlockEClass.getESuperTypes().add(theESFCorePackage.getAbstractSArchitectureElement());
		sPartEClass.getESuperTypes().add(theESFCorePackage.getAbstractSArchitectureElement());
		sPortRoleEClass.getESuperTypes().add(this.getAbstractSConnectableElement());
		sModelEClass.getESuperTypes().add(theESFCorePackage.getAbstractSArchitectureElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(sPortEClass, ISPort.class, "SPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSPort_RolesList(), this.getSPortRole(), this.getSPortRole_Type(), "rolesList", null, 0, -1, ISPort.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSPort_Owner(), this.getSBlock(), this.getSBlock_OwnedSPortsList(), "owner", null, 1, 1, ISPort.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSPort_Base_Port(), theUMLPackage.getPort(), null, "base_Port", null, 1, 1, ISPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(abstractSConnectableElementEClass, IAbstractSConnectableElement.class, "AbstractSConnectableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAbstractSConnectableElement_SConnectorsList(), this.getSConnector(), this.getSConnector_EndsList(), "sConnectorsList", null, 0, -1, IAbstractSConnectableElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAbstractSConnectableElement_SDirection(), this.getSDirection(), "sDirection", null, 1, 1, IAbstractSConnectableElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getAbstractSConnectableElement_SDirectionManual(), this.getSDirection(), "sDirectionManual", "UNDEFINED", 0, 1, IAbstractSConnectableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, //$NON-NLS-1$ //$NON-NLS-2$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(sConnectorEClass, ISConnector.class, "SConnector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSConnector_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 1, 1, ISConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSConnector_Owner(), this.getSBlock(), this.getSBlock_SConnectorsList(), "owner", null, 1, 1, ISConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSConnector_SourcesList(), this.getAbstractSConnectableElement(), null, "sourcesList", null, 0, -1, ISConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSConnector_TargetsList(), this.getAbstractSConnectableElement(), null, "targetsList", null, 0, -1, ISConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSConnector_EndsList(), this.getAbstractSConnectableElement(), this.getAbstractSConnectableElement_SConnectorsList(), "endsList", null, 2, -1, ISConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(sBlockEClass, ISBlock.class, "SBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSBlock_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ISBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSBlock_TopBlock(), theTypesPackage.getBoolean(), "topBlock", "false", 1, 1, ISBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getSBlock_OwnedSPartsList(), this.getSPart(), this.getSPart_Owner(), "ownedSPartsList", null, 0, -1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSBlock_UsagesList(), this.getSPart(), this.getSPart_Type(), "usagesList", null, 0, -1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSBlock_OwnedSPortRolesList(), this.getSPortRole(), this.getSPortRole_Owner(), "ownedSPortRolesList", null, 0, -1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				IS_DERIVED, !IS_ORDERED);
		initEReference(getSBlock_OwnedSPortsList(), this.getSPort(), this.getSPort_Owner(), "ownedSPortsList", null, 0, -1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSBlock_SModel(), this.getSModel(), this.getSModel_OwnedSBlocksList(), "sModel", null, 1, 1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSBlock_SConnectorsList(), this.getSConnector(), this.getSConnector_Owner(), "sConnectorsList", null, 0, -1, ISBlock.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				IS_DERIVED, !IS_ORDERED);

		initEClass(sPartEClass, ISPart.class, "SPart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSPart_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ISPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSPart_Type(), this.getSBlock(), this.getSBlock_UsagesList(), "type", null, 1, 1, ISPart.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSPart_SPortRolesList(), this.getSPortRole(), this.getSPortRole_UsageContext(), "sPortRolesList", null, 0, -1, ISPart.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				IS_DERIVED, !IS_ORDERED);
		initEReference(getSPart_Owner(), this.getSBlock(), this.getSBlock_OwnedSPartsList(), "owner", null, 1, 1, ISPart.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(sPortRoleEClass, ISPortRole.class, "SPortRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSPortRole_Base_Port(), theUMLPackage.getPort(), null, "base_Port", null, 1, 1, ISPortRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSPortRole_Type(), this.getSPort(), this.getSPort_RolesList(), "type", null, 1, 1, ISPortRole.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSPortRole_Owner(), this.getSBlock(), this.getSBlock_OwnedSPortRolesList(), "owner", null, 1, 1, ISPortRole.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSPortRole_UsageContext(), this.getSPart(), this.getSPart_SPortRolesList(), "usageContext", null, 1, 1, ISPortRole.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, //$NON-NLS-1$
				IS_DERIVED, !IS_ORDERED);

		initEClass(sModelEClass, ISModel.class, "SModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSModel_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ISModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSModel_OwnedSBlocksList(), this.getSBlock(), this.getSBlock_SModel(), "ownedSBlocksList", null, 0, -1, ISModel.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(sDirectionEEnum, SDirection.class, "SDirection"); //$NON-NLS-1$
		addEEnumLiteral(sDirectionEEnum, SDirection.IN);
		addEEnumLiteral(sDirectionEEnum, SDirection.OUT);
		addEEnumLiteral(sDirectionEEnum, SDirection.INOUT);
		addEEnumLiteral(sDirectionEEnum, SDirection.UNDEFINED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
		addAnnotation(this,
				source,
				new String[] {
						"originalName", "ESFArchitectureConcepts" //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

} // ESFArchitectureConceptsPackage
