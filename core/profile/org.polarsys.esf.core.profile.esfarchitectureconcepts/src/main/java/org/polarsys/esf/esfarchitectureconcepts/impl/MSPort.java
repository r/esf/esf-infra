/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.PortDirection;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsFactory;
import org.polarsys.esf.esfarchitectureconcepts.IMSPort;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;

/**
 * This class can override the generated class {@link SPort}
 * and will be used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSPort
    extends SPort
    implements IMSPort {

    /**
     * Default constructor.
     */
    public MSPort() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return GenericAbstractSElement.getName(getBase_Port());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUUID() {
        return GenericAbstractSElement.getUUID(getBase_Port());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISConnector> getSConnectorsList() {
        EList<ISConnector> vSConnectorsList = new BasicEList<ISConnector>();
        Port vBase = getBase_Port();

        if (vBase != null) {
            for (ConnectorEnd vEnd : vBase.getEnds()) {
                if (vEnd.getOwner() instanceof Connector) {
                    Connector vConnector = (Connector)vEnd.getOwner();
                    if (UMLUtil.getStereotypeApplication(vConnector, SConnector.class) != null) {
                        ISConnector vSConnector =
                            (SConnector) UMLUtil.getStereotypeApplication(vConnector, SConnector.class);
                        vSConnectorsList.add(vSConnector);
                    }
                }
            }
        }

        UnmodifiableEList<ISConnector> vUSConnectorsList = new UnmodifiableEList<ISConnector>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getAbstractSConnectableElement_SConnectorsList(),
            vSConnectorsList.size(),
            vSConnectorsList.toArray());
        return vUSConnectorsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ISBlock basicGetOwner() {
        Port vBase = getBase_Port();
        ISBlock vSBlock = IESFArchitectureConceptsFactory.eINSTANCE.createSBlock();

        if (vBase != null) {
            Element vOwner = vBase.getOwner();
            if ((vOwner != null) && (UMLUtil.getStereotypeApplication(vOwner, SBlock.class) != null)) {
                vSBlock = (SBlock) UMLUtil.getStereotypeApplication(vOwner, SBlock.class);
            }
        }
        return vSBlock;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPortRole> getRolesList() {
        EList<ISPortRole> vSPortRolesList = new BasicEList<ISPortRole>();

        // TODO: Retrieve all SPortRoles of SPort

        UnmodifiableEList<ISPortRole> vUSPortRolesList =
            new UnmodifiableEList<ISPortRole>(
                this,
                ESFArchitectureConceptsPackage.eINSTANCE.getSPort_RolesList(),
                vSPortRolesList.size(),
                vSPortRolesList.toArray());
        return vUSPortRolesList;
    }

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public SDirection getSDirection() {
		Port port = getBase_Port();
		SDirection direction = getSDirectionManual();
		if (direction == SDirection.UNDEFINED && port != null) {
			direction = PortDirection.getSDirection(port);
		}
		return direction;
	}
}
