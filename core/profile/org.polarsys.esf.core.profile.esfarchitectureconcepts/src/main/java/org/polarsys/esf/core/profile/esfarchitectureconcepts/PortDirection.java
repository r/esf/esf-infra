/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/

package org.polarsys.esf.core.profile.esfarchitectureconcepts;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.uml2.uml.Port;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;

/**
 * Handle calculation of port direction based on extension points
 */
public class PortDirection {

	private static final String CLASS = "class"; //$NON-NLS-1$

	public static final String IPORT_DIRECTION_ID = ESFArchitectureConceptsActivator.PLUGIN_ID + ".portdirection"; //$NON-NLS-1$

	/**
	 * Retrieve a direction from a port. The first extension returning a
	 * defined value (in, out, inout) is returned
	 * 
	 * @param port a UML port for which the direction should be determined
	 * @return the direction of the passed port
	 */
	public static SDirection getSDirection(Port port) {
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(IPORT_DIRECTION_ID);

		for (IConfigurationElement configElement : configElements) {
			try {
				final Object obj = configElement.createExecutableExtension(CLASS);
				if (obj instanceof IGetPortDirection) {
					IGetPortDirection calcDirection = (IGetPortDirection) obj;
					SDirection direction = calcDirection.getSDirectionLAnalysis(port);
					if (direction != SDirection.UNDEFINED) {
						return direction;
					}
				}
			} catch (CoreException exception) {
				exception.printStackTrace();
			}
		}
		// No value via extension point, return default calculation
		if (port.getProvideds().size() > 0 && port.getRequireds().size() > 0) {
			return SDirection.INOUT;
		} else if (port.getProvideds().size() > 0) {
			return SDirection.IN;
		} else if (port.getRequireds().size() > 0) {
			return SDirection.OUT;
		}
		return  SDirection.UNDEFINED;
	}
}
