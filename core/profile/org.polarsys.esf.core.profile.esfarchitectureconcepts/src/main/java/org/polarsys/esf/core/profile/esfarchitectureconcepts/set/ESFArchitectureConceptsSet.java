/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.set;

/**
 * ESFArchitectureConcepts profile set.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFArchitectureConceptsSet {

    /** ESFArchitectureConcepts profile path. */
    public static final String PROFILE_PATH =
        "pathmap://ESFARCHITECTURECONCEPTS_PROFILE/esfarchitectureconcepts.profile.uml"; //$NON-NLS-1$

    /** ESFArchitectureConcepts profile URI. */
    public static final String PROFILE_URI =
        "http://www.polarsys.org/esf/0.7.0/ESFArchitectureConcepts"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private ESFArchitectureConceptsSet() {
     // Nothing to do
    }
}
