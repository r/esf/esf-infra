/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle. Central singleton for
 * the ESFArchitectureConcepts Profile plug-in.
 *
 * @author $Author: jdumont $
 * @version $Revision: 183 $
 */
public class ESFArchitectureConceptsActivator
    extends EMFPlugin
    implements BundleActivator {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.polarsys.esf.core.profile.esfarchitectureconcepts"; //$NON-NLS-1$

    /** Keep track of the encapsulating singleton. */
    public static final ESFArchitectureConceptsActivator INSTANCE = new ESFArchitectureConceptsActivator();

    /** Execution context for the bundle. */
    private static BundleContext sBundleContext = null;

    /** Keep track of the implementation singleton. */
    private static Implementation sPlugin = null;

    /**
     * Create the instance.
     */
    public ESFArchitectureConceptsActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * Get BundlexContext.
     *
     * @return BundleContext
     */
    static BundleContext getContext() {
        return sBundleContext;
    }

    /**
     * {@inheritDoc}
     * 
     * Added to ensure that the activator can work with the profile mechanisms.
     */
    @Override
    public void start(final BundleContext pContext) throws Exception {
        sBundleContext = pContext;

    }

    /**
     * {@inheritDoc}
     * 
     * Added to ensure that the activator can work with the profile mechanisms.
     */
    @Override
    public void stop(final BundleContext pContext) throws Exception {
        sBundleContext = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * The actual implementation of the Eclipse <b>UIPlugin</b>.
     */
    public static class Implementation
        extends EclipsePlugin {

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember of the static instance
            sPlugin = this;
        }
    }
}
