/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.polarsys.esf.esfarchitectureconcepts.impl.MESFArchitectureConceptsFactory;

/**
 * This factory overrides the generated factory {@link IESFArchitectureConceptsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: jdumont $
 * @version $Revision: 176 $
 */
public interface IMESFArchitectureConceptsFactory
    extends IESFArchitectureConceptsFactory {

    /**
     * Specialise the eINSTANCE initialisation with the new interface type
     * (overridden in the override_factory extension).
     */
    IMESFArchitectureConceptsFactory INSTANCE = MESFArchitectureConceptsFactory.init();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSBlock createSBlock();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSModel createSModel();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSPart createSPart();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSPortRole createSPortRole();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSPort createSPort();

    /**
     * {@inheritDoc}
     */
    @Override
    IMSConnector createSConnector();
}
