/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

/**
 * This interface can override the generated interface {@link ISPort} and
 * will be used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 176 $
 */
public interface IMSPort
    extends ISPort {

}
