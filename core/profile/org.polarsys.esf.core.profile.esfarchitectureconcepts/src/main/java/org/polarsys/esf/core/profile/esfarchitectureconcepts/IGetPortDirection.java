/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/

package org.polarsys.esf.core.profile.esfarchitectureconcepts;

import org.eclipse.uml2.uml.Port;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;

public interface IGetPortDirection {
	/**
	 * Returns the direction of a port
	 * Domain-specific implementations should return UNDEFINED, if the port
	 * does not apply a stereotype from which the direction can be deferred.
	 */
	SDirection getSDirectionLAnalysis(Port port);
}
