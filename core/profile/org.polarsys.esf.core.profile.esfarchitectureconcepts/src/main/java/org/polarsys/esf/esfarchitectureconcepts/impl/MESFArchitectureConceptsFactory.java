/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsFactory;
import org.polarsys.esf.esfarchitectureconcepts.IMESFArchitectureConceptsFactory;
import org.polarsys.esf.esfarchitectureconcepts.IMSBlock;
import org.polarsys.esf.esfarchitectureconcepts.IMSConnector;
import org.polarsys.esf.esfarchitectureconcepts.IMSModel;
import org.polarsys.esf.esfarchitectureconcepts.IMSPart;
import org.polarsys.esf.esfarchitectureconcepts.IMSPort;
import org.polarsys.esf.esfarchitectureconcepts.IMSPortRole;

/**
 * This factory overrides the generated factory {@link ESFArchitectureConceptsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: jdumont $
 * @version $Revision: 186 $
 */
public class MESFArchitectureConceptsFactory
    extends ESFArchitectureConceptsFactory
    implements IMESFArchitectureConceptsFactory {

    /**
     * Default constructor.
     */
    public MESFArchitectureConceptsFactory() {
        super();
    }

    /**
     * @return The initialised factory
     */
    public static IMESFArchitectureConceptsFactory init() {

        IESFArchitectureConceptsFactory vFactory = ESFArchitectureConceptsFactory.init();

        // Check if the factory returned by the standard implementation can suit,
        // otherwise create a new instance
        if (!(vFactory instanceof IMESFArchitectureConceptsFactory)) {
            vFactory = new MESFArchitectureConceptsFactory();
        }

        return (IMESFArchitectureConceptsFactory) vFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSBlock createSBlock() {
        IMSBlock vSBlock = new MSBlock();
        return vSBlock;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSModel createSModel() {
        IMSModel vSModel = new MSModel();
        return vSModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSPart createSPart() {
        IMSPart vSPart = new MSPart();
        return vSPart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSPortRole createSPortRole() {
        IMSPortRole vSPortRole = new MSPortRole();
        return vSPortRole;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSPort createSPort() {
        IMSPort vSPort = new MSPort();
        return vSPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSConnector createSConnector() {
        IMSConnector vSConnector = new MSConnector();
        return vSConnector;
    }
}
