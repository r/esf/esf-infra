/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Classifier;

import org.polarsys.esf.esfbehaviours.IESFBehavioursPackage;
import org.polarsys.esf.esfbehaviours.ISEvent;

import org.polarsys.esf.esfproperties.ISProbability;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SEvent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfbehaviours.impl.SEvent#getBase_Classifier <em>Base Classifier</em>}</li>
 * <li>{@link org.polarsys.esf.esfbehaviours.impl.SEvent#getProbability <em>Probability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SEvent
    extends AbstractSBehaviourObject
    implements ISEvent {

    /**
     * The cached value of the '{@link #getBase_Classifier() <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Classifier()
     * @generated
     * @ordered
     */
    protected Classifier base_Classifier;

    /**
     * The cached value of the '{@link #getProbability() <em>Probability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProbability()
     * @generated
     * @ordered
     */
    protected ISProbability probability;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SEvent() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFBehavioursPackage.Literals.SEVENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Classifier getBase_Classifier() {
        if (base_Classifier != null && base_Classifier.eIsProxy()) {
            InternalEObject oldBase_Classifier = (InternalEObject) base_Classifier;
            base_Classifier = (Classifier) eResolveProxy(oldBase_Classifier);
            if (base_Classifier != oldBase_Classifier) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(
                        this,
                        Notification.RESOLVE,
                        IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER,
                        oldBase_Classifier,
                        base_Classifier));
            }
        }
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Classifier basicGetBase_Classifier() {
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Classifier(Classifier newBase_Classifier) {
        Classifier oldBase_Classifier = base_Classifier;
        base_Classifier = newBase_Classifier;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(
                this,
                Notification.SET,
                IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER,
                oldBase_Classifier,
                base_Classifier));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISProbability getProbability() {
        if (probability != null && probability.eIsProxy()) {
            InternalEObject oldProbability = (InternalEObject) probability;
            probability = (ISProbability) eResolveProxy(oldProbability);
            if (probability != oldProbability) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(
                        this,
                        Notification.RESOLVE,
                        IESFBehavioursPackage.SEVENT__PROBABILITY,
                        oldProbability,
                        probability));
            }
        }
        return probability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISProbability basicGetProbability() {
        return probability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setProbability(ISProbability newProbability) {
        ISProbability oldProbability = probability;
        probability = newProbability;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(
                this,
                Notification.SET,
                IESFBehavioursPackage.SEVENT__PROBABILITY,
                oldProbability,
                probability));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER:
                if (resolve)
                    return getBase_Classifier();
                return basicGetBase_Classifier();
            case IESFBehavioursPackage.SEVENT__PROBABILITY:
                if (resolve)
                    return getProbability();
                return basicGetProbability();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER:
                setBase_Classifier((Classifier) newValue);
                return;
            case IESFBehavioursPackage.SEVENT__PROBABILITY:
                setProbability((ISProbability) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER:
                setBase_Classifier((Classifier) null);
                return;
            case IESFBehavioursPackage.SEVENT__PROBABILITY:
                setProbability((ISProbability) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IESFBehavioursPackage.SEVENT__BASE_CLASSIFIER:
                return base_Classifier != null;
            case IESFBehavioursPackage.SEVENT__PROBABILITY:
                return probability != null;
        }
        return super.eIsSet(featureID);
    }

} // SEvent
