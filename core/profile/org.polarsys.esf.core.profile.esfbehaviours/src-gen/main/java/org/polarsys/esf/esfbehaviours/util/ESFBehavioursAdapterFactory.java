/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.polarsys.esf.esfbehaviours.*;

import org.polarsys.esf.esfcore.IAbstractSElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfbehaviours.IESFBehavioursPackage
 * @generated
 */
public class ESFBehavioursAdapterFactory
    extends AdapterFactoryImpl {

    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static IESFBehavioursPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFBehavioursAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = IESFBehavioursPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance
     * object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ESFBehavioursSwitch<Adapter> modelSwitch = new ESFBehavioursSwitch<Adapter>() {

        @Override
        public Adapter caseAbstractSBehaviourObject(IAbstractSBehaviourObject object) {
            return createAbstractSBehaviourObjectAdapter();
        }

        @Override
        public Adapter caseSEvent(ISEvent object) {
            return createSEventAdapter();
        }

        @Override
        public Adapter caseSState(ISState object) {
            return createSStateAdapter();
        }

        @Override
        public Adapter caseAbstractSElement(IAbstractSElement object) {
            return createAbstractSElementAdapter();
        }

        @Override
        public Adapter defaultCase(EObject object) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject
     * <em>Abstract SBehaviour Object</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject
     * @generated
     */
    public Adapter createAbstractSBehaviourObjectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfbehaviours.ISEvent <em>SEvent</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfbehaviours.ISEvent
     * @generated
     */
    public Adapter createSEventAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfbehaviours.ISState <em>SState</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfbehaviours.ISState
     * @generated
     */
    public Adapter createSStateAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement
     * <em>Abstract SElement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSElement
     * @generated
     */
    public Adapter createAbstractSElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // ESFBehavioursAdapterFactory
