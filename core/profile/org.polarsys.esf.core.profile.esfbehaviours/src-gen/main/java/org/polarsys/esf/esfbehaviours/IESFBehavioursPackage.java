/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfbehaviours.IESFBehavioursFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFBehaviours'"
 * @generated
 */
public interface IESFBehavioursPackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "esfbehaviours"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFBehaviours"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "ESFBehaviours"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    IESFBehavioursPackage eINSTANCE = org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage.init();

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfbehaviours.impl.AbstractSBehaviourObject
     * <em>Abstract SBehaviour Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfbehaviours.impl.AbstractSBehaviourObject
     * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getAbstractSBehaviourObject()
     * @generated
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT__UUID = IESFCorePackage.ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT__NAME = IESFCorePackage.ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT__DESCRIPTION = IESFCorePackage.ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The number of structural features of the '<em>Abstract SBehaviour Object</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Abstract SBehaviour Object</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SBEHAVIOUR_OBJECT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfbehaviours.impl.SEvent <em>SEvent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfbehaviours.impl.SEvent
     * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getSEvent()
     * @generated
     */
    int SEVENT = 1;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT__UUID = ABSTRACT_SBEHAVIOUR_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT__NAME = ABSTRACT_SBEHAVIOUR_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT__DESCRIPTION = ABSTRACT_SBEHAVIOUR_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT__BASE_CLASSIFIER = ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Probability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT__PROBABILITY = ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>SEvent</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT_FEATURE_COUNT = ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>SEvent</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEVENT_OPERATION_COUNT = ABSTRACT_SBEHAVIOUR_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfbehaviours.impl.SState <em>SState</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfbehaviours.impl.SState
     * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getSState()
     * @generated
     */
    int SSTATE = 2;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE__UUID = ABSTRACT_SBEHAVIOUR_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE__NAME = ABSTRACT_SBEHAVIOUR_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE__DESCRIPTION = ABSTRACT_SBEHAVIOUR_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE__BASE_CLASSIFIER = ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SState</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE_FEATURE_COUNT = ABSTRACT_SBEHAVIOUR_OBJECT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SState</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSTATE_OPERATION_COUNT = ABSTRACT_SBEHAVIOUR_OBJECT_OPERATION_COUNT + 0;

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject
     * <em>Abstract SBehaviour Object</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SBehaviour Object</em>'.
     * @see org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject
     * @generated
     */
    EClass getAbstractSBehaviourObject();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfbehaviours.ISEvent <em>SEvent</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SEvent</em>'.
     * @see org.polarsys.esf.esfbehaviours.ISEvent
     * @generated
     */
    EClass getSEvent();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfbehaviours.ISEvent#getBase_Classifier
     * <em>Base Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Classifier</em>'.
     * @see org.polarsys.esf.esfbehaviours.ISEvent#getBase_Classifier()
     * @see #getSEvent()
     * @generated
     */
    EReference getSEvent_Base_Classifier();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfbehaviours.ISEvent#getProbability
     * <em>Probability</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Probability</em>'.
     * @see org.polarsys.esf.esfbehaviours.ISEvent#getProbability()
     * @see #getSEvent()
     * @generated
     */
    EReference getSEvent_Probability();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfbehaviours.ISState <em>SState</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SState</em>'.
     * @see org.polarsys.esf.esfbehaviours.ISState
     * @generated
     */
    EClass getSState();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfbehaviours.ISState#getBase_Classifier
     * <em>Base Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Classifier</em>'.
     * @see org.polarsys.esf.esfbehaviours.ISState#getBase_Classifier()
     * @see #getSState()
     * @generated
     */
    EReference getSState_Base_Classifier();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    IESFBehavioursFactory getESFBehavioursFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfbehaviours.impl.AbstractSBehaviourObject
         * <em>Abstract SBehaviour Object</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfbehaviours.impl.AbstractSBehaviourObject
         * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getAbstractSBehaviourObject()
         * @generated
         */
        EClass ABSTRACT_SBEHAVIOUR_OBJECT = eINSTANCE.getAbstractSBehaviourObject();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfbehaviours.impl.SEvent <em>SEvent</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfbehaviours.impl.SEvent
         * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getSEvent()
         * @generated
         */
        EClass SEVENT = eINSTANCE.getSEvent();

        /**
         * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEVENT__BASE_CLASSIFIER = eINSTANCE.getSEvent_Base_Classifier();

        /**
         * The meta object literal for the '<em><b>Probability</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEVENT__PROBABILITY = eINSTANCE.getSEvent_Probability();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfbehaviours.impl.SState <em>SState</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfbehaviours.impl.SState
         * @see org.polarsys.esf.esfbehaviours.impl.ESFBehavioursPackage#getSState()
         * @generated
         */
        EClass SSTATE = eINSTANCE.getSState();

        /**
         * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SSTATE__BASE_CLASSIFIER = eINSTANCE.getSState_Base_Classifier();

    }

} // IESFBehavioursPackage
