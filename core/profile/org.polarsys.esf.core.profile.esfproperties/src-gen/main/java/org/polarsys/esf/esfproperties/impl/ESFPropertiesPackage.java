/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfcore.IESFCorePackage;

import org.polarsys.esf.esfproperties.IAbstractSProperty;
import org.polarsys.esf.esfproperties.IESFPropertiesFactory;
import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfproperties.ISCost;
import org.polarsys.esf.esfproperties.ISCriticality;
import org.polarsys.esf.esfproperties.ISDetectability;
import org.polarsys.esf.esfproperties.ISOccurence;
import org.polarsys.esf.esfproperties.ISProbability;
import org.polarsys.esf.esfproperties.ISSeverity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFPropertiesPackage
    extends EPackageImpl
    implements IESFPropertiesPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSPropertyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sCriticalityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sSeverityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sOccurenceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sDetectabilityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sProbabilityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sCostEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ESFPropertiesPackage() {
        super(eNS_URI, IESFPropertiesFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>
     * This method is used to initialize {@link IESFPropertiesPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static IESFPropertiesPackage init() {
        if (isInited)
            return (IESFPropertiesPackage) EPackage.Registry.INSTANCE.getEPackage(IESFPropertiesPackage.eNS_URI);

        // Obtain or create and register package
        ESFPropertiesPackage theESFPropertiesPackage =
            (ESFPropertiesPackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ESFPropertiesPackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new ESFPropertiesPackage());

        isInited = true;

        // Initialize simple dependencies
        IESFCorePackage.eINSTANCE.eClass();
        UMLPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theESFPropertiesPackage.createPackageContents();

        // Initialize created meta-data
        theESFPropertiesPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theESFPropertiesPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(IESFPropertiesPackage.eNS_URI, theESFPropertiesPackage);
        return theESFPropertiesPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSProperty() {
        return abstractSPropertyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSCriticality() {
        return sCriticalityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCriticality_Base_DataType() {
        return (EReference) sCriticalityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSSeverity() {
        return sSeverityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSSeverity_Base_DataType() {
        return (EReference) sSeverityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSOccurence() {
        return sOccurenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSOccurence_Base_DataType() {
        return (EReference) sOccurenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSDetectability() {
        return sDetectabilityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSDetectability_Base_DataType() {
        return (EReference) sDetectabilityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSProbability() {
        return sProbabilityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSProbability_Base_DataType() {
        return (EReference) sProbabilityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSCost() {
        return sCostEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCost_Base_DataType() {
        return (EReference) sCostEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public IESFPropertiesFactory getESFPropertiesFactory() {
        return (IESFPropertiesFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        abstractSPropertyEClass = createEClass(ABSTRACT_SPROPERTY);

        sCriticalityEClass = createEClass(SCRITICALITY);
        createEReference(sCriticalityEClass, SCRITICALITY__BASE_DATA_TYPE);

        sSeverityEClass = createEClass(SSEVERITY);
        createEReference(sSeverityEClass, SSEVERITY__BASE_DATA_TYPE);

        sOccurenceEClass = createEClass(SOCCURENCE);
        createEReference(sOccurenceEClass, SOCCURENCE__BASE_DATA_TYPE);

        sDetectabilityEClass = createEClass(SDETECTABILITY);
        createEReference(sDetectabilityEClass, SDETECTABILITY__BASE_DATA_TYPE);

        sProbabilityEClass = createEClass(SPROBABILITY);
        createEReference(sProbabilityEClass, SPROBABILITY__BASE_DATA_TYPE);

        sCostEClass = createEClass(SCOST);
        createEReference(sCostEClass, SCOST__BASE_DATA_TYPE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        IESFCorePackage theESFCorePackage =
            (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
        UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        abstractSPropertyEClass.getESuperTypes().add(theESFCorePackage.getAbstractSElement());
        sCriticalityEClass.getESuperTypes().add(this.getAbstractSProperty());
        sSeverityEClass.getESuperTypes().add(this.getAbstractSProperty());
        sOccurenceEClass.getESuperTypes().add(this.getAbstractSProperty());
        sDetectabilityEClass.getESuperTypes().add(this.getAbstractSProperty());
        sProbabilityEClass.getESuperTypes().add(this.getAbstractSProperty());
        sCostEClass.getESuperTypes().add(this.getAbstractSProperty());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            abstractSPropertyEClass,
            IAbstractSProperty.class,
            "AbstractSProperty", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);

        initEClass(
            sCriticalityEClass,
            ISCriticality.class,
            "SCriticality", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSCriticality_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISCriticality.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sSeverityEClass,
            ISSeverity.class,
            "SSeverity", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSSeverity_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISSeverity.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sOccurenceEClass,
            ISOccurence.class,
            "SOccurence", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSOccurence_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISOccurence.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sDetectabilityEClass,
            ISDetectability.class,
            "SDetectability", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSDetectability_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISDetectability.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sProbabilityEClass,
            ISProbability.class,
            "SProbability", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSProbability_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISProbability.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sCostEClass, ISCost.class, "SCost", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSCost_Base_DataType(),
            theUMLPackage.getDataType(),
            null,
            "base_DataType", //$NON-NLS-1$
            null,
            1,
            1,
            ISCost.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "ESFProperties" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // ESFPropertiesPackage
