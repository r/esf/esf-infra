/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties;

import org.eclipse.uml2.uml.DataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SDetectability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfproperties.ISDetectability#getBase_DataType <em>Base Data Type</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage#getSDetectability()
 * @model
 * @generated
 */
public interface ISDetectability
    extends IAbstractSProperty {

    /**
     * Returns the value of the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Data Type</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Data Type</em>' reference.
     * @see #setBase_DataType(DataType)
     * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage#getSDetectability_Base_DataType()
     * @model required="true" ordered="false"
     * @generated
     */
    DataType getBase_DataType();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfproperties.ISDetectability#getBase_DataType
     * <em>Base Data Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Data Type</em>' reference.
     * @see #getBase_DataType()
     * @generated
     */
    void setBase_DataType(DataType value);

} // ISDetectability
