/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.polarsys.esf.esfcore.IAbstractSElement;

import org.polarsys.esf.esfproperties.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage
 * @generated
 */
public class ESFPropertiesSwitch<T>
    extends Switch<T> {

    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static IESFPropertiesPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFPropertiesSwitch() {
        if (modelPackage == null) {
            modelPackage = IESFPropertiesPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
     * result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case IESFPropertiesPackage.ABSTRACT_SPROPERTY: {
                IAbstractSProperty abstractSProperty = (IAbstractSProperty) theEObject;
                T result = caseAbstractSProperty(abstractSProperty);
                if (result == null)
                    result = caseAbstractSElement(abstractSProperty);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SCRITICALITY: {
                ISCriticality sCriticality = (ISCriticality) theEObject;
                T result = caseSCriticality(sCriticality);
                if (result == null)
                    result = caseAbstractSProperty(sCriticality);
                if (result == null)
                    result = caseAbstractSElement(sCriticality);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SSEVERITY: {
                ISSeverity sSeverity = (ISSeverity) theEObject;
                T result = caseSSeverity(sSeverity);
                if (result == null)
                    result = caseAbstractSProperty(sSeverity);
                if (result == null)
                    result = caseAbstractSElement(sSeverity);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SOCCURENCE: {
                ISOccurence sOccurence = (ISOccurence) theEObject;
                T result = caseSOccurence(sOccurence);
                if (result == null)
                    result = caseAbstractSProperty(sOccurence);
                if (result == null)
                    result = caseAbstractSElement(sOccurence);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SDETECTABILITY: {
                ISDetectability sDetectability = (ISDetectability) theEObject;
                T result = caseSDetectability(sDetectability);
                if (result == null)
                    result = caseAbstractSProperty(sDetectability);
                if (result == null)
                    result = caseAbstractSElement(sDetectability);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SPROBABILITY: {
                ISProbability sProbability = (ISProbability) theEObject;
                T result = caseSProbability(sProbability);
                if (result == null)
                    result = caseAbstractSProperty(sProbability);
                if (result == null)
                    result = caseAbstractSElement(sProbability);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFPropertiesPackage.SCOST: {
                ISCost sCost = (ISCost) theEObject;
                T result = caseSCost(sCost);
                if (result == null)
                    result = caseAbstractSProperty(sCost);
                if (result == null)
                    result = caseAbstractSElement(sCost);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SProperty</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SProperty</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSProperty(IAbstractSProperty object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SCriticality</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SCriticality</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSCriticality(ISCriticality object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SSeverity</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SSeverity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSSeverity(ISSeverity object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SOccurence</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SOccurence</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSOccurence(ISOccurence object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SDetectability</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SDetectability</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSDetectability(ISDetectability object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SProbability</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SProbability</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSProbability(ISProbability object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SCost</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SCost</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSCost(ISCost object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSElement(IAbstractSElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // ESFPropertiesSwitch
