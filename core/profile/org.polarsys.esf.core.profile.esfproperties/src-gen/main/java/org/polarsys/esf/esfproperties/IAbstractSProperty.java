/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties;

import org.polarsys.esf.esfcore.IAbstractSElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SProperty</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage#getAbstractSProperty()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSProperty
    extends IAbstractSElement {
} // IAbstractSProperty
