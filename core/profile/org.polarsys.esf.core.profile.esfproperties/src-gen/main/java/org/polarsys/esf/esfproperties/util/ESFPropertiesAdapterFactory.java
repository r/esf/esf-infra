/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.polarsys.esf.esfcore.IAbstractSElement;

import org.polarsys.esf.esfproperties.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage
 * @generated
 */
public class ESFPropertiesAdapterFactory
    extends AdapterFactoryImpl {

    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static IESFPropertiesPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFPropertiesAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = IESFPropertiesPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance
     * object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ESFPropertiesSwitch<Adapter> modelSwitch = new ESFPropertiesSwitch<Adapter>() {

        @Override
        public Adapter caseAbstractSProperty(IAbstractSProperty object) {
            return createAbstractSPropertyAdapter();
        }

        @Override
        public Adapter caseSCriticality(ISCriticality object) {
            return createSCriticalityAdapter();
        }

        @Override
        public Adapter caseSSeverity(ISSeverity object) {
            return createSSeverityAdapter();
        }

        @Override
        public Adapter caseSOccurence(ISOccurence object) {
            return createSOccurenceAdapter();
        }

        @Override
        public Adapter caseSDetectability(ISDetectability object) {
            return createSDetectabilityAdapter();
        }

        @Override
        public Adapter caseSProbability(ISProbability object) {
            return createSProbabilityAdapter();
        }

        @Override
        public Adapter caseSCost(ISCost object) {
            return createSCostAdapter();
        }

        @Override
        public Adapter caseAbstractSElement(IAbstractSElement object) {
            return createAbstractSElementAdapter();
        }

        @Override
        public Adapter defaultCase(EObject object) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.IAbstractSProperty
     * <em>Abstract SProperty</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.IAbstractSProperty
     * @generated
     */
    public Adapter createAbstractSPropertyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISCriticality
     * <em>SCriticality</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISCriticality
     * @generated
     */
    public Adapter createSCriticalityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISSeverity <em>SSeverity</em>
     * }'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISSeverity
     * @generated
     */
    public Adapter createSSeverityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISOccurence
     * <em>SOccurence</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISOccurence
     * @generated
     */
    public Adapter createSOccurenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISDetectability
     * <em>SDetectability</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISDetectability
     * @generated
     */
    public Adapter createSDetectabilityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISProbability
     * <em>SProbability</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISProbability
     * @generated
     */
    public Adapter createSProbabilityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfproperties.ISCost <em>SCost</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfproperties.ISCost
     * @generated
     */
    public Adapter createSCostAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement
     * <em>Abstract SElement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSElement
     * @generated
     */
    public Adapter createAbstractSElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // ESFPropertiesAdapterFactory
