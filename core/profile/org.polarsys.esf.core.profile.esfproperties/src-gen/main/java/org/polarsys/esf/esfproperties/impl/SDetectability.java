/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.DataType;

import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfproperties.ISDetectability;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SDetectability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfproperties.impl.SDetectability#getBase_DataType <em>Base Data Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SDetectability
    extends AbstractSProperty
    implements ISDetectability {

    /**
     * The cached value of the '{@link #getBase_DataType() <em>Base Data Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_DataType()
     * @generated
     * @ordered
     */
    protected DataType base_DataType;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SDetectability() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFPropertiesPackage.Literals.SDETECTABILITY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public DataType getBase_DataType() {
        if (base_DataType != null && base_DataType.eIsProxy()) {
            InternalEObject oldBase_DataType = (InternalEObject) base_DataType;
            base_DataType = (DataType) eResolveProxy(oldBase_DataType);
            if (base_DataType != oldBase_DataType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(
                        this,
                        Notification.RESOLVE,
                        IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE,
                        oldBase_DataType,
                        base_DataType));
            }
        }
        return base_DataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public DataType basicGetBase_DataType() {
        return base_DataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_DataType(DataType newBase_DataType) {
        DataType oldBase_DataType = base_DataType;
        base_DataType = newBase_DataType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(
                this,
                Notification.SET,
                IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE,
                oldBase_DataType,
                base_DataType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE:
                if (resolve)
                    return getBase_DataType();
                return basicGetBase_DataType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE:
                setBase_DataType((DataType) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE:
                setBase_DataType((DataType) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IESFPropertiesPackage.SDETECTABILITY__BASE_DATA_TYPE:
                return base_DataType != null;
        }
        return super.eIsSet(featureID);
    }

} // SDetectability
