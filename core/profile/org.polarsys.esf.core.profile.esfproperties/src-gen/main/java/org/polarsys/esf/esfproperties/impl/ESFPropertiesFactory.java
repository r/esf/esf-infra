/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.esf.esfproperties.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFPropertiesFactory
    extends EFactoryImpl
    implements IESFPropertiesFactory {

    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static IESFPropertiesFactory init() {
        try {
            IESFPropertiesFactory theESFPropertiesFactory =
                (IESFPropertiesFactory) EPackage.Registry.INSTANCE.getEFactory(IESFPropertiesPackage.eNS_URI);
            if (theESFPropertiesFactory != null) {
                return theESFPropertiesFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ESFPropertiesFactory();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFPropertiesFactory() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case IESFPropertiesPackage.SCRITICALITY:
                return createSCriticality();
            case IESFPropertiesPackage.SSEVERITY:
                return createSSeverity();
            case IESFPropertiesPackage.SOCCURENCE:
                return createSOccurence();
            case IESFPropertiesPackage.SDETECTABILITY:
                return createSDetectability();
            case IESFPropertiesPackage.SPROBABILITY:
                return createSProbability();
            case IESFPropertiesPackage.SCOST:
                return createSCost();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCriticality createSCriticality() {
        SCriticality sCriticality = new SCriticality();
        return sCriticality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISSeverity createSSeverity() {
        SSeverity sSeverity = new SSeverity();
        return sSeverity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISOccurence createSOccurence() {
        SOccurence sOccurence = new SOccurence();
        return sOccurence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetectability createSDetectability() {
        SDetectability sDetectability = new SDetectability();
        return sDetectability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISProbability createSProbability() {
        SProbability sProbability = new SProbability();
        return sProbability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCost createSCost() {
        SCost sCost = new SCost();
        return sCost;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public IESFPropertiesPackage getESFPropertiesPackage() {
        return (IESFPropertiesPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static IESFPropertiesPackage getPackage() {
        return IESFPropertiesPackage.eINSTANCE;
    }

} // ESFPropertiesFactory
