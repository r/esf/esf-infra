/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts;

import org.polarsys.esf.esfsafetyconcepts.impl.MESFSafetyConceptsFactory;

/**
 * This interface can override the generated interface {@link IESFSafetyConceptsFactory}.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 186 $
 */
public interface IMESFSafetyConceptsFactory
    extends IESFSafetyConceptsFactory {

    /**
     * Specialise the eINSTANCE initialisation with the new interface type
     * (overridden in the override_factory extension).
     */
    IMESFSafetyConceptsFactory INSTANCE = MESFSafetyConceptsFactory.init();

    @Override
    IMSSafetyArtifacts createSSafetyArtifacts();

}
