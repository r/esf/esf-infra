/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.impl;

import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;
import org.polarsys.esf.esfsafetyconcepts.IMSSafetyArtifacts;

/**
 * This class can override the generated class {@link MSSafetyArtifacts} and will be
 * used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSSafetyArtifacts
    extends SSafetyArtifacts
    implements IMSSafetyArtifacts {

    /**
     * Default constructor.
     */
    public MSSafetyArtifacts() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return GenericAbstractSElement.getName(getBase_Package());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUUID() {
        return GenericAbstractSElement.getUUID(getBase_Package());
    }
}
