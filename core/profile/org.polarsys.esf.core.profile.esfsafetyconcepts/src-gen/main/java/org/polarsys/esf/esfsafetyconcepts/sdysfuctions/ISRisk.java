/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SRisk</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getHazard <em>Hazard</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent <em>Failure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getCausesList <em>Causes List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getEffectsList <em>Effects List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk()
 * @model
 * @generated
 */
public interface ISRisk
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getBase_Class <em>Base
     * Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Hazard</b></em>' reference.
     * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getRisk
     * <em>Risk</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Hazard</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Hazard</em>' reference.
     * @see #setHazard(ISHazard)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk_Hazard()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getRisk
     * @model opposite="risk" ordered="false"
     * @generated
     */
    ISHazard getHazard();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getHazard <em>Hazard</em>}'
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Hazard</em>' reference.
     * @see #getHazard()
     * @generated
     */
    void setHazard(ISHazard value);

    /**
     * Returns the value of the '<em><b>Failure Event</b></em>' reference.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk <em>Risk</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Event</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Event</em>' reference.
     * @see #setFailureEvent(ISFailureEvent)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk_FailureEvent()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk
     * @model opposite="risk" ordered="false"
     * @generated
     */
    ISFailureEvent getFailureEvent();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent <em>Failure
     * Event</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Failure Event</em>' reference.
     * @see #getFailureEvent()
     * @generated
     */
    void setFailureEvent(ISFailureEvent value);

    /**
     * Returns the value of the '<em><b>Causes List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getRisksList <em>Risks List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Causes List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Causes List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk_CausesList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getRisksList
     * @model opposite="risksList" ordered="false"
     * @generated
     */
    EList<ISCause> getCausesList();

    /**
     * Returns the value of the '<em><b>Effects List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getRisksList <em>Risks List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Effects List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Effects List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSRisk_EffectsList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getRisksList
     * @model opposite="risksList" ordered="false"
     * @generated
     */
    EList<ISEffect> getEffectsList();

} // ISRisk
