/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import org.eclipse.uml2.uml.Classifier;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SRecommendation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation#getBase_Classifier <em>Base
 * Classifier</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSRecommendation()
 * @model
 * @generated
 */
public interface ISRecommendation
    extends IAbstractSSafetyConcept {

    /**
     * Returns the value of the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Classifier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Classifier</em>' reference.
     * @see #setBase_Classifier(Classifier)
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSRecommendation_Base_Classifier()
     * @model required="true" ordered="false"
     * @generated
     */
    Classifier getBase_Classifier();

    /**
     * Sets the value of the
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation#getBase_Classifier <em>Base
     * Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Classifier</em>' reference.
     * @see #getBase_Classifier()
     * @generated
     */
    void setBase_Classifier(Classifier value);

} // ISRecommendation
