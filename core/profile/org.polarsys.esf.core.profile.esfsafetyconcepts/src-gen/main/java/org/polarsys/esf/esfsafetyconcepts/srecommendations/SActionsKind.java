/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>SActions Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSActionsKind()
 * @model
 * @generated
 */
public enum SActionsKind implements Enumerator {
    /**
     * The '<em><b>Preventive Action</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #PREVENTIVE_ACTION_VALUE
     * @generated
     * @ordered
     */
    PREVENTIVE_ACTION(0, "PreventiveAction", "PreventiveAction"), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>SCorrective Action</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #SCORRECTIVE_ACTION_VALUE
     * @generated
     * @ordered
     */
    SCORRECTIVE_ACTION(1, "SCorrectiveAction", "SCorrectiveAction"); //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Preventive Action</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Preventive Action</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #PREVENTIVE_ACTION
     * @model name="PreventiveAction"
     * @generated
     * @ordered
     */
    public static final int PREVENTIVE_ACTION_VALUE = 0;

    /**
     * The '<em><b>SCorrective Action</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>SCorrective Action</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #SCORRECTIVE_ACTION
     * @model name="SCorrectiveAction"
     * @generated
     * @ordered
     */
    public static final int SCORRECTIVE_ACTION_VALUE = 1;

    /**
     * An array of all the '<em><b>SActions Kind</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final SActionsKind[] VALUES_ARRAY = new SActionsKind[] {PREVENTIVE_ACTION, SCORRECTIVE_ACTION,};

    /**
     * A public read-only list of all the '<em><b>SActions Kind</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<SActionsKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>SActions Kind</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SActionsKind get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SActionsKind result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>SActions Kind</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SActionsKind getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SActionsKind result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>SActions Kind</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SActionsKind get(int value) {
        switch (value) {
            case PREVENTIVE_ACTION_VALUE:
                return PREVENTIVE_ACTION;
            case SCORRECTIVE_ACTION_VALUE:
                return SCORRECTIVE_ACTION;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private SActionsKind(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} // SActionsKind
