/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.esfcore.IESFCorePackage;
import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.impl.ESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsFactory;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class SDysfunctionsPackage
    extends EPackageImpl
    implements ISDysfunctionsPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSDysfunctionObjectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sFailureModeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sFailureEventEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sEffectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sRiskEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sHazardEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sCauseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sFailureStateEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private SDysfunctionsPackage() {
        super(eNS_URI, ISDysfunctionsFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>
     * This method is used to initialize {@link ISDysfunctionsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ISDysfunctionsPackage init() {
        if (isInited)
            return (ISDysfunctionsPackage) EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI);

        // Obtain or create and register package
        SDysfunctionsPackage theSDysfunctionsPackage =
            (SDysfunctionsPackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SDysfunctionsPackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new SDysfunctionsPackage());

        isInited = true;

        // Initialize simple dependencies
        IESFCorePackage.eINSTANCE.eClass();
        IESFPropertiesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        ESFSafetyConceptsPackage theESFSafetyConceptsPackage = (ESFSafetyConceptsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(IESFSafetyConceptsPackage.eNS_URI) instanceof ESFSafetyConceptsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(IESFSafetyConceptsPackage.eNS_URI)
                : IESFSafetyConceptsPackage.eINSTANCE);
        SRecommendationsPackage theSRecommendationsPackage = (SRecommendationsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(ISRecommendationsPackage.eNS_URI) instanceof SRecommendationsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI)
                : ISRecommendationsPackage.eINSTANCE);

        // Create package meta-data objects
        theSDysfunctionsPackage.createPackageContents();
        theESFSafetyConceptsPackage.createPackageContents();
        theSRecommendationsPackage.createPackageContents();

        // Initialize created meta-data
        theSDysfunctionsPackage.initializePackageContents();
        theESFSafetyConceptsPackage.initializePackageContents();
        theSRecommendationsPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theSDysfunctionsPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ISDysfunctionsPackage.eNS_URI, theSDysfunctionsPackage);
        return theSDysfunctionsPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSDysfunctionObject() {
        return abstractSDysfunctionObjectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSFailureMode() {
        return sFailureModeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureMode_Base_Class() {
        return (EReference) sFailureModeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureMode_FailureEvent() {
        return (EReference) sFailureModeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureMode_Barrier() {
        return (EReference) sFailureModeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSFailureEvent() {
        return sFailureEventEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Base_Class() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_EffectsList() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Risk() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_CausesList() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_DetectionsList() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_RecommendationsList() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Severity() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Criticality() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Cost() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Occurence() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Detectability() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_Probability() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EAttribute getSFailureEvent_FearedEvent() {
        return (EAttribute) sFailureEventEClass.getEStructuralFeatures().get(12);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureEvent_FailureMode() {
        return (EReference) sFailureEventEClass.getEStructuralFeatures().get(13);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSEffect() {
        return sEffectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_Base_Class() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_RisksList() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_Severity() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_Cost() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_Criticality() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEffect_FailureEventsList() {
        return (EReference) sEffectEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSRisk() {
        return sRiskEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRisk_Base_Class() {
        return (EReference) sRiskEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRisk_Hazard() {
        return (EReference) sRiskEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRisk_FailureEvent() {
        return (EReference) sRiskEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRisk_CausesList() {
        return (EReference) sRiskEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRisk_EffectsList() {
        return (EReference) sRiskEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSHazard() {
        return sHazardEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSHazard_Base_Class() {
        return (EReference) sHazardEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSHazard_Risk() {
        return (EReference) sHazardEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSCause() {
        return sCauseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCause_Base_Class() {
        return (EReference) sCauseEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCause_FailureEventsList() {
        return (EReference) sCauseEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCause_Occurence() {
        return (EReference) sCauseEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCause_Detectability() {
        return (EReference) sCauseEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSCause_RisksList() {
        return (EReference) sCauseEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSFailureState() {
        return sFailureStateEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureState_Base_Class() {
        return (EReference) sFailureStateEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSFailureState_FailureMode() {
        return (EReference) sFailureStateEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDysfunctionsFactory getSDysfunctionsFactory() {
        return (ISDysfunctionsFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        abstractSDysfunctionObjectEClass = createEClass(ABSTRACT_SDYSFUNCTION_OBJECT);

        sFailureModeEClass = createEClass(SFAILURE_MODE);
        createEReference(sFailureModeEClass, SFAILURE_MODE__BASE_CLASS);
        createEReference(sFailureModeEClass, SFAILURE_MODE__FAILURE_EVENT);
        createEReference(sFailureModeEClass, SFAILURE_MODE__BARRIER);

        sFailureEventEClass = createEClass(SFAILURE_EVENT);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__BASE_CLASS);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__EFFECTS_LIST);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__RISK);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__CAUSES_LIST);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__DETECTIONS_LIST);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__RECOMMENDATIONS_LIST);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__SEVERITY);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__CRITICALITY);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__COST);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__OCCURENCE);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__DETECTABILITY);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__PROBABILITY);
        createEAttribute(sFailureEventEClass, SFAILURE_EVENT__FEARED_EVENT);
        createEReference(sFailureEventEClass, SFAILURE_EVENT__FAILURE_MODE);

        sEffectEClass = createEClass(SEFFECT);
        createEReference(sEffectEClass, SEFFECT__BASE_CLASS);
        createEReference(sEffectEClass, SEFFECT__RISKS_LIST);
        createEReference(sEffectEClass, SEFFECT__SEVERITY);
        createEReference(sEffectEClass, SEFFECT__COST);
        createEReference(sEffectEClass, SEFFECT__CRITICALITY);
        createEReference(sEffectEClass, SEFFECT__FAILURE_EVENTS_LIST);

        sRiskEClass = createEClass(SRISK);
        createEReference(sRiskEClass, SRISK__BASE_CLASS);
        createEReference(sRiskEClass, SRISK__HAZARD);
        createEReference(sRiskEClass, SRISK__FAILURE_EVENT);
        createEReference(sRiskEClass, SRISK__CAUSES_LIST);
        createEReference(sRiskEClass, SRISK__EFFECTS_LIST);

        sHazardEClass = createEClass(SHAZARD);
        createEReference(sHazardEClass, SHAZARD__BASE_CLASS);
        createEReference(sHazardEClass, SHAZARD__RISK);

        sCauseEClass = createEClass(SCAUSE);
        createEReference(sCauseEClass, SCAUSE__BASE_CLASS);
        createEReference(sCauseEClass, SCAUSE__FAILURE_EVENTS_LIST);
        createEReference(sCauseEClass, SCAUSE__OCCURENCE);
        createEReference(sCauseEClass, SCAUSE__DETECTABILITY);
        createEReference(sCauseEClass, SCAUSE__RISKS_LIST);

        sFailureStateEClass = createEClass(SFAILURE_STATE);
        createEReference(sFailureStateEClass, SFAILURE_STATE__BASE_CLASS);
        createEReference(sFailureStateEClass, SFAILURE_STATE__FAILURE_MODE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        IESFCorePackage theESFCorePackage =
            (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
        UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
        ISRecommendationsPackage theSRecommendationsPackage =
            (ISRecommendationsPackage) EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI);
        IESFPropertiesPackage theESFPropertiesPackage =
            (IESFPropertiesPackage) EPackage.Registry.INSTANCE.getEPackage(IESFPropertiesPackage.eNS_URI);
        TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        abstractSDysfunctionObjectEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyConcept());
        sFailureModeEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sFailureEventEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sEffectEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sRiskEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sHazardEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sCauseEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());
        sFailureStateEClass.getESuperTypes().add(this.getAbstractSDysfunctionObject());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            abstractSDysfunctionObjectEClass,
            IAbstractSDysfunctionObject.class,
            "AbstractSDysfunctionObject", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);

        initEClass(
            sFailureModeEClass,
            ISFailureMode.class,
            "SFailureMode", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSFailureMode_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureMode.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureMode_FailureEvent(),
            this.getSFailureEvent(),
            this.getSFailureEvent_FailureMode(),
            "failureEvent", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureMode.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureMode_Barrier(),
            theSRecommendationsPackage.getSBarrier(),
            null,
            "barrier", //$NON-NLS-1$
            null,
            0,
            1,
            ISFailureMode.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sFailureEventEClass,
            ISFailureEvent.class,
            "SFailureEvent", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSFailureEvent_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_EffectsList(),
            this.getSEffect(),
            this.getSEffect_FailureEventsList(),
            "effectsList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Risk(),
            this.getSRisk(),
            this.getSRisk_FailureEvent(),
            "risk", //$NON-NLS-1$
            null,
            0,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_CausesList(),
            this.getSCause(),
            this.getSCause_FailureEventsList(),
            "causesList", //$NON-NLS-1$
            null,
            1,
            -1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_DetectionsList(),
            theSRecommendationsPackage.getSDetection(),
            null,
            "detectionsList", //$NON-NLS-1$
            null,
            1,
            -1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_RecommendationsList(),
            theSRecommendationsPackage.getSRecommendation(),
            null,
            "recommendationsList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Severity(),
            theESFPropertiesPackage.getSSeverity(),
            null,
            "severity", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Criticality(),
            theESFPropertiesPackage.getSCriticality(),
            null,
            "criticality", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Cost(),
            theESFPropertiesPackage.getSCost(),
            null,
            "cost", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Occurence(),
            theESFPropertiesPackage.getSOccurence(),
            null,
            "occurence", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Detectability(),
            theESFPropertiesPackage.getSDetectability(),
            null,
            "detectability", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_Probability(),
            theESFPropertiesPackage.getSProbability(),
            null,
            "probability", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEAttribute(
            getSFailureEvent_FearedEvent(),
            theTypesPackage.getBoolean(),
            "fearedEvent", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_UNSETTABLE,
            !IS_ID,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureEvent_FailureMode(),
            this.getSFailureMode(),
            this.getSFailureMode_FailureEvent(),
            "failureMode", //$NON-NLS-1$
            null,
            0,
            1,
            ISFailureEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sEffectEClass, ISEffect.class, "SEffect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSEffect_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEffect_RisksList(),
            this.getSRisk(),
            this.getSRisk_EffectsList(),
            "risksList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEffect_Severity(),
            theESFPropertiesPackage.getSSeverity(),
            null,
            "severity", //$NON-NLS-1$
            null,
            1,
            1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEffect_Cost(),
            theESFPropertiesPackage.getSCost(),
            null,
            "cost", //$NON-NLS-1$
            null,
            1,
            1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEffect_Criticality(),
            theESFPropertiesPackage.getSCriticality(),
            null,
            "criticality", //$NON-NLS-1$
            null,
            1,
            1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEffect_FailureEventsList(),
            this.getSFailureEvent(),
            this.getSFailureEvent_EffectsList(),
            "failureEventsList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISEffect.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sRiskEClass, ISRisk.class, "SRisk", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSRisk_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISRisk.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSRisk_Hazard(),
            this.getSHazard(),
            this.getSHazard_Risk(),
            "hazard", //$NON-NLS-1$
            null,
            0,
            1,
            ISRisk.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSRisk_FailureEvent(),
            this.getSFailureEvent(),
            this.getSFailureEvent_Risk(),
            "failureEvent", //$NON-NLS-1$
            null,
            0,
            1,
            ISRisk.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSRisk_CausesList(),
            this.getSCause(),
            this.getSCause_RisksList(),
            "causesList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISRisk.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSRisk_EffectsList(),
            this.getSEffect(),
            this.getSEffect_RisksList(),
            "effectsList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISRisk.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sHazardEClass, ISHazard.class, "SHazard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSHazard_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISHazard.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSHazard_Risk(),
            this.getSRisk(),
            this.getSRisk_Hazard(),
            "risk", //$NON-NLS-1$
            null,
            0,
            1,
            ISHazard.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sCauseEClass, ISCause.class, "SCause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSCause_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISCause.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSCause_FailureEventsList(),
            this.getSFailureEvent(),
            this.getSFailureEvent_CausesList(),
            "failureEventsList", //$NON-NLS-1$
            null,
            1,
            -1,
            ISCause.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSCause_Occurence(),
            theESFPropertiesPackage.getSOccurence(),
            null,
            "occurence", //$NON-NLS-1$
            null,
            1,
            1,
            ISCause.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSCause_Detectability(),
            theESFPropertiesPackage.getSDetectability(),
            null,
            "detectability", //$NON-NLS-1$
            null,
            1,
            1,
            ISCause.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSCause_RisksList(),
            this.getSRisk(),
            this.getSRisk_CausesList(),
            "risksList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISCause.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sFailureStateEClass,
            ISFailureState.class,
            "SFailureState", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSFailureState_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISFailureState.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSFailureState_FailureMode(),
            this.getSFailureMode(),
            null,
            "failureMode", //$NON-NLS-1$
            null,
            0,
            1,
            ISFailureState.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "SDysfunctions" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // SDysfunctionsPackage
