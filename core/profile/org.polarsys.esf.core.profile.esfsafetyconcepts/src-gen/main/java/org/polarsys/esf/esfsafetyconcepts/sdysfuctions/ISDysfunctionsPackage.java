/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='SDysfunctions'"
 * @generated
 */
public interface ISDysfunctionsPackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "sdysfuctions"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFSafetyConcepts/SDysfunctions"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "SDysfunctions"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    ISDysfunctionsPackage eINSTANCE = org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage.init();

    /**
     * The meta object id for the
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.AbstractSDysfunctionObject <em>Abstract SDysfunction
     * Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.AbstractSDysfunctionObject
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getAbstractSDysfunctionObject()
     * @generated
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT__UUID = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT__NAME = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST =
        IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The number of structural features of the '<em>Abstract SDysfunction Object</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Abstract SDysfunction Object</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode <em>SFailure
     * Mode</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureMode()
     * @generated
     */
    int SFAILURE_MODE = 1;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Failure Event</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__FAILURE_EVENT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Barrier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE__BARRIER = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>SFailure Mode</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 3;

    /**
     * The number of operations of the '<em>SFailure Mode</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_MODE_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent
     * <em>SFailure Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureEvent()
     * @generated
     */
    int SFAILURE_EVENT = 2;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Effects List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__EFFECTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Risk</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__RISK = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Causes List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__CAUSES_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Detections List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__DETECTIONS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Recommendations List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__RECOMMENDATIONS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Severity</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__SEVERITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 6;

    /**
     * The feature id for the '<em><b>Criticality</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__CRITICALITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Cost</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__COST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 8;

    /**
     * The feature id for the '<em><b>Occurence</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__OCCURENCE = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 9;

    /**
     * The feature id for the '<em><b>Detectability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__DETECTABILITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 10;

    /**
     * The feature id for the '<em><b>Probability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__PROBABILITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 11;

    /**
     * The feature id for the '<em><b>Feared Event</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__FEARED_EVENT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 12;

    /**
     * The feature id for the '<em><b>Failure Mode</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT__FAILURE_MODE = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 13;

    /**
     * The number of structural features of the '<em>SFailure Event</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 14;

    /**
     * The number of operations of the '<em>SFailure Event</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_EVENT_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect
     * <em>SEffect</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSEffect()
     * @generated
     */
    int SEFFECT = 3;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Risks List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__RISKS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Severity</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__SEVERITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Cost</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__COST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Criticality</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__CRITICALITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Failure Events List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT__FAILURE_EVENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 5;

    /**
     * The number of structural features of the '<em>SEffect</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 6;

    /**
     * The number of operations of the '<em>SEffect</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SEFFECT_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk <em>SRisk</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSRisk()
     * @generated
     */
    int SRISK = 4;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Hazard</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__HAZARD = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Failure Event</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__FAILURE_EVENT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Causes List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__CAUSES_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Effects List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK__EFFECTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>SRisk</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 5;

    /**
     * The number of operations of the '<em>SRisk</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRISK_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SHazard
     * <em>SHazard</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SHazard
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSHazard()
     * @generated
     */
    int SHAZARD = 5;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Risk</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD__RISK = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>SHazard</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>SHazard</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHAZARD_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause <em>SCause</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSCause()
     * @generated
     */
    int SCAUSE = 6;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Failure Events List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__FAILURE_EVENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Occurence</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__OCCURENCE = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Detectability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__DETECTABILITY = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Risks List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE__RISKS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>SCause</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 5;

    /**
     * The number of operations of the '<em>SCause</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCAUSE_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureState
     * <em>SFailure State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureState
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureState()
     * @generated
     */
    int SFAILURE_STATE = 7;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__UUID = ABSTRACT_SDYSFUNCTION_OBJECT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__NAME = ABSTRACT_SDYSFUNCTION_OBJECT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__DESCRIPTION = ABSTRACT_SDYSFUNCTION_OBJECT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SDYSFUNCTION_OBJECT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__BASE_CLASS = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Failure Mode</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE__FAILURE_MODE = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>SFailure State</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE_FEATURE_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>SFailure State</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SFAILURE_STATE_OPERATION_COUNT = ABSTRACT_SDYSFUNCTION_OBJECT_OPERATION_COUNT + 0;

    /**
     * Returns the meta object for class
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject <em>Abstract SDysfunction
     * Object</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SDysfunction Object</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject
     * @generated
     */
    EClass getAbstractSDysfunctionObject();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode
     * <em>SFailure Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SFailure Mode</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode
     * @generated
     */
    EClass getSFailureMode();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBase_Class()
     * @see #getSFailureMode()
     * @generated
     */
    EReference getSFailureMode_Base_Class();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent <em>Failure Event</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Failure Event</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent()
     * @see #getSFailureMode()
     * @generated
     */
    EReference getSFailureMode_FailureEvent();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBarrier <em>Barrier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Barrier</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBarrier()
     * @see #getSFailureMode()
     * @generated
     */
    EReference getSFailureMode_Barrier();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent
     * <em>SFailure Event</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SFailure Event</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent
     * @generated
     */
    EClass getSFailureEvent();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getBase_Class()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Base_Class();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getEffectsList <em>Effects List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Effects List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getEffectsList()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_EffectsList();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk <em>Risk</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Risk</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Risk();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCausesList <em>Causes List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Causes List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCausesList()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_CausesList();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectionsList <em>Detections
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Detections List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectionsList()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_DetectionsList();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRecommendationsList <em>Recommendations
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Recommendations List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRecommendationsList()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_RecommendationsList();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getSeverity <em>Severity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Severity</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getSeverity()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Severity();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCriticality <em>Criticality</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Criticality</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCriticality()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Criticality();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCost <em>Cost</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Cost</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCost()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Cost();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getOccurence <em>Occurence</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Occurence</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getOccurence()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Occurence();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectability <em>Detectability</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Detectability</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectability()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Detectability();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getProbability <em>Probability</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Probability</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getProbability()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_Probability();

    /**
     * Returns the meta object for the attribute
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#isFearedEvent <em>Feared Event</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Feared Event</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#isFearedEvent()
     * @see #getSFailureEvent()
     * @generated
     */
    EAttribute getSFailureEvent_FearedEvent();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode <em>Failure Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Failure Mode</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode()
     * @see #getSFailureEvent()
     * @generated
     */
    EReference getSFailureEvent_FailureMode();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect
     * <em>SEffect</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SEffect</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect
     * @generated
     */
    EClass getSEffect();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getBase_Class()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_Base_Class();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getRisksList <em>Risks List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Risks List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getRisksList()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_RisksList();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getSeverity <em>Severity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Severity</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getSeverity()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_Severity();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCost <em>Cost</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Cost</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCost()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_Cost();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCriticality <em>Criticality</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Criticality</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCriticality()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_Criticality();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getFailureEventsList <em>Failure Events
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Failure Events List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getFailureEventsList()
     * @see #getSEffect()
     * @generated
     */
    EReference getSEffect_FailureEventsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk
     * <em>SRisk</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SRisk</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk
     * @generated
     */
    EClass getSRisk();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getBase_Class()
     * @see #getSRisk()
     * @generated
     */
    EReference getSRisk_Base_Class();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getHazard <em>Hazard</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Hazard</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getHazard()
     * @see #getSRisk()
     * @generated
     */
    EReference getSRisk_Hazard();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent <em>Failure Event</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Failure Event</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent()
     * @see #getSRisk()
     * @generated
     */
    EReference getSRisk_FailureEvent();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getCausesList <em>Causes List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Causes List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getCausesList()
     * @see #getSRisk()
     * @generated
     */
    EReference getSRisk_CausesList();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getEffectsList <em>Effects List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Effects List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getEffectsList()
     * @see #getSRisk()
     * @generated
     */
    EReference getSRisk_EffectsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard
     * <em>SHazard</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SHazard</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard
     * @generated
     */
    EClass getSHazard();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getBase_Class()
     * @see #getSHazard()
     * @generated
     */
    EReference getSHazard_Base_Class();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getRisk <em>Risk</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Risk</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard#getRisk()
     * @see #getSHazard()
     * @generated
     */
    EReference getSHazard_Risk();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause
     * <em>SCause</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SCause</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause
     * @generated
     */
    EClass getSCause();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getBase_Class()
     * @see #getSCause()
     * @generated
     */
    EReference getSCause_Base_Class();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getFailureEventsList <em>Failure Events
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Failure Events List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getFailureEventsList()
     * @see #getSCause()
     * @generated
     */
    EReference getSCause_FailureEventsList();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getOccurence <em>Occurence</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Occurence</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getOccurence()
     * @see #getSCause()
     * @generated
     */
    EReference getSCause_Occurence();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getDetectability <em>Detectability</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Detectability</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getDetectability()
     * @see #getSCause()
     * @generated
     */
    EReference getSCause_Detectability();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getRisksList <em>Risks List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Risks List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getRisksList()
     * @see #getSCause()
     * @generated
     */
    EReference getSCause_RisksList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState
     * <em>SFailure State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SFailure State</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState
     * @generated
     */
    EClass getSFailureState();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getBase_Class()
     * @see #getSFailureState()
     * @generated
     */
    EReference getSFailureState_Base_Class();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getFailureMode <em>Failure Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Failure Mode</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getFailureMode()
     * @see #getSFailureState()
     * @generated
     */
    EReference getSFailureState_FailureMode();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ISDysfunctionsFactory getSDysfunctionsFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the
         * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.AbstractSDysfunctionObject <em>Abstract
         * SDysfunction Object</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.AbstractSDysfunctionObject
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getAbstractSDysfunctionObject()
         * @generated
         */
        EClass ABSTRACT_SDYSFUNCTION_OBJECT = eINSTANCE.getAbstractSDysfunctionObject();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode
         * <em>SFailure Mode</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureMode()
         * @generated
         */
        EClass SFAILURE_MODE = eINSTANCE.getSFailureMode();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_MODE__BASE_CLASS = eINSTANCE.getSFailureMode_Base_Class();

        /**
         * The meta object literal for the '<em><b>Failure Event</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_MODE__FAILURE_EVENT = eINSTANCE.getSFailureMode_FailureEvent();

        /**
         * The meta object literal for the '<em><b>Barrier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_MODE__BARRIER = eINSTANCE.getSFailureMode_Barrier();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent
         * <em>SFailure Event</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureEvent()
         * @generated
         */
        EClass SFAILURE_EVENT = eINSTANCE.getSFailureEvent();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__BASE_CLASS = eINSTANCE.getSFailureEvent_Base_Class();

        /**
         * The meta object literal for the '<em><b>Effects List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__EFFECTS_LIST = eINSTANCE.getSFailureEvent_EffectsList();

        /**
         * The meta object literal for the '<em><b>Risk</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__RISK = eINSTANCE.getSFailureEvent_Risk();

        /**
         * The meta object literal for the '<em><b>Causes List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__CAUSES_LIST = eINSTANCE.getSFailureEvent_CausesList();

        /**
         * The meta object literal for the '<em><b>Detections List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__DETECTIONS_LIST = eINSTANCE.getSFailureEvent_DetectionsList();

        /**
         * The meta object literal for the '<em><b>Recommendations List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__RECOMMENDATIONS_LIST = eINSTANCE.getSFailureEvent_RecommendationsList();

        /**
         * The meta object literal for the '<em><b>Severity</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__SEVERITY = eINSTANCE.getSFailureEvent_Severity();

        /**
         * The meta object literal for the '<em><b>Criticality</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__CRITICALITY = eINSTANCE.getSFailureEvent_Criticality();

        /**
         * The meta object literal for the '<em><b>Cost</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__COST = eINSTANCE.getSFailureEvent_Cost();

        /**
         * The meta object literal for the '<em><b>Occurence</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__OCCURENCE = eINSTANCE.getSFailureEvent_Occurence();

        /**
         * The meta object literal for the '<em><b>Detectability</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__DETECTABILITY = eINSTANCE.getSFailureEvent_Detectability();

        /**
         * The meta object literal for the '<em><b>Probability</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__PROBABILITY = eINSTANCE.getSFailureEvent_Probability();

        /**
         * The meta object literal for the '<em><b>Feared Event</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SFAILURE_EVENT__FEARED_EVENT = eINSTANCE.getSFailureEvent_FearedEvent();

        /**
         * The meta object literal for the '<em><b>Failure Mode</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_EVENT__FAILURE_MODE = eINSTANCE.getSFailureEvent_FailureMode();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect
         * <em>SEffect</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSEffect()
         * @generated
         */
        EClass SEFFECT = eINSTANCE.getSEffect();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__BASE_CLASS = eINSTANCE.getSEffect_Base_Class();

        /**
         * The meta object literal for the '<em><b>Risks List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__RISKS_LIST = eINSTANCE.getSEffect_RisksList();

        /**
         * The meta object literal for the '<em><b>Severity</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__SEVERITY = eINSTANCE.getSEffect_Severity();

        /**
         * The meta object literal for the '<em><b>Cost</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__COST = eINSTANCE.getSEffect_Cost();

        /**
         * The meta object literal for the '<em><b>Criticality</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__CRITICALITY = eINSTANCE.getSEffect_Criticality();

        /**
         * The meta object literal for the '<em><b>Failure Events List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SEFFECT__FAILURE_EVENTS_LIST = eINSTANCE.getSEffect_FailureEventsList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk
         * <em>SRisk</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSRisk()
         * @generated
         */
        EClass SRISK = eINSTANCE.getSRisk();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRISK__BASE_CLASS = eINSTANCE.getSRisk_Base_Class();

        /**
         * The meta object literal for the '<em><b>Hazard</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRISK__HAZARD = eINSTANCE.getSRisk_Hazard();

        /**
         * The meta object literal for the '<em><b>Failure Event</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRISK__FAILURE_EVENT = eINSTANCE.getSRisk_FailureEvent();

        /**
         * The meta object literal for the '<em><b>Causes List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRISK__CAUSES_LIST = eINSTANCE.getSRisk_CausesList();

        /**
         * The meta object literal for the '<em><b>Effects List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRISK__EFFECTS_LIST = eINSTANCE.getSRisk_EffectsList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SHazard
         * <em>SHazard</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SHazard
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSHazard()
         * @generated
         */
        EClass SHAZARD = eINSTANCE.getSHazard();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SHAZARD__BASE_CLASS = eINSTANCE.getSHazard_Base_Class();

        /**
         * The meta object literal for the '<em><b>Risk</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SHAZARD__RISK = eINSTANCE.getSHazard_Risk();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause
         * <em>SCause</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSCause()
         * @generated
         */
        EClass SCAUSE = eINSTANCE.getSCause();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCAUSE__BASE_CLASS = eINSTANCE.getSCause_Base_Class();

        /**
         * The meta object literal for the '<em><b>Failure Events List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCAUSE__FAILURE_EVENTS_LIST = eINSTANCE.getSCause_FailureEventsList();

        /**
         * The meta object literal for the '<em><b>Occurence</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCAUSE__OCCURENCE = eINSTANCE.getSCause_Occurence();

        /**
         * The meta object literal for the '<em><b>Detectability</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCAUSE__DETECTABILITY = eINSTANCE.getSCause_Detectability();

        /**
         * The meta object literal for the '<em><b>Risks List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCAUSE__RISKS_LIST = eINSTANCE.getSCause_RisksList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureState
         * <em>SFailure State</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureState
         * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage#getSFailureState()
         * @generated
         */
        EClass SFAILURE_STATE = eINSTANCE.getSFailureState();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_STATE__BASE_CLASS = eINSTANCE.getSFailureState_Base_Class();

        /**
         * The meta object literal for the '<em><b>Failure Mode</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SFAILURE_STATE__FAILURE_MODE = eINSTANCE.getSFailureState_FailureMode();

    }

} // ISDysfunctionsPackage
