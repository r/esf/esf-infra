/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsFactory;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ESFSafetyConceptsFactory
    extends EFactoryImpl
    implements IESFSafetyConceptsFactory {

    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public static IESFSafetyConceptsFactory init() {
        try {
            IESFSafetyConceptsFactory theESFSafetyConceptsFactory =
                (IESFSafetyConceptsFactory) EPackage.Registry.INSTANCE.getEFactory(IESFSafetyConceptsPackage.eNS_URI);
            if (theESFSafetyConceptsFactory != null) {
                return theESFSafetyConceptsFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ESFSafetyConceptsFactory();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public ESFSafetyConceptsFactory() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case IESFSafetyConceptsPackage.SSAFETY_ARTIFACTS:
                return createSSafetyArtifacts();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public ISSafetyArtifacts createSSafetyArtifacts() {
        SSafetyArtifacts sSafetyArtifacts = new SSafetyArtifacts();
        return sSafetyArtifacts;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public IESFSafetyConceptsPackage getESFSafetyConceptsPackage() {
        return (IESFSafetyConceptsPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @deprecated
     * @generated
     */
    @Deprecated
    public static IESFSafetyConceptsPackage getPackage() {
        return IESFSafetyConceptsPackage.eINSTANCE;
    }

} // ESFSafetyConceptsFactory
