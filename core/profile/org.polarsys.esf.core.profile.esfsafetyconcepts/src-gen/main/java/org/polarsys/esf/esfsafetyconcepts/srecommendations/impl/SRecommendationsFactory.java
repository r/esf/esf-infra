/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsFactory;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class SRecommendationsFactory
    extends EFactoryImpl
    implements ISRecommendationsFactory {

    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static ISRecommendationsFactory init() {
        try {
            ISRecommendationsFactory theSRecommendationsFactory =
                (ISRecommendationsFactory) EPackage.Registry.INSTANCE.getEFactory(ISRecommendationsPackage.eNS_URI);
            if (theSRecommendationsFactory != null) {
                return theSRecommendationsFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new SRecommendationsFactory();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SRecommendationsFactory() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case ISRecommendationsPackage.SBARRIER:
                return createSBarrier();
            case ISRecommendationsPackage.SRECOMMENDATION:
                return createSRecommendation();
            case ISRecommendationsPackage.SACTION:
                return createSAction();
            case ISRecommendationsPackage.SDETECTION:
                return createSDetection();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case ISRecommendationsPackage.SACTIONS_KIND:
                return createSActionsKindFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException(
                    "The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case ISRecommendationsPackage.SACTIONS_KIND:
                return convertSActionsKindToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException(
                    "The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISBarrier createSBarrier() {
        SBarrier sBarrier = new SBarrier();
        return sBarrier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRecommendation createSRecommendation() {
        SRecommendation sRecommendation = new SRecommendation();
        return sRecommendation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISAction createSAction() {
        SAction sAction = new SAction();
        return sAction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetection createSDetection() {
        SDetection sDetection = new SDetection();
        return sDetection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SActionsKind createSActionsKindFromString(EDataType eDataType, String initialValue) {
        SActionsKind result = SActionsKind.get(initialValue);
        if (result == null)
            throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertSActionsKindToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRecommendationsPackage getSRecommendationsPackage() {
        return (ISRecommendationsPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static ISRecommendationsPackage getPackage() {
        return ISRecommendationsPackage.eINSTANCE;
    }

} // SRecommendationsFactory
