/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Classifier;
import org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SDetection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SDetection#getBase_Classifier <em>Base
 * Classifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SDetection
    extends AbstractSSafetyConcept
    implements ISDetection {

    /**
     * The cached value of the '{@link #getBase_Classifier() <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Classifier()
     * @generated
     * @ordered
     */
    protected Classifier base_Classifier;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SDetection() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISRecommendationsPackage.Literals.SDETECTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Classifier getBase_Classifier() {
        if (base_Classifier != null && base_Classifier.eIsProxy()) {
            InternalEObject oldBase_Classifier = (InternalEObject) base_Classifier;
            base_Classifier = (Classifier) eResolveProxy(oldBase_Classifier);
            if (base_Classifier != oldBase_Classifier) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER,
                            oldBase_Classifier,
                            base_Classifier));
            }
        }
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Classifier basicGetBase_Classifier() {
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Classifier(Classifier newBase_Classifier) {
        Classifier oldBase_Classifier = base_Classifier;
        base_Classifier = newBase_Classifier;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER,
                    oldBase_Classifier,
                    base_Classifier));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER:
                if (resolve)
                    return getBase_Classifier();
                return basicGetBase_Classifier();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER:
                setBase_Classifier((Classifier) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER:
                setBase_Classifier((Classifier) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SDETECTION__BASE_CLASSIFIER:
                return base_Classifier != null;
        }
        return super.eIsSet(featureID);
    }

} // SDetection
