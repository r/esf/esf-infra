/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='SRecommendations'"
 * @generated
 */
public interface ISRecommendationsPackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "srecommendations"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFSafetyConcepts/SRecommendations"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "SRecommendations"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    ISRecommendationsPackage eINSTANCE =
        org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage.init();

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendation
     * <em>SRecommendation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendation
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSRecommendation()
     * @generated
     */
    int SRECOMMENDATION = 1;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION__UUID = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION__NAME = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION__SARCHITECTURE_ELEMENTS_LIST =
        IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION__BASE_CLASSIFIER = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SRecommendation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SRecommendation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SRECOMMENDATION_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier
     * <em>SBarrier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSBarrier()
     * @generated
     */
    int SBARRIER = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__UUID = SRECOMMENDATION__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__NAME = SRECOMMENDATION__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__DESCRIPTION = SRECOMMENDATION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__SARCHITECTURE_ELEMENTS_LIST = SRECOMMENDATION__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__BASE_CLASSIFIER = SRECOMMENDATION__BASE_CLASSIFIER;

    /**
     * The feature id for the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__BASE_CLASS = SRECOMMENDATION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>SActions List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER__SACTIONS_LIST = SRECOMMENDATION_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>SBarrier</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER_FEATURE_COUNT = SRECOMMENDATION_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>SBarrier</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SBARRIER_OPERATION_COUNT = SRECOMMENDATION_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction
     * <em>SAction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSAction()
     * @generated
     */
    int SACTION = 2;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__UUID = SRECOMMENDATION__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__NAME = SRECOMMENDATION__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__DESCRIPTION = SRECOMMENDATION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__SARCHITECTURE_ELEMENTS_LIST = SRECOMMENDATION__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__BASE_CLASSIFIER = SRECOMMENDATION__BASE_CLASSIFIER;

    /**
     * The feature id for the '<em><b>Kind</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__KIND = SRECOMMENDATION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>SBarriers List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION__SBARRIERS_LIST = SRECOMMENDATION_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>SAction</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION_FEATURE_COUNT = SRECOMMENDATION_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>SAction</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SACTION_OPERATION_COUNT = SRECOMMENDATION_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SDetection
     * <em>SDetection</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SDetection
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSDetection()
     * @generated
     */
    int SDETECTION = 3;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION__UUID = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION__NAME = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION__DESCRIPTION = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION__SARCHITECTURE_ELEMENTS_LIST = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION__BASE_CLASSIFIER = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SDetection</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SDetection</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTION_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind <em>SActions
     * Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSActionsKind()
     * @generated
     */
    int SACTIONS_KIND = 4;

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier
     * <em>SBarrier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SBarrier</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier
     * @generated
     */
    EClass getSBarrier();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getBase_Class <em>Base Class</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Class</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getBase_Class()
     * @see #getSBarrier()
     * @generated
     */
    EReference getSBarrier_Base_Class();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getSActionsList <em>SActions List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SActions List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getSActionsList()
     * @see #getSBarrier()
     * @generated
     */
    EReference getSBarrier_SActionsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation
     * <em>SRecommendation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SRecommendation</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation
     * @generated
     */
    EClass getSRecommendation();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation#getBase_Classifier <em>Base
     * Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Classifier</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation#getBase_Classifier()
     * @see #getSRecommendation()
     * @generated
     */
    EReference getSRecommendation_Base_Classifier();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction
     * <em>SAction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SAction</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction
     * @generated
     */
    EClass getSAction();

    /**
     * Returns the meta object for the attribute
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getKind <em>Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Kind</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getKind()
     * @see #getSAction()
     * @generated
     */
    EAttribute getSAction_Kind();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getSBarriersList <em>SBarriers List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SBarriers List</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getSBarriersList()
     * @see #getSAction()
     * @generated
     */
    EReference getSAction_SBarriersList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection
     * <em>SDetection</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SDetection</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection
     * @generated
     */
    EClass getSDetection();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection#getBase_Classifier <em>Base
     * Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Classifier</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection#getBase_Classifier()
     * @see #getSDetection()
     * @generated
     */
    EReference getSDetection_Base_Classifier();

    /**
     * Returns the meta object for enum '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
     * <em>SActions Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>SActions Kind</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
     * @generated
     */
    EEnum getSActionsKind();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ISRecommendationsFactory getSRecommendationsFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier
         * <em>SBarrier</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSBarrier()
         * @generated
         */
        EClass SBARRIER = eINSTANCE.getSBarrier();

        /**
         * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SBARRIER__BASE_CLASS = eINSTANCE.getSBarrier_Base_Class();

        /**
         * The meta object literal for the '<em><b>SActions List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SBARRIER__SACTIONS_LIST = eINSTANCE.getSBarrier_SActionsList();

        /**
         * The meta object literal for the
         * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendation <em>SRecommendation</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendation
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSRecommendation()
         * @generated
         */
        EClass SRECOMMENDATION = eINSTANCE.getSRecommendation();

        /**
         * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SRECOMMENDATION__BASE_CLASSIFIER = eINSTANCE.getSRecommendation_Base_Classifier();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction
         * <em>SAction</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSAction()
         * @generated
         */
        EClass SACTION = eINSTANCE.getSAction();

        /**
         * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SACTION__KIND = eINSTANCE.getSAction_Kind();

        /**
         * The meta object literal for the '<em><b>SBarriers List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SACTION__SBARRIERS_LIST = eINSTANCE.getSAction_SBarriersList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SDetection
         * <em>SDetection</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SDetection
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSDetection()
         * @generated
         */
        EClass SDETECTION = eINSTANCE.getSDetection();

        /**
         * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SDETECTION__BASE_CLASSIFIER = eINSTANCE.getSDetection_Base_Classifier();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
         * <em>SActions Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
         * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage#getSActionsKind()
         * @generated
         */
        EEnum SACTIONS_KIND = eINSTANCE.getSActionsKind();

    }

} // ISRecommendationsPackage
