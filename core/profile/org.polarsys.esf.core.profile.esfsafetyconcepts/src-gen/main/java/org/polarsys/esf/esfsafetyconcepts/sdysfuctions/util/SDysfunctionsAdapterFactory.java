/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage
 * @generated
 */
public class SDysfunctionsAdapterFactory
    extends AdapterFactoryImpl {

    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ISDysfunctionsPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SDysfunctionsAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = ISDysfunctionsPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance
     * object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SDysfunctionsSwitch<Adapter> modelSwitch = new SDysfunctionsSwitch<Adapter>() {

        @Override
        public Adapter caseAbstractSDysfunctionObject(IAbstractSDysfunctionObject object) {
            return createAbstractSDysfunctionObjectAdapter();
        }

        @Override
        public Adapter caseSFailureMode(ISFailureMode object) {
            return createSFailureModeAdapter();
        }

        @Override
        public Adapter caseSFailureEvent(ISFailureEvent object) {
            return createSFailureEventAdapter();
        }

        @Override
        public Adapter caseSEffect(ISEffect object) {
            return createSEffectAdapter();
        }

        @Override
        public Adapter caseSRisk(ISRisk object) {
            return createSRiskAdapter();
        }

        @Override
        public Adapter caseSHazard(ISHazard object) {
            return createSHazardAdapter();
        }

        @Override
        public Adapter caseSCause(ISCause object) {
            return createSCauseAdapter();
        }

        @Override
        public Adapter caseSFailureState(ISFailureState object) {
            return createSFailureStateAdapter();
        }

        @Override
        public Adapter caseAbstractSElement(IAbstractSElement object) {
            return createAbstractSElementAdapter();
        }

        @Override
        public Adapter caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
            return createAbstractSSafetyConceptAdapter();
        }

        @Override
        public Adapter defaultCase(EObject object) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject <em>Abstract SDysfunction
     * Object</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject
     * @generated
     */
    public Adapter createAbstractSDysfunctionObjectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode <em>SFailure Mode</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode
     * @generated
     */
    public Adapter createSFailureModeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent <em>SFailure Event</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent
     * @generated
     */
    public Adapter createSFailureEventAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect
     * <em>SEffect</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect
     * @generated
     */
    public Adapter createSEffectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk
     * <em>SRisk</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk
     * @generated
     */
    public Adapter createSRiskAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard
     * <em>SHazard</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard
     * @generated
     */
    public Adapter createSHazardAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause
     * <em>SCause</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause
     * @generated
     */
    public Adapter createSCauseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState <em>SFailure State</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState
     * @generated
     */
    public Adapter createSFailureStateAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement <em>Abstract
     * SElement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSElement
     * @generated
     */
    public Adapter createAbstractSElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept
     * <em>Abstract SSafety Concept</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyConcept
     * @generated
     */
    public Adapter createAbstractSSafetyConceptAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // SDysfunctionsAdapterFactory
