/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SBarrier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SBarrier#getSActionsList <em>SActions
 * List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SBarrier
    extends SRecommendation
    implements ISBarrier {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getSActionsList() <em>SActions List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSActionsList()
     * @generated
     * @ordered
     */
    protected EList<ISAction> sActionsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SBarrier() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISRecommendationsPackage.Literals.SBARRIER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISRecommendationsPackage.SBARRIER__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISRecommendationsPackage.SBARRIER__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISAction> getSActionsList() {
        if (sActionsList == null) {
            sActionsList = new EObjectWithInverseResolvingEList.ManyInverse<ISAction>(
                ISAction.class,
                this,
                ISRecommendationsPackage.SBARRIER__SACTIONS_LIST,
                ISRecommendationsPackage.SACTION__SBARRIERS_LIST);
        }
        return sActionsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getSActionsList()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                return ((InternalEList<?>) getSActionsList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                return getSActionsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                getSActionsList().clear();
                getSActionsList().addAll((Collection<? extends ISAction>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                getSActionsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SBARRIER__BASE_CLASS:
                return base_Class != null;
            case ISRecommendationsPackage.SBARRIER__SACTIONS_LIST:
                return sActionsList != null && !sActionsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // SBarrier
