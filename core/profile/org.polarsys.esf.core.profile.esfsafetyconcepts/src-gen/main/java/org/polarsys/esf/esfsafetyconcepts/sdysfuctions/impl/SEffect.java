/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfproperties.ISCost;
import org.polarsys.esf.esfproperties.ISCriticality;
import org.polarsys.esf.esfproperties.ISSeverity;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SEffect</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getRisksList <em>Risks List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getSeverity <em>Severity</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getCost <em>Cost</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getCriticality <em>Criticality</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SEffect#getFailureEventsList <em>Failure Events
 * List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SEffect
    extends AbstractSDysfunctionObject
    implements ISEffect {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getRisksList() <em>Risks List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRisksList()
     * @generated
     * @ordered
     */
    protected EList<ISRisk> risksList;

    /**
     * The cached value of the '{@link #getSeverity() <em>Severity</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSeverity()
     * @generated
     * @ordered
     */
    protected ISSeverity severity;

    /**
     * The cached value of the '{@link #getCost() <em>Cost</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCost()
     * @generated
     * @ordered
     */
    protected ISCost cost;

    /**
     * The cached value of the '{@link #getCriticality() <em>Criticality</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCriticality()
     * @generated
     * @ordered
     */
    protected ISCriticality criticality;

    /**
     * The cached value of the '{@link #getFailureEventsList() <em>Failure Events List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFailureEventsList()
     * @generated
     * @ordered
     */
    protected EList<ISFailureEvent> failureEventsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SEffect() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISDysfunctionsPackage.Literals.SEFFECT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SEFFECT__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SEFFECT__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISRisk> getRisksList() {
        if (risksList == null) {
            risksList = new EObjectWithInverseResolvingEList.ManyInverse<ISRisk>(
                ISRisk.class,
                this,
                ISDysfunctionsPackage.SEFFECT__RISKS_LIST,
                ISDysfunctionsPackage.SRISK__EFFECTS_LIST);
        }
        return risksList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISSeverity getSeverity() {
        if (severity != null && severity.eIsProxy()) {
            InternalEObject oldSeverity = (InternalEObject) severity;
            severity = (ISSeverity) eResolveProxy(oldSeverity);
            if (severity != oldSeverity) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SEFFECT__SEVERITY,
                            oldSeverity,
                            severity));
            }
        }
        return severity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISSeverity basicGetSeverity() {
        return severity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setSeverity(ISSeverity newSeverity) {
        ISSeverity oldSeverity = severity;
        severity = newSeverity;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SEFFECT__SEVERITY,
                    oldSeverity,
                    severity));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCost getCost() {
        if (cost != null && cost.eIsProxy()) {
            InternalEObject oldCost = (InternalEObject) cost;
            cost = (ISCost) eResolveProxy(oldCost);
            if (cost != oldCost) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SEFFECT__COST,
                            oldCost,
                            cost));
            }
        }
        return cost;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCost basicGetCost() {
        return cost;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setCost(ISCost newCost) {
        ISCost oldCost = cost;
        cost = newCost;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ISDysfunctionsPackage.SEFFECT__COST, oldCost, cost));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCriticality getCriticality() {
        if (criticality != null && criticality.eIsProxy()) {
            InternalEObject oldCriticality = (InternalEObject) criticality;
            criticality = (ISCriticality) eResolveProxy(oldCriticality);
            if (criticality != oldCriticality) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SEFFECT__CRITICALITY,
                            oldCriticality,
                            criticality));
            }
        }
        return criticality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCriticality basicGetCriticality() {
        return criticality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setCriticality(ISCriticality newCriticality) {
        ISCriticality oldCriticality = criticality;
        criticality = newCriticality;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SEFFECT__CRITICALITY,
                    oldCriticality,
                    criticality));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISFailureEvent> getFailureEventsList() {
        if (failureEventsList == null) {
            failureEventsList = new EObjectWithInverseResolvingEList.ManyInverse<ISFailureEvent>(
                ISFailureEvent.class,
                this,
                ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST,
                ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST);
        }
        return failureEventsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getRisksList()).basicAdd(otherEnd, msgs);
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getFailureEventsList())
                    .basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                return ((InternalEList<?>) getRisksList()).basicRemove(otherEnd, msgs);
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                return ((InternalEList<?>) getFailureEventsList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                return getRisksList();
            case ISDysfunctionsPackage.SEFFECT__SEVERITY:
                if (resolve)
                    return getSeverity();
                return basicGetSeverity();
            case ISDysfunctionsPackage.SEFFECT__COST:
                if (resolve)
                    return getCost();
                return basicGetCost();
            case ISDysfunctionsPackage.SEFFECT__CRITICALITY:
                if (resolve)
                    return getCriticality();
                return basicGetCriticality();
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                return getFailureEventsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                getRisksList().clear();
                getRisksList().addAll((Collection<? extends ISRisk>) newValue);
                return;
            case ISDysfunctionsPackage.SEFFECT__SEVERITY:
                setSeverity((ISSeverity) newValue);
                return;
            case ISDysfunctionsPackage.SEFFECT__COST:
                setCost((ISCost) newValue);
                return;
            case ISDysfunctionsPackage.SEFFECT__CRITICALITY:
                setCriticality((ISCriticality) newValue);
                return;
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                getFailureEventsList().clear();
                getFailureEventsList().addAll((Collection<? extends ISFailureEvent>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                getRisksList().clear();
                return;
            case ISDysfunctionsPackage.SEFFECT__SEVERITY:
                setSeverity((ISSeverity) null);
                return;
            case ISDysfunctionsPackage.SEFFECT__COST:
                setCost((ISCost) null);
                return;
            case ISDysfunctionsPackage.SEFFECT__CRITICALITY:
                setCriticality((ISCriticality) null);
                return;
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                getFailureEventsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SEFFECT__BASE_CLASS:
                return base_Class != null;
            case ISDysfunctionsPackage.SEFFECT__RISKS_LIST:
                return risksList != null && !risksList.isEmpty();
            case ISDysfunctionsPackage.SEFFECT__SEVERITY:
                return severity != null;
            case ISDysfunctionsPackage.SEFFECT__COST:
                return cost != null;
            case ISDysfunctionsPackage.SEFFECT__CRITICALITY:
                return criticality != null;
            case ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST:
                return failureEventsList != null && !failureEventsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // SEffect
