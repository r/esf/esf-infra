/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 *
 * @see org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFSafetyConcepts'"
 * @generated
 */
public interface IESFSafetyConceptsPackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    String eNAME = "esfsafetyconcepts"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFSafetyConcepts"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    String eNS_PREFIX = "ESFSafetyConcepts"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    IESFSafetyConceptsPackage eINSTANCE = org.polarsys.esf.esfsafetyconcepts.impl.ESFSafetyConceptsPackage.init();

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfsafetyconcepts.impl.SSafetyArtifacts <em>SSafety
     * Artifacts</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @see org.polarsys.esf.esfsafetyconcepts.impl.SSafetyArtifacts
     * @see org.polarsys.esf.esfsafetyconcepts.impl.ESFSafetyConceptsPackage#getSSafetyArtifacts()
     * @generated
     */
    int SSAFETY_ARTIFACTS = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS__UUID = IESFCorePackage.ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS__NAME = IESFCorePackage.ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS__DESCRIPTION = IESFCorePackage.ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Package</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS__BASE_PACKAGE = IESFCorePackage.ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SSafety Artifacts</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SSafety Artifacts</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     * @ordered
     */
    int SSAFETY_ARTIFACTS_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts <em>SSafety
     * Artifacts</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @return the meta object for class '<em>SSafety Artifacts</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts
     * @generated
     */
    EClass getSSafetyArtifacts();

    /**
     * Returns the meta object for the reference
     * '{@link org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts#getBase_Package <em>Base Package</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @return the meta object for the reference '<em>Base Package</em>'.
     * @see org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts#getBase_Package()
     * @see #getSSafetyArtifacts()
     * @generated
     */
    EReference getSSafetyArtifacts_Base_Package();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @return the factory that creates the instances of the model.
     * @generated
     */
    IESFSafetyConceptsFactory getESFSafetyConceptsFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     *
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfsafetyconcepts.impl.SSafetyArtifacts <em>SSafety
         * Artifacts</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         *
         * @see org.polarsys.esf.esfsafetyconcepts.impl.SSafetyArtifacts
         * @see org.polarsys.esf.esfsafetyconcepts.impl.ESFSafetyConceptsPackage#getSSafetyArtifacts()
         * @generated
         */
        EClass SSAFETY_ARTIFACTS = eINSTANCE.getSSafetyArtifacts();

        /**
         * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         *
         * @generated
         */
        EReference SSAFETY_ARTIFACTS__BASE_PACKAGE = eINSTANCE.getSSafetyArtifacts_Base_Package();

    }

} // IESFSafetyConceptsPackage
