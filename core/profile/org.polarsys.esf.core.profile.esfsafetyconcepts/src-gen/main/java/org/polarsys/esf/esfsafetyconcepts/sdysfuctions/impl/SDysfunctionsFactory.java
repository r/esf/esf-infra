/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsFactory;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class SDysfunctionsFactory
    extends EFactoryImpl
    implements ISDysfunctionsFactory {

    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static ISDysfunctionsFactory init() {
        try {
            ISDysfunctionsFactory theSDysfunctionsFactory =
                (ISDysfunctionsFactory) EPackage.Registry.INSTANCE.getEFactory(ISDysfunctionsPackage.eNS_URI);
            if (theSDysfunctionsFactory != null) {
                return theSDysfunctionsFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new SDysfunctionsFactory();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SDysfunctionsFactory() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case ISDysfunctionsPackage.SFAILURE_MODE:
                return createSFailureMode();
            case ISDysfunctionsPackage.SFAILURE_EVENT:
                return createSFailureEvent();
            case ISDysfunctionsPackage.SEFFECT:
                return createSEffect();
            case ISDysfunctionsPackage.SRISK:
                return createSRisk();
            case ISDysfunctionsPackage.SHAZARD:
                return createSHazard();
            case ISDysfunctionsPackage.SCAUSE:
                return createSCause();
            case ISDysfunctionsPackage.SFAILURE_STATE:
                return createSFailureState();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureMode createSFailureMode() {
        SFailureMode sFailureMode = new SFailureMode();
        return sFailureMode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureEvent createSFailureEvent() {
        SFailureEvent sFailureEvent = new SFailureEvent();
        return sFailureEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISEffect createSEffect() {
        SEffect sEffect = new SEffect();
        return sEffect;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRisk createSRisk() {
        SRisk sRisk = new SRisk();
        return sRisk;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISHazard createSHazard() {
        SHazard sHazard = new SHazard();
        return sHazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCause createSCause() {
        SCause sCause = new SCause();
        return sCause;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureState createSFailureState() {
        SFailureState sFailureState = new SFailureState();
        return sFailureState;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDysfunctionsPackage getSDysfunctionsPackage() {
        return (ISDysfunctionsPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static ISDysfunctionsPackage getPackage() {
        return ISDysfunctionsPackage.eINSTANCE;
    }

} // SDysfunctionsFactory
