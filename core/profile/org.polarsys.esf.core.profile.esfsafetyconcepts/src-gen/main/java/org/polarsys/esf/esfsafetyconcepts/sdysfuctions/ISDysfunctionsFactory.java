/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage
 * @generated
 */
public interface ISDysfunctionsFactory
    extends EFactory {

    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    ISDysfunctionsFactory eINSTANCE = org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsFactory.init();

    /**
     * Returns a new object of class '<em>SFailure Mode</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SFailure Mode</em>'.
     * @generated
     */
    ISFailureMode createSFailureMode();

    /**
     * Returns a new object of class '<em>SFailure Event</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SFailure Event</em>'.
     * @generated
     */
    ISFailureEvent createSFailureEvent();

    /**
     * Returns a new object of class '<em>SEffect</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SEffect</em>'.
     * @generated
     */
    ISEffect createSEffect();

    /**
     * Returns a new object of class '<em>SRisk</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SRisk</em>'.
     * @generated
     */
    ISRisk createSRisk();

    /**
     * Returns a new object of class '<em>SHazard</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SHazard</em>'.
     * @generated
     */
    ISHazard createSHazard();

    /**
     * Returns a new object of class '<em>SCause</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SCause</em>'.
     * @generated
     */
    ISCause createSCause();

    /**
     * Returns a new object of class '<em>SFailure State</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SFailure State</em>'.
     * @generated
     */
    ISFailureState createSFailureState();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    ISDysfunctionsPackage getSDysfunctionsPackage();

} // ISDysfunctionsFactory
