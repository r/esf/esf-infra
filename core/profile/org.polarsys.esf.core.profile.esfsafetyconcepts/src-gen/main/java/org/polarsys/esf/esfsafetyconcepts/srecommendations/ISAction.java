/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SAction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getKind <em>Kind</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getSBarriersList <em>SBarriers
 * List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSAction()
 * @model
 * @generated
 */
public interface ISAction
    extends ISRecommendation {

    /**
     * Returns the value of the '<em><b>Kind</b></em>' attribute.
     * The literals are from the enumeration {@link org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Kind</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Kind</em>' attribute.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
     * @see #setKind(SActionsKind)
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSAction_Kind()
     * @model required="true" ordered="false"
     * @generated
     */
    SActionsKind getKind();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getKind
     * <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Kind</em>' attribute.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind
     * @see #getKind()
     * @generated
     */
    void setKind(SActionsKind value);

    /**
     * Returns the value of the '<em><b>SBarriers List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getSActionsList <em>SActions List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>SBarriers List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>SBarriers List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSAction_SBarriersList()
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getSActionsList
     * @model opposite="sActionsList" ordered="false"
     * @generated
     */
    EList<ISBarrier> getSBarriersList();

} // ISAction
