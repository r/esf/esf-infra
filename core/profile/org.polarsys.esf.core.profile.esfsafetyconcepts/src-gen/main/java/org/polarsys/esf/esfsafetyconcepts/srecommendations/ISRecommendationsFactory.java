/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage
 * @generated
 */
public interface ISRecommendationsFactory
    extends EFactory {

    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    ISRecommendationsFactory eINSTANCE =
        org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsFactory.init();

    /**
     * Returns a new object of class '<em>SBarrier</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SBarrier</em>'.
     * @generated
     */
    ISBarrier createSBarrier();

    /**
     * Returns a new object of class '<em>SRecommendation</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SRecommendation</em>'.
     * @generated
     */
    ISRecommendation createSRecommendation();

    /**
     * Returns a new object of class '<em>SAction</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SAction</em>'.
     * @generated
     */
    ISAction createSAction();

    /**
     * Returns a new object of class '<em>SDetection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SDetection</em>'.
     * @generated
     */
    ISDetection createSDetection();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    ISRecommendationsPackage getSRecommendationsPackage();

} // ISRecommendationsFactory
