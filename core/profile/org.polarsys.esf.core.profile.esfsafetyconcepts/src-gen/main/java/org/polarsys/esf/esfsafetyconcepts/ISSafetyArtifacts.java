/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts;

import org.polarsys.esf.esfcore.IAbstractSElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SSafety Artifacts</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts#getBase_Package <em>Base Package</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage#getSSafetyArtifacts()
 * @model
 * @generated
 */
public interface ISSafetyArtifacts
    extends IAbstractSElement {

    /**
     * Returns the value of the '<em><b>Base Package</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Package</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Base Package</em>' reference.
     * @see #setBase_Package(org.eclipse.uml2.uml.Package)
     * @see org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage#getSSafetyArtifacts_Base_Package()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Package getBase_Package();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts#getBase_Package <em>Base
     * Package</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @param value the new value of the '<em>Base Package</em>' reference.
     * @see #getBase_Package()
     * @generated
     */
    void setBase_Package(org.eclipse.uml2.uml.Package value);

} // ISSafetyArtifacts
