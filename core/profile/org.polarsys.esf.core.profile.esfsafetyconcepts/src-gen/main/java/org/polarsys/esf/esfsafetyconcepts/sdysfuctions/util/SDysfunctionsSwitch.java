/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.IAbstractSDysfunctionObject;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage
 * @generated
 */
public class SDysfunctionsSwitch<T>
    extends Switch<T> {

    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ISDysfunctionsPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SDysfunctionsSwitch() {
        if (modelPackage == null) {
            modelPackage = ISDysfunctionsPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
     * result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case ISDysfunctionsPackage.ABSTRACT_SDYSFUNCTION_OBJECT: {
                IAbstractSDysfunctionObject abstractSDysfunctionObject = (IAbstractSDysfunctionObject) theEObject;
                T result = caseAbstractSDysfunctionObject(abstractSDysfunctionObject);
                if (result == null)
                    result = caseAbstractSSafetyConcept(abstractSDysfunctionObject);
                if (result == null)
                    result = caseAbstractSElement(abstractSDysfunctionObject);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SFAILURE_MODE: {
                ISFailureMode sFailureMode = (ISFailureMode) theEObject;
                T result = caseSFailureMode(sFailureMode);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sFailureMode);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sFailureMode);
                if (result == null)
                    result = caseAbstractSElement(sFailureMode);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SFAILURE_EVENT: {
                ISFailureEvent sFailureEvent = (ISFailureEvent) theEObject;
                T result = caseSFailureEvent(sFailureEvent);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sFailureEvent);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sFailureEvent);
                if (result == null)
                    result = caseAbstractSElement(sFailureEvent);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SEFFECT: {
                ISEffect sEffect = (ISEffect) theEObject;
                T result = caseSEffect(sEffect);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sEffect);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sEffect);
                if (result == null)
                    result = caseAbstractSElement(sEffect);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SRISK: {
                ISRisk sRisk = (ISRisk) theEObject;
                T result = caseSRisk(sRisk);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sRisk);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sRisk);
                if (result == null)
                    result = caseAbstractSElement(sRisk);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SHAZARD: {
                ISHazard sHazard = (ISHazard) theEObject;
                T result = caseSHazard(sHazard);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sHazard);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sHazard);
                if (result == null)
                    result = caseAbstractSElement(sHazard);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SCAUSE: {
                ISCause sCause = (ISCause) theEObject;
                T result = caseSCause(sCause);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sCause);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sCause);
                if (result == null)
                    result = caseAbstractSElement(sCause);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISDysfunctionsPackage.SFAILURE_STATE: {
                ISFailureState sFailureState = (ISFailureState) theEObject;
                T result = caseSFailureState(sFailureState);
                if (result == null)
                    result = caseAbstractSDysfunctionObject(sFailureState);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sFailureState);
                if (result == null)
                    result = caseAbstractSElement(sFailureState);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SDysfunction Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SDysfunction Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSDysfunctionObject(IAbstractSDysfunctionObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SFailure Mode</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SFailure Mode</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSFailureMode(ISFailureMode object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SFailure Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SFailure Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSFailureEvent(ISFailureEvent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SEffect</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SEffect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSEffect(ISEffect object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SRisk</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SRisk</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSRisk(ISRisk object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SHazard</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SHazard</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSHazard(ISHazard object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SCause</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SCause</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSCause(ISCause object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SFailure State</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SFailure State</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSFailureState(ISFailureState object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSElement(IAbstractSElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // SDysfunctionsSwitch
