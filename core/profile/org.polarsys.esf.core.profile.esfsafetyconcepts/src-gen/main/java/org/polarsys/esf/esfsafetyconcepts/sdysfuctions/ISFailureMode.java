/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFailure Mode</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent <em>Failure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBarrier <em>Barrier</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureMode()
 * @model
 * @generated
 */
public interface ISFailureMode
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureMode_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBase_Class
     * <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Failure Event</b></em>' reference.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode <em>Failure Mode</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Event</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Event</em>' reference.
     * @see #setFailureEvent(ISFailureEvent)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureMode_FailureEvent()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode
     * @model opposite="failureMode" required="true" ordered="false"
     * @generated
     */
    ISFailureEvent getFailureEvent();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent
     * <em>Failure Event</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Failure Event</em>' reference.
     * @see #getFailureEvent()
     * @generated
     */
    void setFailureEvent(ISFailureEvent value);

    /**
     * Returns the value of the '<em><b>Barrier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Barrier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Barrier</em>' reference.
     * @see #setBarrier(ISBarrier)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureMode_Barrier()
     * @model ordered="false"
     * @generated
     */
    ISBarrier getBarrier();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getBarrier
     * <em>Barrier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Barrier</em>' reference.
     * @see #getBarrier()
     * @generated
     */
    void setBarrier(ISBarrier value);

} // ISFailureMode
