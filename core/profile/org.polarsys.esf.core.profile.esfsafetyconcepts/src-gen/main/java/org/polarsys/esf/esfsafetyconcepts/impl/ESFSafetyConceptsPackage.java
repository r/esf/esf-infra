/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.esfcore.IESFCorePackage;
import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsFactory;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ESFSafetyConceptsPackage
    extends EPackageImpl
    implements IESFSafetyConceptsPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private EClass sSafetyArtifactsEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ESFSafetyConceptsPackage() {
        super(eNS_URI, IESFSafetyConceptsFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>
     * This method is used to initialize {@link IESFSafetyConceptsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static IESFSafetyConceptsPackage init() {
        if (isInited)
            return (IESFSafetyConceptsPackage) EPackage.Registry.INSTANCE
                .getEPackage(IESFSafetyConceptsPackage.eNS_URI);

        // Obtain or create and register package
        ESFSafetyConceptsPackage theESFSafetyConceptsPackage =
            (ESFSafetyConceptsPackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ESFSafetyConceptsPackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new ESFSafetyConceptsPackage());

        isInited = true;

        // Initialize simple dependencies
        IESFCorePackage.eINSTANCE.eClass();
        IESFPropertiesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        SDysfunctionsPackage theSDysfunctionsPackage = (SDysfunctionsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(ISDysfunctionsPackage.eNS_URI) instanceof SDysfunctionsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI)
                : ISDysfunctionsPackage.eINSTANCE);
        SRecommendationsPackage theSRecommendationsPackage = (SRecommendationsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(ISRecommendationsPackage.eNS_URI) instanceof SRecommendationsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI)
                : ISRecommendationsPackage.eINSTANCE);

        // Create package meta-data objects
        theESFSafetyConceptsPackage.createPackageContents();
        theSDysfunctionsPackage.createPackageContents();
        theSRecommendationsPackage.createPackageContents();

        // Initialize created meta-data
        theESFSafetyConceptsPackage.initializePackageContents();
        theSDysfunctionsPackage.initializePackageContents();
        theSRecommendationsPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theESFSafetyConceptsPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(IESFSafetyConceptsPackage.eNS_URI, theESFSafetyConceptsPackage);
        return theESFSafetyConceptsPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public EClass getSSafetyArtifacts() {
        return sSafetyArtifactsEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public EReference getSSafetyArtifacts_Base_Package() {
        return (EReference) sSafetyArtifactsEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public IESFSafetyConceptsFactory getESFSafetyConceptsFactory() {
        return (IESFSafetyConceptsFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        sSafetyArtifactsEClass = createEClass(SSAFETY_ARTIFACTS);
        createEReference(sSafetyArtifactsEClass, SSAFETY_ARTIFACTS__BASE_PACKAGE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        ISDysfunctionsPackage theSDysfunctionsPackage =
            (ISDysfunctionsPackage) EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI);
        ISRecommendationsPackage theSRecommendationsPackage =
            (ISRecommendationsPackage) EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI);
        IESFCorePackage theESFCorePackage =
            (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
        UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

        // Add subpackages
        getESubpackages().add(theSDysfunctionsPackage);
        getESubpackages().add(theSRecommendationsPackage);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        sSafetyArtifactsEClass.getESuperTypes().add(theESFCorePackage.getAbstractSElement());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            sSafetyArtifactsEClass,
            ISSafetyArtifacts.class,
            "SSafetyArtifacts", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSSafetyArtifacts_Base_Package(),
            theUMLPackage.getPackage(),
            null,
            "base_Package", //$NON-NLS-1$
            null,
            1,
            1,
            ISSafetyArtifacts.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "ESFSafetyConcepts" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // ESFSafetyConceptsPackage
