/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SAction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction#getKind <em>Kind</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.impl.SAction#getSBarriersList <em>SBarriers
 * List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SAction
    extends SRecommendation
    implements ISAction {

    /**
     * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getKind()
     * @generated
     * @ordered
     */
    protected static final SActionsKind KIND_EDEFAULT = SActionsKind.PREVENTIVE_ACTION;

    /**
     * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getKind()
     * @generated
     * @ordered
     */
    protected SActionsKind kind = KIND_EDEFAULT;

    /**
     * The cached value of the '{@link #getSBarriersList() <em>SBarriers List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSBarriersList()
     * @generated
     * @ordered
     */
    protected EList<ISBarrier> sBarriersList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SAction() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISRecommendationsPackage.Literals.SACTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SActionsKind getKind() {
        return kind;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setKind(SActionsKind newKind) {
        SActionsKind oldKind = kind;
        kind = newKind == null ? KIND_EDEFAULT : newKind;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(this, Notification.SET, ISRecommendationsPackage.SACTION__KIND, oldKind, kind));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISBarrier> getSBarriersList() {
        if (sBarriersList == null) {
            sBarriersList = new EObjectWithInverseResolvingEList.ManyInverse<ISBarrier>(
                ISBarrier.class,
                this,
                ISRecommendationsPackage.SACTION__SBARRIERS_LIST,
                ISRecommendationsPackage.SBARRIER__SACTIONS_LIST);
        }
        return sBarriersList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getSBarriersList())
                    .basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                return ((InternalEList<?>) getSBarriersList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__KIND:
                return getKind();
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                return getSBarriersList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__KIND:
                setKind((SActionsKind) newValue);
                return;
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                getSBarriersList().clear();
                getSBarriersList().addAll((Collection<? extends ISBarrier>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__KIND:
                setKind(KIND_EDEFAULT);
                return;
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                getSBarriersList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISRecommendationsPackage.SACTION__KIND:
                return kind != KIND_EDEFAULT;
            case ISRecommendationsPackage.SACTION__SBARRIERS_LIST:
                return sBarriersList != null && !sBarriersList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (kind: "); //$NON-NLS-1$
        result.append(kind);
        result.append(')');
        return result.toString();
    }

} // SAction
