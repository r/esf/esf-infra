/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISHazard;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SRisk</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk#getHazard <em>Hazard</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk#getFailureEvent <em>Failure Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk#getCausesList <em>Causes List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SRisk#getEffectsList <em>Effects List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SRisk
    extends AbstractSDysfunctionObject
    implements ISRisk {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getHazard() <em>Hazard</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getHazard()
     * @generated
     * @ordered
     */
    protected ISHazard hazard;

    /**
     * The cached value of the '{@link #getFailureEvent() <em>Failure Event</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFailureEvent()
     * @generated
     * @ordered
     */
    protected ISFailureEvent failureEvent;

    /**
     * The cached value of the '{@link #getCausesList() <em>Causes List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCausesList()
     * @generated
     * @ordered
     */
    protected EList<ISCause> causesList;

    /**
     * The cached value of the '{@link #getEffectsList() <em>Effects List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEffectsList()
     * @generated
     * @ordered
     */
    protected EList<ISEffect> effectsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SRisk() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISDysfunctionsPackage.Literals.SRISK;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SRISK__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SRISK__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISHazard getHazard() {
        if (hazard != null && hazard.eIsProxy()) {
            InternalEObject oldHazard = (InternalEObject) hazard;
            hazard = (ISHazard) eResolveProxy(oldHazard);
            if (hazard != oldHazard) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SRISK__HAZARD,
                            oldHazard,
                            hazard));
            }
        }
        return hazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISHazard basicGetHazard() {
        return hazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetHazard(ISHazard newHazard, NotificationChain msgs) {
        ISHazard oldHazard = hazard;
        hazard = newHazard;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                this,
                Notification.SET,
                ISDysfunctionsPackage.SRISK__HAZARD,
                oldHazard,
                newHazard);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setHazard(ISHazard newHazard) {
        if (newHazard != hazard) {
            NotificationChain msgs = null;
            if (hazard != null)
                msgs = ((InternalEObject) hazard)
                    .eInverseRemove(this, ISDysfunctionsPackage.SHAZARD__RISK, ISHazard.class, msgs);
            if (newHazard != null)
                msgs = ((InternalEObject) newHazard)
                    .eInverseAdd(this, ISDysfunctionsPackage.SHAZARD__RISK, ISHazard.class, msgs);
            msgs = basicSetHazard(newHazard, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SRISK__HAZARD,
                    newHazard,
                    newHazard));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureEvent getFailureEvent() {
        if (failureEvent != null && failureEvent.eIsProxy()) {
            InternalEObject oldFailureEvent = (InternalEObject) failureEvent;
            failureEvent = (ISFailureEvent) eResolveProxy(oldFailureEvent);
            if (failureEvent != oldFailureEvent) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SRISK__FAILURE_EVENT,
                            oldFailureEvent,
                            failureEvent));
            }
        }
        return failureEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureEvent basicGetFailureEvent() {
        return failureEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetFailureEvent(ISFailureEvent newFailureEvent, NotificationChain msgs) {
        ISFailureEvent oldFailureEvent = failureEvent;
        failureEvent = newFailureEvent;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                this,
                Notification.SET,
                ISDysfunctionsPackage.SRISK__FAILURE_EVENT,
                oldFailureEvent,
                newFailureEvent);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setFailureEvent(ISFailureEvent newFailureEvent) {
        if (newFailureEvent != failureEvent) {
            NotificationChain msgs = null;
            if (failureEvent != null)
                msgs = ((InternalEObject) failureEvent)
                    .eInverseRemove(this, ISDysfunctionsPackage.SFAILURE_EVENT__RISK, ISFailureEvent.class, msgs);
            if (newFailureEvent != null)
                msgs = ((InternalEObject) newFailureEvent)
                    .eInverseAdd(this, ISDysfunctionsPackage.SFAILURE_EVENT__RISK, ISFailureEvent.class, msgs);
            msgs = basicSetFailureEvent(newFailureEvent, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SRISK__FAILURE_EVENT,
                    newFailureEvent,
                    newFailureEvent));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISCause> getCausesList() {
        if (causesList == null) {
            causesList = new EObjectWithInverseResolvingEList.ManyInverse<ISCause>(
                ISCause.class,
                this,
                ISDysfunctionsPackage.SRISK__CAUSES_LIST,
                ISDysfunctionsPackage.SCAUSE__RISKS_LIST);
        }
        return causesList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISEffect> getEffectsList() {
        if (effectsList == null) {
            effectsList = new EObjectWithInverseResolvingEList.ManyInverse<ISEffect>(
                ISEffect.class,
                this,
                ISDysfunctionsPackage.SRISK__EFFECTS_LIST,
                ISDysfunctionsPackage.SEFFECT__RISKS_LIST);
        }
        return effectsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__HAZARD:
                if (hazard != null)
                    msgs = ((InternalEObject) hazard)
                        .eInverseRemove(this, ISDysfunctionsPackage.SHAZARD__RISK, ISHazard.class, msgs);
                return basicSetHazard((ISHazard) otherEnd, msgs);
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                if (failureEvent != null)
                    msgs = ((InternalEObject) failureEvent)
                        .eInverseRemove(this, ISDysfunctionsPackage.SFAILURE_EVENT__RISK, ISFailureEvent.class, msgs);
                return basicSetFailureEvent((ISFailureEvent) otherEnd, msgs);
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getCausesList()).basicAdd(otherEnd, msgs);
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getEffectsList()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__HAZARD:
                return basicSetHazard(null, msgs);
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                return basicSetFailureEvent(null, msgs);
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                return ((InternalEList<?>) getCausesList()).basicRemove(otherEnd, msgs);
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                return ((InternalEList<?>) getEffectsList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISDysfunctionsPackage.SRISK__HAZARD:
                if (resolve)
                    return getHazard();
                return basicGetHazard();
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                if (resolve)
                    return getFailureEvent();
                return basicGetFailureEvent();
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                return getCausesList();
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                return getEffectsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISDysfunctionsPackage.SRISK__HAZARD:
                setHazard((ISHazard) newValue);
                return;
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                setFailureEvent((ISFailureEvent) newValue);
                return;
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                getCausesList().clear();
                getCausesList().addAll((Collection<? extends ISCause>) newValue);
                return;
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                getEffectsList().clear();
                getEffectsList().addAll((Collection<? extends ISEffect>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISDysfunctionsPackage.SRISK__HAZARD:
                setHazard((ISHazard) null);
                return;
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                setFailureEvent((ISFailureEvent) null);
                return;
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                getCausesList().clear();
                return;
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                getEffectsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SRISK__BASE_CLASS:
                return base_Class != null;
            case ISDysfunctionsPackage.SRISK__HAZARD:
                return hazard != null;
            case ISDysfunctionsPackage.SRISK__FAILURE_EVENT:
                return failureEvent != null;
            case ISDysfunctionsPackage.SRISK__CAUSES_LIST:
                return causesList != null && !causesList.isEmpty();
            case ISDysfunctionsPackage.SRISK__EFFECTS_LIST:
                return effectsList != null && !effectsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // SRisk
