/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfproperties.ISCost;
import org.polarsys.esf.esfproperties.ISCriticality;
import org.polarsys.esf.esfproperties.ISDetectability;
import org.polarsys.esf.esfproperties.ISOccurence;
import org.polarsys.esf.esfproperties.ISProbability;
import org.polarsys.esf.esfproperties.ISSeverity;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SFailure Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getEffectsList <em>Effects
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getRisk <em>Risk</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getCausesList <em>Causes
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getDetectionsList <em>Detections
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getRecommendationsList
 * <em>Recommendations List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getSeverity <em>Severity</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getCriticality
 * <em>Criticality</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getCost <em>Cost</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getOccurence <em>Occurence</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getDetectability
 * <em>Detectability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getProbability
 * <em>Probability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#isFearedEvent <em>Feared
 * Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureEvent#getFailureMode <em>Failure
 * Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SFailureEvent
    extends AbstractSDysfunctionObject
    implements ISFailureEvent {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getEffectsList() <em>Effects List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEffectsList()
     * @generated
     * @ordered
     */
    protected EList<ISEffect> effectsList;

    /**
     * The cached value of the '{@link #getRisk() <em>Risk</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRisk()
     * @generated
     * @ordered
     */
    protected ISRisk risk;

    /**
     * The cached value of the '{@link #getCausesList() <em>Causes List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCausesList()
     * @generated
     * @ordered
     */
    protected EList<ISCause> causesList;

    /**
     * The cached value of the '{@link #getDetectionsList() <em>Detections List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDetectionsList()
     * @generated
     * @ordered
     */
    protected EList<ISDetection> detectionsList;

    /**
     * The cached value of the '{@link #getRecommendationsList() <em>Recommendations List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRecommendationsList()
     * @generated
     * @ordered
     */
    protected EList<ISRecommendation> recommendationsList;

    /**
     * The cached value of the '{@link #getSeverity() <em>Severity</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSeverity()
     * @generated
     * @ordered
     */
    protected ISSeverity severity;

    /**
     * The cached value of the '{@link #getCriticality() <em>Criticality</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCriticality()
     * @generated
     * @ordered
     */
    protected ISCriticality criticality;

    /**
     * The cached value of the '{@link #getCost() <em>Cost</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCost()
     * @generated
     * @ordered
     */
    protected ISCost cost;

    /**
     * The cached value of the '{@link #getOccurence() <em>Occurence</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOccurence()
     * @generated
     * @ordered
     */
    protected ISOccurence occurence;

    /**
     * The cached value of the '{@link #getDetectability() <em>Detectability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDetectability()
     * @generated
     * @ordered
     */
    protected ISDetectability detectability;

    /**
     * The cached value of the '{@link #getProbability() <em>Probability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProbability()
     * @generated
     * @ordered
     */
    protected ISProbability probability;

    /**
     * The default value of the '{@link #isFearedEvent() <em>Feared Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isFearedEvent()
     * @generated
     * @ordered
     */
    protected static final boolean FEARED_EVENT_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isFearedEvent() <em>Feared Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isFearedEvent()
     * @generated
     * @ordered
     */
    protected boolean fearedEvent = FEARED_EVENT_EDEFAULT;

    /**
     * The cached value of the '{@link #getFailureMode() <em>Failure Mode</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFailureMode()
     * @generated
     * @ordered
     */
    protected ISFailureMode failureMode;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SFailureEvent() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISDysfunctionsPackage.Literals.SFAILURE_EVENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISEffect> getEffectsList() {
        if (effectsList == null) {
            effectsList = new EObjectWithInverseResolvingEList.ManyInverse<ISEffect>(
                ISEffect.class,
                this,
                ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST,
                ISDysfunctionsPackage.SEFFECT__FAILURE_EVENTS_LIST);
        }
        return effectsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRisk getRisk() {
        if (risk != null && risk.eIsProxy()) {
            InternalEObject oldRisk = (InternalEObject) risk;
            risk = (ISRisk) eResolveProxy(oldRisk);
            if (risk != oldRisk) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__RISK,
                            oldRisk,
                            risk));
            }
        }
        return risk;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRisk basicGetRisk() {
        return risk;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetRisk(ISRisk newRisk, NotificationChain msgs) {
        ISRisk oldRisk = risk;
        risk = newRisk;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                this,
                Notification.SET,
                ISDysfunctionsPackage.SFAILURE_EVENT__RISK,
                oldRisk,
                newRisk);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setRisk(ISRisk newRisk) {
        if (newRisk != risk) {
            NotificationChain msgs = null;
            if (risk != null)
                msgs = ((InternalEObject) risk)
                    .eInverseRemove(this, ISDysfunctionsPackage.SRISK__FAILURE_EVENT, ISRisk.class, msgs);
            if (newRisk != null)
                msgs = ((InternalEObject) newRisk)
                    .eInverseAdd(this, ISDysfunctionsPackage.SRISK__FAILURE_EVENT, ISRisk.class, msgs);
            msgs = basicSetRisk(newRisk, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__RISK,
                    newRisk,
                    newRisk));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISCause> getCausesList() {
        if (causesList == null) {
            causesList = new EObjectWithInverseResolvingEList.ManyInverse<ISCause>(
                ISCause.class,
                this,
                ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST,
                ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST);
        }
        return causesList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISDetection> getDetectionsList() {
        if (detectionsList == null) {
            detectionsList = new EObjectResolvingEList<ISDetection>(
                ISDetection.class,
                this,
                ISDysfunctionsPackage.SFAILURE_EVENT__DETECTIONS_LIST);
        }
        return detectionsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISRecommendation> getRecommendationsList() {
        if (recommendationsList == null) {
            recommendationsList = new EObjectResolvingEList<ISRecommendation>(
                ISRecommendation.class,
                this,
                ISDysfunctionsPackage.SFAILURE_EVENT__RECOMMENDATIONS_LIST);
        }
        return recommendationsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISSeverity getSeverity() {
        if (severity != null && severity.eIsProxy()) {
            InternalEObject oldSeverity = (InternalEObject) severity;
            severity = (ISSeverity) eResolveProxy(oldSeverity);
            if (severity != oldSeverity) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY,
                            oldSeverity,
                            severity));
            }
        }
        return severity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISSeverity basicGetSeverity() {
        return severity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setSeverity(ISSeverity newSeverity) {
        ISSeverity oldSeverity = severity;
        severity = newSeverity;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY,
                    oldSeverity,
                    severity));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCriticality getCriticality() {
        if (criticality != null && criticality.eIsProxy()) {
            InternalEObject oldCriticality = (InternalEObject) criticality;
            criticality = (ISCriticality) eResolveProxy(oldCriticality);
            if (criticality != oldCriticality) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY,
                            oldCriticality,
                            criticality));
            }
        }
        return criticality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCriticality basicGetCriticality() {
        return criticality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setCriticality(ISCriticality newCriticality) {
        ISCriticality oldCriticality = criticality;
        criticality = newCriticality;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY,
                    oldCriticality,
                    criticality));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCost getCost() {
        if (cost != null && cost.eIsProxy()) {
            InternalEObject oldCost = (InternalEObject) cost;
            cost = (ISCost) eResolveProxy(oldCost);
            if (cost != oldCost) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__COST,
                            oldCost,
                            cost));
            }
        }
        return cost;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISCost basicGetCost() {
        return cost;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setCost(ISCost newCost) {
        ISCost oldCost = cost;
        cost = newCost;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__COST,
                    oldCost,
                    cost));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISOccurence getOccurence() {
        if (occurence != null && occurence.eIsProxy()) {
            InternalEObject oldOccurence = (InternalEObject) occurence;
            occurence = (ISOccurence) eResolveProxy(oldOccurence);
            if (occurence != oldOccurence) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE,
                            oldOccurence,
                            occurence));
            }
        }
        return occurence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISOccurence basicGetOccurence() {
        return occurence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setOccurence(ISOccurence newOccurence) {
        ISOccurence oldOccurence = occurence;
        occurence = newOccurence;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE,
                    oldOccurence,
                    occurence));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetectability getDetectability() {
        if (detectability != null && detectability.eIsProxy()) {
            InternalEObject oldDetectability = (InternalEObject) detectability;
            detectability = (ISDetectability) eResolveProxy(oldDetectability);
            if (detectability != oldDetectability) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY,
                            oldDetectability,
                            detectability));
            }
        }
        return detectability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetectability basicGetDetectability() {
        return detectability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setDetectability(ISDetectability newDetectability) {
        ISDetectability oldDetectability = detectability;
        detectability = newDetectability;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY,
                    oldDetectability,
                    detectability));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISProbability getProbability() {
        if (probability != null && probability.eIsProxy()) {
            InternalEObject oldProbability = (InternalEObject) probability;
            probability = (ISProbability) eResolveProxy(oldProbability);
            if (probability != oldProbability) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY,
                            oldProbability,
                            probability));
            }
        }
        return probability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISProbability basicGetProbability() {
        return probability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setProbability(ISProbability newProbability) {
        ISProbability oldProbability = probability;
        probability = newProbability;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY,
                    oldProbability,
                    probability));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean isFearedEvent() {
        return fearedEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setFearedEvent(boolean newFearedEvent) {
        boolean oldFearedEvent = fearedEvent;
        fearedEvent = newFearedEvent;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__FEARED_EVENT,
                    oldFearedEvent,
                    fearedEvent));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureMode getFailureMode() {
        if (failureMode != null && failureMode.eIsProxy()) {
            InternalEObject oldFailureMode = (InternalEObject) failureMode;
            failureMode = (ISFailureMode) eResolveProxy(oldFailureMode);
            if (failureMode != oldFailureMode) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE,
                            oldFailureMode,
                            failureMode));
            }
        }
        return failureMode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureMode basicGetFailureMode() {
        return failureMode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetFailureMode(ISFailureMode newFailureMode, NotificationChain msgs) {
        ISFailureMode oldFailureMode = failureMode;
        failureMode = newFailureMode;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                this,
                Notification.SET,
                ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE,
                oldFailureMode,
                newFailureMode);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setFailureMode(ISFailureMode newFailureMode) {
        if (newFailureMode != failureMode) {
            NotificationChain msgs = null;
            if (failureMode != null)
                msgs = ((InternalEObject) failureMode).eInverseRemove(
                    this,
                    ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT,
                    ISFailureMode.class,
                    msgs);
            if (newFailureMode != null)
                msgs = ((InternalEObject) newFailureMode)
                    .eInverseAdd(this, ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT, ISFailureMode.class, msgs);
            msgs = basicSetFailureMode(newFailureMode, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE,
                    newFailureMode,
                    newFailureMode));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getEffectsList()).basicAdd(otherEnd, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                if (risk != null)
                    msgs = ((InternalEObject) risk)
                        .eInverseRemove(this, ISDysfunctionsPackage.SRISK__FAILURE_EVENT, ISRisk.class, msgs);
                return basicSetRisk((ISRisk) otherEnd, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getCausesList()).basicAdd(otherEnd, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                if (failureMode != null)
                    msgs = ((InternalEObject) failureMode).eInverseRemove(
                        this,
                        ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT,
                        ISFailureMode.class,
                        msgs);
                return basicSetFailureMode((ISFailureMode) otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                return ((InternalEList<?>) getEffectsList()).basicRemove(otherEnd, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                return basicSetRisk(null, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                return ((InternalEList<?>) getCausesList()).basicRemove(otherEnd, msgs);
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                return basicSetFailureMode(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                return getEffectsList();
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                if (resolve)
                    return getRisk();
                return basicGetRisk();
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                return getCausesList();
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTIONS_LIST:
                return getDetectionsList();
            case ISDysfunctionsPackage.SFAILURE_EVENT__RECOMMENDATIONS_LIST:
                return getRecommendationsList();
            case ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY:
                if (resolve)
                    return getSeverity();
                return basicGetSeverity();
            case ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY:
                if (resolve)
                    return getCriticality();
                return basicGetCriticality();
            case ISDysfunctionsPackage.SFAILURE_EVENT__COST:
                if (resolve)
                    return getCost();
                return basicGetCost();
            case ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE:
                if (resolve)
                    return getOccurence();
                return basicGetOccurence();
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY:
                if (resolve)
                    return getDetectability();
                return basicGetDetectability();
            case ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY:
                if (resolve)
                    return getProbability();
                return basicGetProbability();
            case ISDysfunctionsPackage.SFAILURE_EVENT__FEARED_EVENT:
                return isFearedEvent();
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                if (resolve)
                    return getFailureMode();
                return basicGetFailureMode();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                getEffectsList().clear();
                getEffectsList().addAll((Collection<? extends ISEffect>) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                setRisk((ISRisk) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                getCausesList().clear();
                getCausesList().addAll((Collection<? extends ISCause>) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTIONS_LIST:
                getDetectionsList().clear();
                getDetectionsList().addAll((Collection<? extends ISDetection>) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__RECOMMENDATIONS_LIST:
                getRecommendationsList().clear();
                getRecommendationsList().addAll((Collection<? extends ISRecommendation>) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY:
                setSeverity((ISSeverity) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY:
                setCriticality((ISCriticality) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__COST:
                setCost((ISCost) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE:
                setOccurence((ISOccurence) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY:
                setDetectability((ISDetectability) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY:
                setProbability((ISProbability) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FEARED_EVENT:
                setFearedEvent((Boolean) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                setFailureMode((ISFailureMode) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                getEffectsList().clear();
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                setRisk((ISRisk) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                getCausesList().clear();
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTIONS_LIST:
                getDetectionsList().clear();
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__RECOMMENDATIONS_LIST:
                getRecommendationsList().clear();
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY:
                setSeverity((ISSeverity) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY:
                setCriticality((ISCriticality) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__COST:
                setCost((ISCost) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE:
                setOccurence((ISOccurence) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY:
                setDetectability((ISDetectability) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY:
                setProbability((ISProbability) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FEARED_EVENT:
                setFearedEvent(FEARED_EVENT_EDEFAULT);
                return;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                setFailureMode((ISFailureMode) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_EVENT__BASE_CLASS:
                return base_Class != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__EFFECTS_LIST:
                return effectsList != null && !effectsList.isEmpty();
            case ISDysfunctionsPackage.SFAILURE_EVENT__RISK:
                return risk != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST:
                return causesList != null && !causesList.isEmpty();
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTIONS_LIST:
                return detectionsList != null && !detectionsList.isEmpty();
            case ISDysfunctionsPackage.SFAILURE_EVENT__RECOMMENDATIONS_LIST:
                return recommendationsList != null && !recommendationsList.isEmpty();
            case ISDysfunctionsPackage.SFAILURE_EVENT__SEVERITY:
                return severity != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__CRITICALITY:
                return criticality != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__COST:
                return cost != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__OCCURENCE:
                return occurence != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__DETECTABILITY:
                return detectability != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__PROBABILITY:
                return probability != null;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FEARED_EVENT:
                return fearedEvent != FEARED_EVENT_EDEFAULT;
            case ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE:
                return failureMode != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (fearedEvent: "); //$NON-NLS-1$
        result.append(fearedEvent);
        result.append(')');
        return result.toString();
    }

} // SFailureEvent
