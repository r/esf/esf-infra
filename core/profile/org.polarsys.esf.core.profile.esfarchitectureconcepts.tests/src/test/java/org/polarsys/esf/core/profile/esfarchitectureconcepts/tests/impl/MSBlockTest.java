/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.tests.impl;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.tests.util.ESFArchitectureConceptsTestUtil;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.impl.ESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.impl.SBlock;
import org.polarsys.esf.esfarchitectureconcepts.impl.SModel;

/**
 * MSBlock unit test.
 *
 * @author $Author: bmaggi $
 * @version $Revision: 176 $
 */
public final class MSBlockTest {

    /** SBlock. */
    private SBlock mSBblock;

    /** SModel. */
    private SModel mSModel;

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @Before
    public void setUp() {
        // Prepare Model test
        Model vModel = ESFArchitectureConceptsTestUtil.createESFModel(new ResourceSetImpl());

        Assert
            .assertFalse("The ESFArchitectureConcepts profile must be applied.", vModel.getAppliedProfiles().isEmpty());

        // Apply SModel stereotype on vModel
        StereotypeApplicationHelper vStereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);
        mSModel = (SModel) vStereotypeApplicationHelper
            .applyStereotype(vModel, ESFArchitectureConceptsPackage.eINSTANCE.getSModel());

        // Create new class and apply SBlock stereotype on it
        Class vNewClass = vModel.createOwnedClass("SBlock1", false);
        mSBblock = (SBlock) vStereotypeApplicationHelper
            .applyStereotype(vNewClass, ESFArchitectureConceptsPackage.eINSTANCE.getSBlock());
    }

    /**
     * Test SBlock.basicGetModel().
     */
    @Test
    public void testBasicGetSModel() {
        ISModel vBasicGetSModel = mSBblock.basicGetSModel();
        Assert.assertEquals("Wrong SModel", mSModel, vBasicGetSModel);
    }

}
