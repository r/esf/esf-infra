/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.application.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.infra.widgets.editors.TreeSelectorDialog;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.uml.tools.providers.UMLLabelProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.common.ui.diagram.ESFDiagramUtil;
import org.polarsys.esf.core.common.ui.provider.ESFContentProvider;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfarchitectureconcepts.impl.SBlock;

/**
 * Handler class for creating an ESFLocalAnalysis Diagram.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class CreateESFArchitectureConceptsDiagramHandler
	extends AbstractHandler {

	private static final String SYSTEM_FAULT_DIAGRAM = "System fault diagram"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public CreateESFArchitectureConceptsDiagramHandler() {
	}

	/**
	 * Get the selected element (SBlockLAnalysis class) and call the action for creating an ESFLocalAnalysis Diagram
	 * Table.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

		ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

		Class vSelectedClass =
			(Class) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getClass_());

		final Package vSelectedPkg =
				(Package) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getPackage());
		
		if (vSelectedClass == null || !StereotypeUtil.isApplied(vSelectedClass, SBlock.class)) {
			TreeSelectorDialog dialog = new TreeSelectorDialog(Display.getCurrent().getActiveShell());
			dialog.setLabelProvider((new UMLLabelProvider()));
			ESFContentProvider cp = new ESFContentProvider(vSelectedPkg, UMLPackage.eINSTANCE.getClass_(), SBlock.class);
			if (cp.getElements().length > 0) {
				dialog.setContentProvider(new EncapsulatedContentProvider(cp));
				dialog.setTitle("Select class");
				dialog.setDescription("System failure analysis can be executed on classes with the SBlock stereotype. Select below");
				int code = dialog.open();
			
				if (code == Dialog.OK) {
					Object result = dialog.getResult()[0];
					if (result instanceof Class) {
						vSelectedClass = (Class) result;
					}
				}
			}
			else {
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "No elements",
						"System failure analysis can be executed on classes with the SBlock stereotype. No elements with this annotations are found. " +
						"Please apply architecture annotations first");
			}
		}
		
		if (vSelectedClass != null && StereotypeUtil.isApplied(vSelectedClass, SBlock.class)) {
			ESFDiagramUtil.createDiagram(SYSTEM_FAULT_DIAGRAM, vSelectedClass);
		}
		return null;
	}

}
