/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.impl;

import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.uml2.uml.NamedElement;
import org.polarsys.esf.core.profile.esfcore.CustomUUID;

/**
 * This class implements the derived attributes of the generated class {@link AbstractSElement}.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class GenericAbstractSElement {

    /**
     * Default constructor, private as it's a utility class.
     */
    private GenericAbstractSElement() {
        // Nothing to do
    }

    /**
     * Returns the value of the Name attribute of the NamedElement.
     * @param pElement NamedElement
     * @return Name of NamedElement
     */
    public static String getName(final NamedElement pElement) {
        String vName = "";
        // Get name from UML element
        if (pElement != null) {
            vName = pElement.getName();
        }
        return vName;
    }

    /**
     * Returns the value of the UUID attribute of the NamedElement.
     * @param pElement NamedElement
     * @return UUID of NamedElement
     */
    public static String getUUID(final NamedElement pElement) {
        String vUUID = "";
        // Get UUID from UML element
        if (pElement != null) {
        	String customID = CustomUUID.getUUID(pElement);
        	if (customID != null) {
        		vUUID = customID;
        	}
        	else {
        		vUUID = ((XMIResource) pElement.eResource()).getID(pElement);
        	}
        }
        return vUUID;
    }
}