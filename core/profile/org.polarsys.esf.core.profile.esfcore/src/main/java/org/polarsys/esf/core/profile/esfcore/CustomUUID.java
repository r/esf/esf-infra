/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/

package org.polarsys.esf.core.profile.esfcore;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.uml2.uml.Element;

/**
 * Handle UUIDs based on extension points
 */
public class CustomUUID {

	private static final String CLASS = "class"; //$NON-NLS-1$

	public static final String ICUSTOM_UUID_ID = ESFCoreActivator.PLUGIN_ID + ".customuuid"; //$NON-NLS-1$
	
	public static String getUUID(Element element) {
		ICustomUUID customUUIDintf = getCustomUUID(element);
		if (customUUIDintf != null) {
			return customUUIDintf.getUUID(element);
		}
		return null;
	}

	public static boolean showUUID(Element element) {
		ICustomUUID customUUIDintf = getCustomUUID(element);
		if (customUUIDintf != null) {
			return customUUIDintf.showUUID(element);
		}
		// show (ESF) UUID by default
		return true;
	}

	/**
	 * Retrieve a direction from a port. The first extension returning a
	 * defined value (in, out, inout) is returned
	 * 
	 * @param element a UML element for which the UUID be determined
	 * @return the UUIDfunction
	 */
	public static ICustomUUID getCustomUUID(Element element) {
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(ICUSTOM_UUID_ID);

		for (IConfigurationElement configElement : configElements) {
			try {
				final Object obj = configElement.createExecutableExtension(CLASS);
				if (obj instanceof ICustomUUID) {
					ICustomUUID customUUID = (ICustomUUID) obj;
					if (customUUID.getUUID(element) != null) {
						return customUUID;
					}
				}
			} catch (CoreException exception) {
				exception.printStackTrace();
			}
		}
		return null;
	}
	
	
}
