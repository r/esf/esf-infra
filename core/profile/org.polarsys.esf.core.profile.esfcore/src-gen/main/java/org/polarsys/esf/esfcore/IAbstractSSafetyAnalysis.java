/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SSafety Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis#getSSafetyConceptsList <em>SSafety Concepts
 * List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSSafetyAnalysis()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSSafetyAnalysis
    extends IAbstractSElement {

    /**
     * Returns the value of the '<em><b>SSafety Concepts List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>SSafety Concepts List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>SSafety Concepts List</em>' reference list.
     * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSSafetyAnalysis_SSafetyConceptsList()
     * @model ordered="false"
     * @generated
     */
    EList<IAbstractSSafetyConcept> getSSafetyConceptsList();

} // IAbstractSSafetyAnalysis
