/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;
import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSRequirement;
import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfcore.IESFCoreFactory;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFCorePackage
    extends EPackageImpl
    implements IESFCorePackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSArchitectureElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSSafetyConceptEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSSafetyAnalysisEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSRequirementEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfcore.IESFCorePackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ESFCorePackage() {
        super(eNS_URI, IESFCoreFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>
     * This method is used to initialize {@link IESFCorePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static IESFCorePackage init() {
        if (isInited)
            return (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);

        // Obtain or create and register package
        ESFCorePackage theESFCorePackage =
            (ESFCorePackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ESFCorePackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new ESFCorePackage());

        isInited = true;

        // Initialize simple dependencies
        TypesPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theESFCorePackage.createPackageContents();

        // Initialize created meta-data
        theESFCorePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theESFCorePackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(IESFCorePackage.eNS_URI, theESFCorePackage);
        return theESFCorePackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSElement() {
        return abstractSElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EAttribute getAbstractSElement_UUID() {
        return (EAttribute) abstractSElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EAttribute getAbstractSElement_Name() {
        return (EAttribute) abstractSElementEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EAttribute getAbstractSElement_Description() {
        return (EAttribute) abstractSElementEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSArchitectureElement() {
        return abstractSArchitectureElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getAbstractSArchitectureElement_SSafetyConceptsList() {
        return (EReference) abstractSArchitectureElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSSafetyConcept() {
        return abstractSSafetyConceptEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getAbstractSSafetyConcept_SArchitectureElementsList() {
        return (EReference) abstractSSafetyConceptEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSSafetyAnalysis() {
        return abstractSSafetyAnalysisEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getAbstractSSafetyAnalysis_SSafetyConceptsList() {
        return (EReference) abstractSSafetyAnalysisEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSRequirement() {
        return abstractSRequirementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getAbstractSRequirement_SElementsList() {
        return (EReference) abstractSRequirementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public IESFCoreFactory getESFCoreFactory() {
        return (IESFCoreFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        abstractSElementEClass = createEClass(ABSTRACT_SELEMENT);
        createEAttribute(abstractSElementEClass, ABSTRACT_SELEMENT__UUID);
        createEAttribute(abstractSElementEClass, ABSTRACT_SELEMENT__NAME);
        createEAttribute(abstractSElementEClass, ABSTRACT_SELEMENT__DESCRIPTION);

        abstractSArchitectureElementEClass = createEClass(ABSTRACT_SARCHITECTURE_ELEMENT);
        createEReference(abstractSArchitectureElementEClass, ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST);

        abstractSSafetyConceptEClass = createEClass(ABSTRACT_SSAFETY_CONCEPT);
        createEReference(abstractSSafetyConceptEClass, ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST);

        abstractSSafetyAnalysisEClass = createEClass(ABSTRACT_SSAFETY_ANALYSIS);
        createEReference(abstractSSafetyAnalysisEClass, ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST);

        abstractSRequirementEClass = createEClass(ABSTRACT_SREQUIREMENT);
        createEReference(abstractSRequirementEClass, ABSTRACT_SREQUIREMENT__SELEMENTS_LIST);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        abstractSArchitectureElementEClass.getESuperTypes().add(this.getAbstractSElement());
        abstractSSafetyConceptEClass.getESuperTypes().add(this.getAbstractSElement());
        abstractSSafetyAnalysisEClass.getESuperTypes().add(this.getAbstractSElement());
        abstractSRequirementEClass.getESuperTypes().add(this.getAbstractSElement());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            abstractSElementEClass,
            IAbstractSElement.class,
            "AbstractSElement", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(
            getAbstractSElement_UUID(),
            theTypesPackage.getString(),
            "UUID", //$NON-NLS-1$
            null,
            1,
            1,
            IAbstractSElement.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_UNSETTABLE,
            !IS_ID,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEAttribute(
            getAbstractSElement_Name(),
            theTypesPackage.getString(),
            "name", //$NON-NLS-1$
            null,
            1,
            1,
            IAbstractSElement.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_UNSETTABLE,
            !IS_ID,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEAttribute(
            getAbstractSElement_Description(),
            theTypesPackage.getString(),
            "description", //$NON-NLS-1$
            null,
            0,
            1,
            IAbstractSElement.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_UNSETTABLE,
            !IS_ID,
            !IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            abstractSArchitectureElementEClass,
            IAbstractSArchitectureElement.class,
            "AbstractSArchitectureElement", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getAbstractSArchitectureElement_SSafetyConceptsList(),
            this.getAbstractSSafetyConcept(),
            this.getAbstractSSafetyConcept_SArchitectureElementsList(),
            "sSafetyConceptsList", //$NON-NLS-1$
            null,
            0,
            -1,
            IAbstractSArchitectureElement.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            abstractSSafetyConceptEClass,
            IAbstractSSafetyConcept.class,
            "AbstractSSafetyConcept", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getAbstractSSafetyConcept_SArchitectureElementsList(),
            this.getAbstractSArchitectureElement(),
            this.getAbstractSArchitectureElement_SSafetyConceptsList(),
            "sArchitectureElementsList", //$NON-NLS-1$
            null,
            0,
            -1,
            IAbstractSSafetyConcept.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            abstractSSafetyAnalysisEClass,
            IAbstractSSafetyAnalysis.class,
            "AbstractSSafetyAnalysis", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getAbstractSSafetyAnalysis_SSafetyConceptsList(),
            this.getAbstractSSafetyConcept(),
            null,
            "sSafetyConceptsList", //$NON-NLS-1$
            null,
            0,
            -1,
            IAbstractSSafetyAnalysis.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            abstractSRequirementEClass,
            IAbstractSRequirement.class,
            "AbstractSRequirement", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getAbstractSRequirement_SElementsList(),
            this.getAbstractSElement(),
            null,
            "sElementsList", //$NON-NLS-1$
            null,
            0,
            -1,
            IAbstractSRequirement.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "ESFCore" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // ESFCorePackage
