/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfcore.IESFCoreFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFCore'"
 * @generated
 */
public interface IESFCorePackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "esfcore"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFCore"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "ESFCore"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    IESFCorePackage eINSTANCE = org.polarsys.esf.esfcore.impl.ESFCorePackage.init();

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfcore.impl.AbstractSElement <em>Abstract SElement</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfcore.impl.AbstractSElement
     * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSElement()
     * @generated
     */
    int ABSTRACT_SELEMENT = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SELEMENT__UUID = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SELEMENT__NAME = 1;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SELEMENT__DESCRIPTION = 2;

    /**
     * The number of structural features of the '<em>Abstract SElement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SELEMENT_FEATURE_COUNT = 3;

    /**
     * The number of operations of the '<em>Abstract SElement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SELEMENT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement <em>Abstract
     * SArchitecture Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement
     * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSArchitectureElement()
     * @generated
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT = 1;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT__UUID = ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT__NAME = ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT__DESCRIPTION = ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST = ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Abstract SArchitecture Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT_FEATURE_COUNT = ABSTRACT_SELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Abstract SArchitecture Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SARCHITECTURE_ELEMENT_OPERATION_COUNT = ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept <em>Abstract SSafety
     * Concept</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept
     * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSSafetyConcept()
     * @generated
     */
    int ABSTRACT_SSAFETY_CONCEPT = 2;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT__UUID = ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT__NAME = ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT__DESCRIPTION = ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST = ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Abstract SSafety Concept</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT_FEATURE_COUNT = ABSTRACT_SELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Abstract SSafety Concept</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_CONCEPT_OPERATION_COUNT = ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis <em>Abstract SSafety
     * Analysis</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis
     * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSSafetyAnalysis()
     * @generated
     */
    int ABSTRACT_SSAFETY_ANALYSIS = 3;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS__UUID = ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS__NAME = ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS__DESCRIPTION = ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SSafety Concepts List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST = ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Abstract SSafety Analysis</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS_FEATURE_COUNT = ABSTRACT_SELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Abstract SSafety Analysis</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SSAFETY_ANALYSIS_OPERATION_COUNT = ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfcore.impl.AbstractSRequirement <em>Abstract
     * SRequirement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfcore.impl.AbstractSRequirement
     * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSRequirement()
     * @generated
     */
    int ABSTRACT_SREQUIREMENT = 4;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT__UUID = ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT__NAME = ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT__DESCRIPTION = ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The feature id for the '<em><b>SElements List</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT__SELEMENTS_LIST = ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Abstract SRequirement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT_FEATURE_COUNT = ABSTRACT_SELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Abstract SRequirement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SREQUIREMENT_OPERATION_COUNT = ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfcore.IAbstractSElement <em>Abstract
     * SElement</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SElement</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSElement
     * @generated
     */
    EClass getAbstractSElement();

    /**
     * Returns the meta object for the attribute '{@link org.polarsys.esf.esfcore.IAbstractSElement#getUUID
     * <em>UUID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>UUID</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSElement#getUUID()
     * @see #getAbstractSElement()
     * @generated
     */
    EAttribute getAbstractSElement_UUID();

    /**
     * Returns the meta object for the attribute '{@link org.polarsys.esf.esfcore.IAbstractSElement#getName
     * <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSElement#getName()
     * @see #getAbstractSElement()
     * @generated
     */
    EAttribute getAbstractSElement_Name();

    /**
     * Returns the meta object for the attribute '{@link org.polarsys.esf.esfcore.IAbstractSElement#getDescription
     * <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSElement#getDescription()
     * @see #getAbstractSElement()
     * @generated
     */
    EAttribute getAbstractSElement_Description();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement <em>Abstract
     * SArchitecture Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SArchitecture Element</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSArchitectureElement
     * @generated
     */
    EClass getAbstractSArchitectureElement();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement#getSSafetyConceptsList <em>SSafety Concepts
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SSafety Concepts List</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSArchitectureElement#getSSafetyConceptsList()
     * @see #getAbstractSArchitectureElement()
     * @generated
     */
    EReference getAbstractSArchitectureElement_SSafetyConceptsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept <em>Abstract SSafety
     * Concept</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SSafety Concept</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyConcept
     * @generated
     */
    EClass getAbstractSSafetyConcept();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept#getSArchitectureElementsList <em>SArchitecture Elements
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SArchitecture Elements List</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyConcept#getSArchitectureElementsList()
     * @see #getAbstractSSafetyConcept()
     * @generated
     */
    EReference getAbstractSSafetyConcept_SArchitectureElementsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis <em>Abstract SSafety
     * Analysis</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SSafety Analysis</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis
     * @generated
     */
    EClass getAbstractSSafetyAnalysis();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis#getSSafetyConceptsList <em>SSafety Concepts
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SSafety Concepts List</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis#getSSafetyConceptsList()
     * @see #getAbstractSSafetyAnalysis()
     * @generated
     */
    EReference getAbstractSSafetyAnalysis_SSafetyConceptsList();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfcore.IAbstractSRequirement <em>Abstract
     * SRequirement</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SRequirement</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSRequirement
     * @generated
     */
    EClass getAbstractSRequirement();

    /**
     * Returns the meta object for the reference list
     * '{@link org.polarsys.esf.esfcore.IAbstractSRequirement#getSElementsList <em>SElements List</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>SElements List</em>'.
     * @see org.polarsys.esf.esfcore.IAbstractSRequirement#getSElementsList()
     * @see #getAbstractSRequirement()
     * @generated
     */
    EReference getAbstractSRequirement_SElementsList();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    IESFCoreFactory getESFCoreFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfcore.impl.AbstractSElement <em>Abstract
         * SElement</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfcore.impl.AbstractSElement
         * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSElement()
         * @generated
         */
        EClass ABSTRACT_SELEMENT = eINSTANCE.getAbstractSElement();

        /**
         * The meta object literal for the '<em><b>UUID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_SELEMENT__UUID = eINSTANCE.getAbstractSElement_UUID();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_SELEMENT__NAME = eINSTANCE.getAbstractSElement_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_SELEMENT__DESCRIPTION = eINSTANCE.getAbstractSElement_Description();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement
         * <em>Abstract SArchitecture Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement
         * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSArchitectureElement()
         * @generated
         */
        EClass ABSTRACT_SARCHITECTURE_ELEMENT = eINSTANCE.getAbstractSArchitectureElement();

        /**
         * The meta object literal for the '<em><b>SSafety Concepts List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST =
            eINSTANCE.getAbstractSArchitectureElement_SSafetyConceptsList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept <em>Abstract
         * SSafety Concept</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept
         * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSSafetyConcept()
         * @generated
         */
        EClass ABSTRACT_SSAFETY_CONCEPT = eINSTANCE.getAbstractSSafetyConcept();

        /**
         * The meta object literal for the '<em><b>SArchitecture Elements List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST =
            eINSTANCE.getAbstractSSafetyConcept_SArchitectureElementsList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis <em>Abstract
         * SSafety Analysis</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis
         * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSSafetyAnalysis()
         * @generated
         */
        EClass ABSTRACT_SSAFETY_ANALYSIS = eINSTANCE.getAbstractSSafetyAnalysis();

        /**
         * The meta object literal for the '<em><b>SSafety Concepts List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST =
            eINSTANCE.getAbstractSSafetyAnalysis_SSafetyConceptsList();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfcore.impl.AbstractSRequirement <em>Abstract
         * SRequirement</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfcore.impl.AbstractSRequirement
         * @see org.polarsys.esf.esfcore.impl.ESFCorePackage#getAbstractSRequirement()
         * @generated
         */
        EClass ABSTRACT_SREQUIREMENT = eINSTANCE.getAbstractSRequirement();

        /**
         * The meta object literal for the '<em><b>SElements List</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SREQUIREMENT__SELEMENTS_LIST = eINSTANCE.getAbstractSRequirement_SElementsList();

    }

} // IESFCorePackage
