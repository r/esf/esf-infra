/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.impl;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSRequirement;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SRequirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.impl.AbstractSRequirement#getSElementsList <em>SElements List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSRequirement
    extends AbstractSElement
    implements IAbstractSRequirement {

    /**
     * The cached value of the '{@link #getSElementsList() <em>SElements List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSElementsList()
     * @generated
     * @ordered
     */
    protected EList<IAbstractSElement> sElementsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractSRequirement() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFCorePackage.Literals.ABSTRACT_SREQUIREMENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<IAbstractSElement> getSElementsList() {
        if (sElementsList == null) {
            sElementsList = new EObjectResolvingEList<IAbstractSElement>(
                IAbstractSElement.class,
                this,
                IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST);
        }
        return sElementsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST:
                return getSElementsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST:
                getSElementsList().clear();
                getSElementsList().addAll((Collection<? extends IAbstractSElement>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST:
                getSElementsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SREQUIREMENT__SELEMENTS_LIST:
                return sElementsList != null && !sElementsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // AbstractSRequirement
