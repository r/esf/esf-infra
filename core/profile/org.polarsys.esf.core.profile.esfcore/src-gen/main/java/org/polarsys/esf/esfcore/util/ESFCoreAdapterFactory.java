/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.polarsys.esf.esfcore.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfcore.IESFCorePackage
 * @generated
 */
public class ESFCoreAdapterFactory
    extends AdapterFactoryImpl {

    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static IESFCorePackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFCoreAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = IESFCorePackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance
     * object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ESFCoreSwitch<Adapter> modelSwitch = new ESFCoreSwitch<Adapter>() {

        @Override
        public Adapter caseAbstractSElement(IAbstractSElement object) {
            return createAbstractSElementAdapter();
        }

        @Override
        public Adapter caseAbstractSArchitectureElement(IAbstractSArchitectureElement object) {
            return createAbstractSArchitectureElementAdapter();
        }

        @Override
        public Adapter caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
            return createAbstractSSafetyConceptAdapter();
        }

        @Override
        public Adapter caseAbstractSSafetyAnalysis(IAbstractSSafetyAnalysis object) {
            return createAbstractSSafetyAnalysisAdapter();
        }

        @Override
        public Adapter caseAbstractSRequirement(IAbstractSRequirement object) {
            return createAbstractSRequirementAdapter();
        }

        @Override
        public Adapter defaultCase(EObject object) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement <em>Abstract
     * SElement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSElement
     * @generated
     */
    public Adapter createAbstractSElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement
     * <em>Abstract SArchitecture Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSArchitectureElement
     * @generated
     */
    public Adapter createAbstractSArchitectureElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept
     * <em>Abstract SSafety Concept</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyConcept
     * @generated
     */
    public Adapter createAbstractSSafetyConceptAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis
     * <em>Abstract SSafety Analysis</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis
     * @generated
     */
    public Adapter createAbstractSSafetyAnalysisAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSRequirement <em>Abstract
     * SRequirement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see org.polarsys.esf.esfcore.IAbstractSRequirement
     * @generated
     */
    public Adapter createAbstractSRequirementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // ESFCoreAdapterFactory
