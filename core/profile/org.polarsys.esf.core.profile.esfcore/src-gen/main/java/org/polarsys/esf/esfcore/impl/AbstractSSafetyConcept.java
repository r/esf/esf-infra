/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SSafety Concept</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyConcept#getSArchitectureElementsList <em>SArchitecture
 * Elements List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSSafetyConcept
    extends AbstractSElement
    implements IAbstractSSafetyConcept {

    /**
     * The cached value of the '{@link #getSArchitectureElementsList() <em>SArchitecture Elements List</em>}' reference
     * list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSArchitectureElementsList()
     * @generated
     * @ordered
     */
    protected EList<IAbstractSArchitectureElement> sArchitectureElementsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractSSafetyConcept() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFCorePackage.Literals.ABSTRACT_SSAFETY_CONCEPT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<IAbstractSArchitectureElement> getSArchitectureElementsList() {
        if (sArchitectureElementsList == null) {
            sArchitectureElementsList = new EObjectWithInverseResolvingEList.ManyInverse<IAbstractSArchitectureElement>(
                IAbstractSArchitectureElement.class,
                this,
                IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST,
                IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT__SSAFETY_CONCEPTS_LIST);
        }
        return sArchitectureElementsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getSArchitectureElementsList())
                    .basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                return ((InternalEList<?>) getSArchitectureElementsList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                return getSArchitectureElementsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                getSArchitectureElementsList().clear();
                getSArchitectureElementsList().addAll((Collection<? extends IAbstractSArchitectureElement>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                getSArchitectureElementsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT__SARCHITECTURE_ELEMENTS_LIST:
                return sArchitectureElementsList != null && !sArchitectureElementsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // AbstractSSafetyConcept
