/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SSafety Concept</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.IAbstractSSafetyConcept#getSArchitectureElementsList <em>SArchitecture Elements
 * List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSSafetyConcept()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSSafetyConcept
    extends IAbstractSElement {

    /**
     * Returns the value of the '<em><b>SArchitecture Elements List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement#getSSafetyConceptsList <em>SSafety Concepts
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>SArchitecture Elements List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>SArchitecture Elements List</em>' reference list.
     * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSSafetyConcept_SArchitectureElementsList()
     * @see org.polarsys.esf.esfcore.IAbstractSArchitectureElement#getSSafetyConceptsList
     * @model opposite="sSafetyConceptsList" ordered="false"
     * @generated
     */
    EList<IAbstractSArchitectureElement> getSArchitectureElementsList();

} // IAbstractSSafetyConcept
