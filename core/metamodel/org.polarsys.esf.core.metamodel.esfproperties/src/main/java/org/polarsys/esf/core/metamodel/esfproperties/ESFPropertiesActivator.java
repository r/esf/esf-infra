/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.metamodel.esfproperties;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;

/**
 * The activator class controls the plug-in life cycle. Central singleton for
 * the ESFProperties Meta-model plug-in.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 169 $
 */
public class ESFPropertiesActivator
    extends EMFPlugin {

    /** Keep track of the encapsulating singleton. */
    public static final ESFPropertiesActivator INSTANCE = new ESFPropertiesActivator();

    /** Keep track of the implementation singleton. */
    private static Implementation sPlugin = null;

    /**
     * Create the instance.
     */
    public ESFPropertiesActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * The actual implementation of the Eclipse <b>UIPlugin</b>.
     */
    public static class Implementation
        extends EclipsePlugin {

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember of the static instance
            sPlugin = this;
        }
    }
}
